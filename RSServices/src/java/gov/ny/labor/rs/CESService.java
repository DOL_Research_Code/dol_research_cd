/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * http://50.19.104.225:8080/RSServices/CESService?periodtype=03&startyear=2010&endyear=2012&areatype=01&a0=000036&i0=20000000
 */

package gov.ny.labor.rs;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Administrator
 */
public class CESService extends HttpServlet {

    private static int MAX_NUM_AREAS = 100;
    private static int MAX_NUM_INDUSTRIES = 200;    // There are currently 195 distinct entries in CES_NAICS

    // The repeated question marks let us use a single prepared statement that the database can cache the execution plan for.
    // For any unused parameters, we'll just insert null which the database should quickly optimize away.
    private static String CES_V2_5_PS = ""
    + " select"
    +     " CES_V2_5.AREA,"
    +     " AREANAME,"
    +     " PERIODYEAR,"
    +     " PERIOD,"
    +     " EMPCES,"
    +     " CES_V2_5.SERIESCODE,"
    +     " DESCRIPTION,"
    +     " HOURS," // Hours per week
    +     " EARNINGS," // Earnings per week
    +     " HOUREARN," // Earnings per hour
    +     " HOURSALLWRKR," // Hours per week
    +     " EARNINGSALLWRKR," // Earnings per week
    +     " HOUREARNALLWRKR" // Earnings per hour
    + " from"
    +     " CES_V2_5,"
    +     " GEOG,"
    +     " CES_NAICS"
    + " where"
    +     " CES_V2_5.STFIPS = 36"
    +     " and CES_V2_5.ADJUSTED = ?"
    +     " and CES_V2_5.PERIODTYPE = ?"
    +     " and CES_V2_5.PERIODYEAR >= ?"
    +     " and CES_V2_5.PERIODYEAR <= ?"
    +     " and CES_V2_5.PERIOD >= ?"
    +     " and CES_V2_5.PERIOD <= ?"
    +     " and ((CES_V2_5.AREATYPE = '01' and CES_V2_5.AREA = ?) or ((CES_V2_5.AREATYPE = ? or CES_V2_5.AREATYPE = ?) and CES_V2_5.AREA in (" + StringUtils.repeat("?",",",MAX_NUM_AREAS) + ")))"
    +     " and CES_V2_5.SERIESCODE in (" + StringUtils.repeat("?",",",MAX_NUM_INDUSTRIES) + ")"
    +     " and GEOG.STFIPS = CES_V2_5.STFIPS and GEOG.AREATYPE = CES_V2_5.AREATYPE and GEOG.AREA = CES_V2_5.AREA"
    +     " and CES_NAICS.SERIESCODE = CES_V2_5.SERIESCODE"
    + " order by"
    +     " CES_V2_5.AREATYPE, AREA, SERIESCODE, PERIODYEAR, PERIOD ";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();

        String callback = request.getParameter("callback");
        if(callback != null) {
            out.print(callback);
            out.print("(");
        }
        out.print("{");
        out.print("\"aaData\": [");

        //processRequest(request, response);

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DecimalFormat dfTenths = new DecimalFormat("#0.0");

            DataSource ds = (DataSource)getServletContext().getAttribute("ds");
            con = ds.getConnection();
            ps = con.prepareStatement(CES_V2_5_PS);
            int i = 0;
            String seasonallyAdjusted = request.getParameter("seasonAdjusted");
            if(seasonallyAdjusted != null && seasonallyAdjusted.equalsIgnoreCase("1")) {
                ps.setString(++i, "1");
            }
            else {
                ps.setString(++i, "0");
            }
            String periodType = request.getParameter("periodtype");
            ps.setString(++i, periodType);
            String startYear = request.getParameter("startyear");
            ps.setString(++i, (startYear == null) ? "1000" : startYear);
            String endYear = request.getParameter("endyear");
            ps.setString(++i, (endYear == null) ? "3000" : endYear);
            String startPeriod = request.getParameter("startperiod");
            ps.setString(++i, (startPeriod == null) ? "00" : startPeriod);
            String endPeriod = request.getParameter("endperiod");
            ps.setString(++i, (endPeriod == null) ? "99" : endPeriod);
            String stateWide = request.getParameter("isstatewide");
            if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                ps.setString(++i, "000036");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }
            String areaType = request.getParameter("areatype");
            if(areaType != null) {
                ps.setString(++i, areaType);
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }
            if(areaType != null && areaType.equals("21")) {
                ps.setString(++i, "23");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            // Grab selected area parameters
            for(int j = 0; j < MAX_NUM_AREAS; j++) {
                String areaParam = request.getParameter("a" + Integer.toString(j));
                if(areaParam != null) {
                    ps.setString(++i, areaParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }
            // Grab selected industry code parameters
            for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                String industryParam = request.getParameter("i" + Integer.toString(j));
                if(industryParam != null) {
                    ps.setString(++i, industryParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }

            rs = ps.executeQuery();

            String prevArea = null;
            String prevSeries = null;
            int nPrevEmployment = Integer.MAX_VALUE;
            boolean first = true;
            
            while (rs.next()) {
                String area = rs.getString("AREA");
                String series = rs.getString("SERIESCODE");
                if((prevArea != null && !prevArea.equals(area)) || (prevSeries != null && !prevSeries.equals(series))) {
                    nPrevEmployment = Integer.MAX_VALUE;
                }
                String areaName = rs.getString("AREANAME");
                String year = rs.getString("PERIODYEAR");
                String period = rs.getString("PERIOD");
                String seriesCode = rs.getString("SERIESCODE");
                String description = rs.getString("DESCRIPTION");
                String employment = rs.getString("EMPCES");
                int nEmployment = (employment != null) ? Integer.parseInt(employment) : 0;
                // Figure out the computed values
                int nAbsoluteChangeEmployment = (nPrevEmployment == Integer.MAX_VALUE) ? 0 : nEmployment - nPrevEmployment;
                double dPercentChangeEmployment = (nPrevEmployment == Integer.MAX_VALUE) ? 0.0 : (double)nAbsoluteChangeEmployment * 100.0 / (double)nPrevEmployment;
                String hoursProd = rs.getString("HOURS");
                String weeklyEarningsProd = rs.getString("EARNINGS");
                String hourlyEarningsProd = rs.getString("HOUREARN");
                String hoursAll = rs.getString("HOURSALLWRKR");
                String weeklyEarningsAll = rs.getString("EARNINGSALLWRKR");
                String hourlyEarningsAll = rs.getString("HOUREARNALLWRKR");

                if(first) {
                    first = false;
                }
                else {
                    out.print(",");
                }
                out.print("["
                        + "\"" + area + "\","
                        + "\"" + areaName + "\","
                        + "\"" + year + "\","
                        + "\"" + period + "\","
                        + "\"" + Utils.getPeriodName(periodType, period, year) + "\","
                        + "\"" + seriesCode + "\","
                        + "\"" + description + "\","
                        + employment + ","
                        + nAbsoluteChangeEmployment + ","
                        + dfTenths.format(dPercentChangeEmployment) + ","
                        + hoursProd + ","
                        + weeklyEarningsProd + ","
                        + hourlyEarningsProd + ","
                        + hoursAll + ","
                        + weeklyEarningsAll + ","
                        + hourlyEarningsAll + "]");

                // Assign current values to previous variables
                nPrevEmployment = nEmployment;
                prevArea = area;
                prevSeries = series;
            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            if(rs != null) {
                try {
                    rs.close();
                }
                catch(SQLException ex) {
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                }
                catch(SQLException ex) {
                }
            }
            if(con != null) {
                try {
                    con.close();
                }
                catch(SQLException ex) {
                }
            }
        }

        out.print("],");
        out.print("\"aoColumns\": [");
        out.print("{ \"sTitle\": \"Area\" },");
        out.print("{ \"sTitle\": \"Area Name\" },");
        out.print("{ \"sTitle\": \"Year\" },");
        out.print("{ \"sTitle\": \"Period\" },");
        out.print("{ \"sTitle\": \"Period Name\" },");
        out.print("{ \"sTitle\": \"Series\" },");
        out.print("{ \"sTitle\": \"Industry\" },");
        out.print("{ \"sTitle\": \"Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Absolute Change Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Percent Change Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Hours (Production)\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Weekly Earnings (Production)\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Hourly Earnings (Production)\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Hours (All)\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Weekly Earnings (All)\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Hourly Earnings (All)\", \"sClass\": \"ta_right\" }");

        out.print("]");
        out.print("}");
        if(callback != null) {
            out.println(");");
        }
        out.close();
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * http://localhost:8080/RSServices/LAUSService?periodtype=01&startyear=2005&endyear=2010&areatype=01&area0=000036
     * http://localhost:8080/RSServices/LAUSService?periodtype=03&startyear=2003&endyear=2010&areatype=04&area0=000015&area1=000017
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
