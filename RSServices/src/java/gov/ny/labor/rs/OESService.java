/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.ny.labor.rs;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Administrator
 */
public class OESService extends HttpServlet {

    private static int MAX_NUM_AREAS = 100;
    private static int MAX_NUM_INDUSTRIES = 150;    // There are currently 137 codes with 3 or less digits
    private static int MAX_NUM_OCC = 150;
    // The repeated question marks let us use a single prepared statement that the database can cache the execution plan for.
    // For any unused parameters, we'll just insert null which the database should quickly optimize away.
    private static String LABFORCE_PS = ""
    + "with"
    + "	Base as ("
    + "		select"
    + "			OESWAGE.AREATYPE,"
    + "			OESWAGE.AREA,"
    + "			AREANAME,"
    + "			PERIODYEAR,"
    + "			PERIOD,"
    + "			INDCODE,"
    + "			TITLE as NAICSTITLE,"
    + "			OCCCODE,"
    + "			SOCCODE.SOCTITLE as OCCTITLE,"
    + "			EMPCOUNT,"
    + "			MEAN,"
    + "			ENTRYWG,"
    + "			EXPERIENCE,"
    + "			MEDIAN,"
    + "			SUPPRESS"
    + "		from"
    + "			OESWAGE,"
    + "			GEOG,"
    + "                 NAICS_CODE,"
    + "                 SOCCODE"
    + "		where"
    + "			OESWAGE.RATETYPE = ?"
    + "			and ((OESWAGE.AREATYPE = '01' and OESWAGE.AREA = ?) or (OESWAGE.AREATYPE = ? and OESWAGE.AREA in (" + StringUtils.repeat("?",",",MAX_NUM_AREAS) + ")))"
    + "			and OESWAGE.INDCODE in (" + StringUtils.repeat("?",",",MAX_NUM_INDUSTRIES) + ")"
    + "			and OESWAGE.OCCCODE in (" + StringUtils.repeat("?",",",MAX_NUM_OCC) + ")"
    + "			and GEOG.STFIPS = '36' and GEOG.AREATYPE = OESWAGE.AREATYPE and GEOG.AREA = OESWAGE.AREA"
    + "                 and NAICS_CODE.NAICS = OESWAGE.INDCODE"
    + "                 and SOCCODE.SOCCODE = OESWAGE.OCCCODE"
    + "                 and (SUPPRESS = 0 or SUPPRESS = 1)"
    + "	),"
    + "	CrossProduct as ("
    + "		select AREA, PERIODYEAR, PERIOD, INDCODE, OCCCODE"
    + "		from"
    + "			(select distinct AREA from Base) as AREA"
    + "			CROSS JOIN"
    + "			(select distinct PERIODYEAR from Base) as PERIODYEAR"
    + "			CROSS JOIN"
    + "			(select distinct PERIOD from Base) as PERIOD"
    + "			CROSS JOIN"
    + "			(select distinct INDCODE from Base) as INDCODE"
    + "			CROSS JOIN"
    + "			(select distinct OCCCODE from Base) as OCCCODE"
    + "	)"
    + " select"
    + "     CrossProduct.AREA,"
    + "     ISNULL(Base.AREANAME, (select AREANAME from GEOG where AREATYPE = ? and AREA = CrossProduct.AREA and STFIPS = '36')) as AREANAME,"
    + "     CrossProduct.PERIODYEAR,"
    + "     CrossProduct.PERIOD,"
    + "     CrossProduct.INDCODE,"
    + "     ISNULL(Base.NAICSTITLE, (select TITLE from NAICS_CODE where NAICS = CrossProduct.INDCODE)) as NAICSTITLE,"
    + "     CrossProduct.OCCCODE,"
    + "     ISNULL(Base.OCCTITLE, (select SOCTITLE from SOCCODE where SOCCODE = CrossProduct.OCCCODE )) as OCCTITLE,"
    + "     ISNULL(Base.EMPCOUNT, 0) as EMPCOUNT,"
    + "     ISNULL(Base.MEAN, 0) as MEAN,"
    + "     ISNULL(Base.ENTRYWG, 0) as ENTRYWG,"
    + "     ISNULL(Base.EXPERIENCE, 0) as EXPERIENCE,"
    + "     ISNULL(Base.MEDIAN, 0) as MEDIAN,"
    + "     Base.SUPPRESS as SUPPRESS"
    + " from"
    + "     CrossProduct left outer join Base on"
    + "     CrossProduct.AREA = Base.AREA and"
    + "     CrossProduct.INDCODE = Base.INDCODE and"
    + "     CrossProduct.OCCCODE = Base.OCCCODE"
    + " order by"
    + "     BASE.AREATYPE, AREA, INDCODE, OCCCODE, PERIODYEAR, PERIOD "
    + "";

    private static String NR_PS = ""
    + " select"
    + "			OESWAGE.AREA,"
    + "			OCCCODE,"
    + "			EMPCOUNT"
    + "		from"
    + "			OESWAGE,"
    + "			GEOG"
    + "		where"
    + "			OESWAGE.AREATYPE = ?"
    + "                 and OESWAGE.RATETYPE = ?"
    + "			and OESWAGE.AREA in (" + StringUtils.repeat("?",",",MAX_NUM_AREAS) + ")"
    + "			and OESWAGE.INDCODE in ('000000')"
    + "			and OESWAGE.OCCCODE in (" + StringUtils.repeat("?",",",MAX_NUM_OCC) + ")"
    + "			and GEOG.STFIPS = '36' and GEOG.AREATYPE = OESWAGE.AREATYPE and GEOG.AREA = OESWAGE.AREA"
    + "                 and SUPPRESS = 0"
    + " order by"
    +     " AREA, OCCCODE ";

    private static String DR_PS = ""
    + " select"
    + "			OESWAGE.AREA,"
    + "			OCCCODE,"
    + "			EMPCOUNT"
    + "		from"
    + "			OESWAGE,"
    + "			GEOG"
    + "		where"
    + "			OESWAGE.AREATYPE = ?"
    + "                 and OESWAGE.RATETYPE = ?"
    + "			and OESWAGE.AREA in (" + StringUtils.repeat("?",",",MAX_NUM_AREAS) + ")"
    + "			and OESWAGE.INDCODE in ('000000')"
    + "			and OESWAGE.OCCCODE in ('000000')"
    + "			and GEOG.STFIPS = '36' and GEOG.AREATYPE = OESWAGE.AREATYPE and GEOG.AREA = OESWAGE.AREA"
    + "                 and SUPPRESS = 0"
    + " order by"
    +     " AREA, OCCCODE ";

    private static String NS_PS = ""
    + " select"
    + "			OESWAGE.AREA,"
    + "			OCCCODE,"
    + "			EMPCOUNT"
    + "		from"
    + "			OESWAGE,"
    + "			GEOG"
    + "		where"
    + "			OESWAGE.AREATYPE = 01"
    + "                 and OESWAGE.RATETYPE = ?"
    + "			and OESWAGE.AREA in ('000036')"
    + "			and OESWAGE.INDCODE in ('000000')"
    + "			and OESWAGE.OCCCODE in (" + StringUtils.repeat("?",",",MAX_NUM_OCC) + ")"
    + "			and GEOG.STFIPS = '36' and GEOG.AREATYPE = OESWAGE.AREATYPE and GEOG.AREA = OESWAGE.AREA"
    + "                 and SUPPRESS = 0"
    + " order by"
    +     " AREA, OCCCODE ";

    private static String DS_PS = ""
    + " select"
    + "			OESWAGE.AREA,"
    + "			OCCCODE,"
    + "			EMPCOUNT"
    + "		from"
    + "			OESWAGE,"
    + "			GEOG"
    + "		where"
    + "			OESWAGE.AREATYPE = 01"
    + "                 and OESWAGE.RATETYPE = ?"
    + "			and OESWAGE.AREA in ('000036')"
    + "			and OESWAGE.INDCODE in ('000000')"
    + "			and OESWAGE.OCCCODE in ('000000')"
    + "			and GEOG.STFIPS = '36' and GEOG.AREATYPE = OESWAGE.AREATYPE and GEOG.AREA = OESWAGE.AREA"
    + "                 and SUPPRESS = 0"
    + " order by"
    +     " AREA, OCCCODE ";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void writeOutput( boolean first,
                                boolean hourly,
                                PrintWriter out,
                                String area,
                                String areaName,
                                String year,
                                String period,
                                String periodType,
                                String indcode,
                                String naicsTitle,
                                String occCode,
                                String occTitle,
                                long nNumEmployed,
                                double dAverageWage,
                                double dEntryWage,
                                double dExperienceWage,
                                double dMedianWage,
                                double dLocQuotient)
    {
        DecimalFormat dfHundredths = new DecimalFormat("#0.00");


        if(!first) {
            out.print(",");
        }
        out.print("["
                + "\"" + area + "\","
                + "\"" + areaName + "\","
                + "\"Latest\"," // + "\"" + year + "\","
                + "\"Latest\"," // + "\"" + period + "\","
                + "\"Latest\","
                + "\"" + indcode + "\","
                + "\"" + naicsTitle + "\","
                + "\"" + occCode + "\","
                + "\"" + occTitle + "\","
                + nNumEmployed + ","
                + (hourly ? Math.round(dAverageWage) : Math.round(dAverageWage / 10.0) * 10.0) + ","
                + (hourly ? Math.round(dEntryWage) : Math.round(dEntryWage / 10.0) * 10.0) + ","
                + (hourly ? Math.round(dExperienceWage) : Math.round(dExperienceWage / 10.0) * 10.0) + ","
                + (hourly ? Math.round(dMedianWage) : Math.round(dMedianWage / 10.0) * 10.0) + ","
                + dfHundredths.format(dLocQuotient) + "]");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<String, String> nrMap = new HashMap<String, String>();
        HashMap<String, String> drMap = new HashMap<String, String>();
        HashMap<String, String> nsMap = new HashMap<String, String>();
        double dsEmpCount = 0;
        if(request.getParameter("areatype") != null && request.getParameter("areatype").equalsIgnoreCase("10") && request.getParameter("resultset") != null && request.getParameter("resultset").equalsIgnoreCase("14")){
            try {
                DataSource ds = (DataSource)getServletContext().getAttribute("ds");
                con = ds.getConnection();
                ps = con.prepareStatement(NR_PS);
                int i = 0;
                String areaType = request.getParameter("areatype");
                ps.setString(++i, areaType);
                String rateType = request.getParameter("ratetype");
                ps.setString(++i, rateType);
                // Grab selected area parameters
                for(int j = 0; j < MAX_NUM_AREAS; j++) {
                    String areaParam = request.getParameter("a" + Integer.toString(j));
                    if(areaParam != null) {
                        ps.setString(++i, areaParam);
                    }
                    else {
                        ps.setNull(++i, Types.VARCHAR);
                    }
                }

                // Grab selected Occupation code parameters
                for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                    String occupationParam = request.getParameter("o" + Integer.toString(j));
                    if(occupationParam != null) {
                        ps.setString(++i, occupationParam);
                    }
                    else {
                        ps.setNull(++i, Types.VARCHAR);
                    }
                }

                rs = ps.executeQuery();

                while (rs.next()) {
                    String area = rs.getString("AREA");

                    String occcode = rs.getString("OCCCODE");
                    String numEmployed = rs.getString("EMPCOUNT");

                    nrMap.put(occcode+area, numEmployed);
                }
            }
            catch(SQLException ex) {
                ex.printStackTrace();
            }
            finally {
                if(rs != null) {
                    try {
                        rs.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(ps != null) {
                    try {
                        ps.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(con != null) {
                    try {
                        con.close();
                    }
                    catch(SQLException ex) {
                    }
                }
            }

            try {
                DataSource ds = (DataSource)getServletContext().getAttribute("ds");
                con = ds.getConnection();
                ps = con.prepareStatement(DR_PS);
                int i = 0;
                String areaType = request.getParameter("areatype");
                ps.setString(++i, areaType);
                String rateType = request.getParameter("ratetype");
                ps.setString(++i, rateType);
                // Grab selected area parameters
                for(int j = 0; j < MAX_NUM_AREAS; j++) {
                    String areaParam = request.getParameter("a" + Integer.toString(j));
                    if(areaParam != null) {
                        ps.setString(++i, areaParam);
                    }
                    else {
                        ps.setNull(++i, Types.VARCHAR);
                    }
                }

                rs = ps.executeQuery();

                while (rs.next()) {
                    String area = rs.getString("AREA");
                    String numEmployed = rs.getString("EMPCOUNT");

                    drMap.put(area, numEmployed);

                }
            }
            catch(SQLException ex) {
                ex.printStackTrace();
            }
            finally {
                if(rs != null) {
                    try {
                        rs.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(ps != null) {
                    try {
                        ps.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(con != null) {
                    try {
                        con.close();
                    }
                    catch(SQLException ex) {
                    }
                }
            }

            try {
                DataSource ds = (DataSource)getServletContext().getAttribute("ds");
                con = ds.getConnection();
                ps = con.prepareStatement(NS_PS);
                int i = 0;
                String rateType = request.getParameter("ratetype");
                ps.setString(++i, rateType);

                 // Grab selected Occupation code parameters
                for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                    String occupationParam = request.getParameter("o" + Integer.toString(j));
                    if(occupationParam != null) {
                        ps.setString(++i, occupationParam);
                    }
                    else {
                        ps.setNull(++i, Types.VARCHAR);
                    }
                }

                rs = ps.executeQuery();

                while (rs.next()) {
                    String occcode = rs.getString("OCCCODE");
                    String numEmployed = rs.getString("EMPCOUNT");

                    nsMap.put(occcode, numEmployed);
                }
            }
            catch(SQLException ex) {
                ex.printStackTrace();
            }
            finally {
                if(rs != null) {
                    try {
                        rs.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(ps != null) {
                    try {
                        ps.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(con != null) {
                    try {
                        con.close();
                    }
                    catch(SQLException ex) {
                    }
                }
            }

            try {
                DataSource ds = (DataSource)getServletContext().getAttribute("ds");
                con = ds.getConnection();
                ps = con.prepareStatement(DS_PS);
                int i = 0;
                String rateType = request.getParameter("ratetype");
                ps.setString(++i, rateType);

                rs = ps.executeQuery();

                while (rs.next()) {
                    String occcode = rs.getString("OCCCODE");
                    String numEmployed = rs.getString("EMPCOUNT");

                    dsEmpCount = Double.parseDouble(numEmployed);

                }
            }
            catch(SQLException ex) {
                ex.printStackTrace();
            }
            finally {
                if(rs != null) {
                    try {
                        rs.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(ps != null) {
                    try {
                        ps.close();
                    }
                    catch(SQLException ex) {
                    }
                }
                if(con != null) {
                    try {
                        con.close();
                    }
                    catch(SQLException ex) {
                    }
                }
            }
        }

        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();

        String callback = request.getParameter("callback");
        if(callback != null) {
            out.print(callback);
            out.print("(");
        }
        out.print("{");
        out.print("\"aaData\": [");
        
        try {
            DecimalFormat df = new DecimalFormat("##.##");

            DataSource ds = (DataSource)getServletContext().getAttribute("ds");
            con = ds.getConnection();
            ps = con.prepareStatement(LABFORCE_PS);
            int i = 0;

            String rateType = request.getParameter("ratetype");
            ps.setString(++i, rateType);

            String stateWide = request.getParameter("isstatewide");
            if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                ps.setString(++i, "000036");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            String areaType = request.getParameter("areatype");
            if(areaType != null) {
                ps.setString(++i, areaType);
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            // Grab selected area parameters
            for(int j = 0; j < MAX_NUM_AREAS; j++) {
                String areaParam = request.getParameter("a" + Integer.toString(j));
                if(areaParam != null) {
                    ps.setString(++i, areaParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }

            // Grab selected industry code parameters
            for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                String industryParam = request.getParameter("i" + Integer.toString(j));
                if(industryParam != null) {
                    ps.setString(++i, industryParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }
            // Grab selected Occupation code parameters
            for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                String occupationParam = request.getParameter("o" + Integer.toString(j));
                if(occupationParam != null) {
                    ps.setString(++i, occupationParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }
            // And one more areatype
            if(areaType != null) {
                ps.setString(++i, areaType);
            }
            else if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                ps.setString(++i, "01");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            rs = ps.executeQuery();

            int row = rs.getRow();
            boolean first = true;

            while (rs.next()) {
                String area = rs.getString("AREA");
                String areaName = rs.getString("AREANAME");
                String year = rs.getString("PERIODYEAR");
                String period = rs.getString("PERIOD");
                int nPeriod = Integer.parseInt(period);
                String indcode = rs.getString("INDCODE");
                String naicsTitle = rs.getString("NAICSTITLE").replace("\"", "");

                String occcode = rs.getString("OCCCODE");
                String occTitle = rs.getString("OCCTITLE").replace("\"", "");

                String numEmployed = rs.getString("EMPCOUNT");
                long nNumEmployed = Long.parseLong(numEmployed);
                if(rs.getString("SUPPRESS").equals("1")) {
                    nNumEmployed = -1;
                }

                String averageWage = rs.getString("MEAN");
                double dAverageWage = Double.parseDouble(averageWage);

                String entryWage = rs.getString("ENTRYWG");
                double dEntryWage = Double.parseDouble(entryWage);

                String experience = rs.getString("EXPERIENCE");
                double dExperienceWage = Double.parseDouble(experience);

                String median = rs.getString("MEDIAN");
                double dMedianWage = Double.parseDouble(median);

                //need to calculate
                double dLocQuotient = -1;
                double nr = 0;
                double dr = 0;
                double ns = 0;
                if(request.getParameter("areatype") != null && request.getParameter("areatype").equalsIgnoreCase("10") && request.getParameter("resultset") != null && request.getParameter("resultset").equalsIgnoreCase("14")){
                    if(nrMap.get(occcode+area) != null && nsMap.get(occcode) != null){
                        nr = Double.parseDouble(nrMap.get(occcode+area));
                        dr = Double.parseDouble(drMap.get(area));
                        ns = Double.parseDouble(nsMap.get(occcode));
                    }
                    /*System.out.println("nr "+occcode+" "+area+": "+nrMap.get(occcode+area));
                    System.out.println("dr "+area+": "+drMap.get(area));
                    System.out.println("nr "+occcode+": "+nsMap.get(occcode));
                    System.out.println("ds: "+dsEmpCount);*/
                    if(dr != 0 && dsEmpCount != 0 && nrMap.get(occcode+area) != null && nsMap.get(occcode) != null){
                        dLocQuotient = (nr/dr)/(ns/dsEmpCount);
                    }
                }

                writeOutput(first,
                            rateType.equals("1"),
                            out,
                            area,
                            areaName,
                            year,
                            period,
                            "2",
                            indcode,
                            naicsTitle,
                            occcode,
                            occTitle,
                            nNumEmployed,
                            dAverageWage,
                            dEntryWage,
                            dExperienceWage,
                            dMedianWage,
                            dLocQuotient);
                first = false;
            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            if(rs != null) {
                try {
                    rs.close();
                }
                catch(SQLException ex) {
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                }
                catch(SQLException ex) {
                }
            }
            if(con != null) {
                try {
                    con.close();
                }
                catch(SQLException ex) {
                }
            }
        }

        out.print("],");
        out.print("\"aoColumns\": [");
        out.print("{ \"sTitle\": \"Area\" },");
        out.print("{ \"sTitle\": \"Area Name\" },");
        out.print("{ \"sTitle\": \"Year\" },");
        out.print("{ \"sTitle\": \"Period\" },");
        out.print("{ \"sTitle\": \"Period Name\" },");
        out.print("{ \"sTitle\": \"Industry Code\" },");
        out.print("{ \"sTitle\": \"Industry Title\" },");
        out.print("{ \"sTitle\": \"Occupation Code\" },");
        out.print("{ \"sTitle\": \"Occupation Title\" },");
        out.print("{ \"sTitle\": \"Number Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Average Wage\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Entry Level Wage\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Experienced Level Wage\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Median Wage\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Location Quotient\", \"sClass\": \"ta_right\" }");

        out.print("]");
        out.print("}");
        if(callback != null) {
            out.println(");");
        }
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
