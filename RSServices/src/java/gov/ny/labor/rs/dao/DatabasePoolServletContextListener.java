/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.ny.labor.rs.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

/**
 *
 * @author Administrator
 */
public class DatabasePoolServletContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        try {
            Context initContext = new InitialContext();
            Context envContext  = (Context)initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource)envContext.lookup("jdbc/ALMIS_PARTIAL_1");
            sce.getServletContext().setAttribute("ds", ds);
        }
        catch(NamingException nex) {
            nex.printStackTrace();
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {

    }
}
