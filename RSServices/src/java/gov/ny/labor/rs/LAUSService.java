/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.ny.labor.rs;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Administrator
 */
public class LAUSService extends HttpServlet {

    private static int MAX_NUM_AREAS = 100;
    // The repeated question marks let us use a single prepared statement that the database can cache the execution plan for.
    // For any unused parameters, we'll just insert null which the database should quickly optimize away.
    private static String LABFORCE_PS = ""
    + " select"
    +     " LABFORCE.AREA,"
    +     " AREANAME,"
    +     " PERIODYEAR,"
    +     " PERIOD,"
    +     " LABORFORCE,"
    +     " EMPLAB,"
    +     " UNEMP,"
    +     " UNEMPRATE"
    + " from"
    +     " LABFORCE,"
    +     " GEOG"
    + " where"
    +     " LABFORCE.STFIPS = 36"
    +     " and LABFORCE.ADJUSTED = ?"
    +     " and LABFORCE.PERIODTYPE = ?"
    +     " and LABFORCE.PERIODYEAR >= ?"
    +     " and LABFORCE.PERIODYEAR <= ?"
    +     " and LABFORCE.PERIOD >= ?"
    +     " and LABFORCE.PERIOD <= ?"
    +     " and ((LABFORCE.AREATYPE = '01' and LABFORCE.AREA = ?) or ((LABFORCE.AREATYPE = ? or LABFORCE.AREATYPE = ?) and LABFORCE.AREA in (" + StringUtils.repeat("?",",",MAX_NUM_AREAS) + ")))"
    +     " and GEOG.STFIPS = LABFORCE.STFIPS and GEOG.AREATYPE = LABFORCE.AREATYPE and GEOG.AREA = LABFORCE.AREA"
    + " order by"
    +     " LABFORCE.AREATYPE, AREA, PERIODYEAR, PERIOD ";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();

        String callback = request.getParameter("callback");
        if(callback != null) {
            out.print(callback);
            out.print("(");
        }
        out.print("{");
        out.print("\"aaData\": [");

        //processRequest(request, response);

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DecimalFormat dfHundredths = new DecimalFormat("#0.00");
            DecimalFormat dfTenths = new DecimalFormat("#0.0");

            DataSource ds = (DataSource)getServletContext().getAttribute("ds");
            con = ds.getConnection();
            ps = con.prepareStatement(LABFORCE_PS);

            String periodType = request.getParameter("periodtype");
            String startYear = request.getParameter("startyear");
            if(startYear == null) {
                startYear = "1000";
            }
            String endYear = request.getParameter("endyear");
            if(endYear == null) {
                endYear = "3000";
            }
            String startPeriod = request.getParameter("startperiod");
            if(startPeriod == null) {
                startPeriod = "00";
            }
            String endPeriod = request.getParameter("endperiod");
            if(endPeriod == null) {
                endPeriod = "99";
            }
            String seasonallyAdjusted = request.getParameter("seasonAdjusted");
            String stateWide = request.getParameter("isstatewide");
            String areaType = request.getParameter("areatype");

            // Subtract one year from the start
            int nStartYear = Integer.parseInt(startYear);
            nStartYear--;
            startYear = Integer.toString(nStartYear);
            // Compute the number of rows to skip
            int nStartPeriod = Integer.parseInt(startPeriod);
            int nEndPeriod = Integer.parseInt(endPeriod);
            int nRowsToSkip = (periodType.equals("01")) ? 1 : (nEndPeriod - nStartPeriod) + 1;

            int i = 0;

            if(seasonallyAdjusted != null && seasonallyAdjusted.equalsIgnoreCase("1")) {
                ps.setString(++i, "1");
            }
            else {
                ps.setString(++i, "0");
            }

            ps.setString(++i, periodType);
            ps.setString(++i, startYear);
            ps.setString(++i, endYear);
            ps.setString(++i, startPeriod);
            ps.setString(++i, endPeriod);

            if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                ps.setString(++i, "000036");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            if(areaType != null) {
                ps.setString(++i, areaType);
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            if(areaType != null && areaType.equals("21")) {
                ps.setString(++i, "23");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }


            for(int j = 0; j < MAX_NUM_AREAS; j++) {
                String areaParam = request.getParameter("a" + Integer.toString(j));
                if(areaParam != null) {
                    ps.setString(++i, areaParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }

            rs = ps.executeQuery();

            String prevArea = null;
            int nPrevLaborForce = Integer.MAX_VALUE;
            int nPrevEmployment = Integer.MAX_VALUE;
            int nPrevUnemployment = Integer.MAX_VALUE;
            double dPrevUnemploymentRate = Double.MAX_VALUE;
            boolean first = true;
            
            while (rs.next()) {
                String area = rs.getString("AREA");
                if(prevArea != null && !prevArea.equals(area)) {
                    nPrevLaborForce = Integer.MAX_VALUE;
                    nPrevEmployment = Integer.MAX_VALUE;
                    nPrevUnemployment = Integer.MAX_VALUE;
                    dPrevUnemploymentRate = Double.MAX_VALUE;
                    nRowsToSkip = (periodType.equals("01")) ? 1 : (nEndPeriod - nStartPeriod) + 1;
                }
                String areaName = rs.getString("AREANAME");
                String year = rs.getString("PERIODYEAR");
                String period = rs.getString("PERIOD");
                String laborForce = rs.getString("LABORFORCE");
                int nLaborForce = Integer.parseInt(laborForce);
                String employment = rs.getString("EMPLAB");
                int nEmployment = Integer.parseInt(employment);
                String unemployment = rs.getString("UNEMP");
                int nUnemployment = Integer.parseInt(unemployment);
                String unemploymentRate = rs.getString("UNEMPRATE");
                double dUnemploymentRate = Double.parseDouble(unemploymentRate);
                // Figure out the computed values
                int nAbsoluteChangeLaborForce = (nPrevLaborForce == Integer.MAX_VALUE) ? 0 : nLaborForce - nPrevLaborForce;
                double dPercentChangeLaborForce = (nPrevLaborForce == Integer.MAX_VALUE) ? 0.0 : (double)nAbsoluteChangeLaborForce * 100.0 / (double)nPrevLaborForce;
                int nAbsoluteChangeEmployment = (nPrevEmployment == Integer.MAX_VALUE) ? 0 : nEmployment - nPrevEmployment;
                double dPercentChangeEmployment = (nPrevEmployment == Integer.MAX_VALUE) ? 0.0 : (double)nAbsoluteChangeEmployment * 100.0 / (double)nPrevEmployment;
                int nAbsoluteChangeUnemployment = (nPrevUnemployment == Integer.MAX_VALUE) ? 0 : nUnemployment - nPrevUnemployment;
                double dPercentChangeUnemployment = (nPrevUnemployment == Integer.MAX_VALUE) ? 0.0 : (double)nAbsoluteChangeUnemployment * 100.0 / (double)nPrevUnemployment;
                double dAbsoluteChangeUnemploymentRate = (dPrevUnemploymentRate == Double.MAX_VALUE) ? 0.0 : dUnemploymentRate - dPrevUnemploymentRate;


                if(--nRowsToSkip < 0) {
                    if(first) {
                        first = false;
                    }
                    else {
                        out.print(",");
                    }
                    out.print("["
                            + "\"" + area + "\","
                            + "\"" + areaName + "\","
                            + "\"" + year + "\","
                            + "\"" + period + "\","
                            + "\"" + Utils.getPeriodName(periodType, period, year) + "\","
                            + ((nLaborForce + 50) / 100 * 100) + ","    // Rounded to nearest 100
                            + ((nEmployment + 50) / 100 * 100) + ","    // Rounded to nearest 100
                            + ((nUnemployment + 50) / 100 * 100) + ","  // Rounded to nearest 100
                            + dfTenths.format(dUnemploymentRate) + ","
                            + ((nAbsoluteChangeLaborForce + 50) / 100 * 100) + ","
                            + dfTenths.format(dPercentChangeLaborForce) + ","
                            + ((nAbsoluteChangeEmployment + 50) / 100 * 100) + ","
                            + dfTenths.format(dPercentChangeEmployment) + ","
                            + ((nAbsoluteChangeUnemployment + 50) / 100 * 100) + ","
                            + dfTenths.format(dPercentChangeUnemployment) + ","
                            + dfHundredths.format(dAbsoluteChangeUnemploymentRate) + "]");
                }

                // Assign current values to previous variables
                nPrevLaborForce = nLaborForce;
                nPrevEmployment = nEmployment;
                nPrevUnemployment = nUnemployment;
                dPrevUnemploymentRate = dUnemploymentRate;
                prevArea = area;
            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            if(rs != null) {
                try {
                    rs.close();
                }
                catch(SQLException ex) {
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                }
                catch(SQLException ex) {
                }
            }
            if(con != null) {
                try {
                    con.close();
                }
                catch(SQLException ex) {
                }
            }
        }

        out.print("],");
        out.print("\"aoColumns\": [");
        out.print("{ \"sTitle\": \"Area\" },");
        out.print("{ \"sTitle\": \"Area Name\" },");
        out.print("{ \"sTitle\": \"Year\" },");
        out.print("{ \"sTitle\": \"Period\" },");
        out.print("{ \"sTitle\": \"Period Name\" },");
        out.print("{ \"sTitle\": \"Labor Force\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Unemployed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Unemployment Rate\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Absolute Change Labor Force\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Percent Change Labor Force\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Absolute Change Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Percent Change Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Absolute Change Unemployed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Percent Change Unemployed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Absolute Change Unemployment Rate\", \"sClass\": \"ta_right\" }");

        out.print("]");
        out.print("}");
        if(callback != null) {
            out.println(");");
        }
        out.close();
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * http://localhost:8080/RSServices/LAUSService?periodtype=01&startyear=2005&endyear=2010&areatype=01&area0=000036
     * http://localhost:8080/RSServices/LAUSService?periodtype=03&startyear=2003&endyear=2010&areatype=04&area0=000015&area1=000017
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
