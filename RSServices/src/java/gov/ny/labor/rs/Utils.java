/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.ny.labor.rs;

import java.text.DateFormatSymbols;

/**
 *
 * @author Administrator
 */
public class Utils {
    
    public static String getPeriodName(String periodType, String period, String year) {
        if(periodType != null && periodType.length() > 0 && period != null && period.length() > 0) {
            try {
                int nPeriodType = Integer.parseInt(periodType);
                int nPeriod = Integer.parseInt(period);

                if(nPeriodType == 2) {
                    switch(nPeriod) {
                        case 1: return year + " Q1";
                        case 2: return year + " Q2";
                        case 3: return year + " Q3";
                        case 4: return year + " Q4";
                    }
                }
                else if(nPeriodType == 3) {
                    switch(nPeriod) {
                        case 1: return year + "-" + period + " (Jan)";
                        case 2: return year + "-" + period + " (Feb)";
                        case 3: return year + "-" + period + " (Mar)";
                        case 4: return year + "-" + period + " (Apr)";
                        case 5: return year + "-" + period + " (May)";
                        case 6: return year + "-" + period + " (Jun)";
                        case 7: return year + "-" + period + " (Jul)";
                        case 8: return year + "-" + period + " (Aug)";
                        case 9: return year + "-" + period + " (Sep)";
                        case 10: return year + "-" + period + " (Oct)";
                        case 11: return year + "-" + period + " (Nov)";
                        case 12: return year + "-" + period + " (Dec)";
                    }
                }
                else {
                    return year;
                }
            }
            catch(NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }

        return "";
    }

}
