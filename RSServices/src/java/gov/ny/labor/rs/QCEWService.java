/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.ny.labor.rs;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
//import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Administrator
 */
public class QCEWService extends HttpServlet {
   
    private static int MAX_NUM_AREAS = 100;
    private static int MAX_NUM_INDUSTRIES = 150;    // There are currently 137 codes with 3 or less digits

    // The repeated question marks let us use a single prepared statement that the database can cache the execution plan for.
    // For any unused parameters, we'll just insert null which the database should quickly optimize away.
    private static String LABFORCE_PS = ""
    + "with"
    + "	Base as ("
    + "		select"
    + "			INDUSTRY_PUBLIC_VU.AREATYPE,"
    + "			INDUSTRY_PUBLIC_VU.AREA,"
    + "			AREANAME,"
    + "			PERIODYEAR,"
    + "			PERIOD,"
    + "			INDCODE,"
    + "			NAICSTITLE,"
    + "			ESTAB,"
    + "			MNTH1EMP,"
    + "			MNTH2EMP,"
    + "			MNTH3EMP,"
    + "			AVGEMP,"
    + "			TOTWAGE,"
    + "			ANN_AVG_SAL"
    + "		from"
    + "			INDUSTRY_PUBLIC_VU,"
    + "			GEOG"
    + "		where"
    + "			INDUSTRY_PUBLIC_VU.PERIODTYPE = ?"
    + "			and INDUSTRY_PUBLIC_VU.PERIODYEAR >= ?"
    + "			and INDUSTRY_PUBLIC_VU.PERIODYEAR <= ?"
    + "			and INDUSTRY_PUBLIC_VU.PERIOD >= ?"
    + "			and INDUSTRY_PUBLIC_VU.PERIOD <= ?"
    + "			and ((INDUSTRY_PUBLIC_VU.AREATYPE = '01' and INDUSTRY_PUBLIC_VU.AREA = ?) or (INDUSTRY_PUBLIC_VU.AREATYPE = ? and INDUSTRY_PUBLIC_VU.AREA in (?)))"
    + "			and INDUSTRY_PUBLIC_VU.INDCODE in (?)"
    + "			and GEOG.STFIPS = '36' and GEOG.AREATYPE = INDUSTRY_PUBLIC_VU.AREATYPE and GEOG.AREA = INDUSTRY_PUBLIC_VU.AREA"
    + "	),"
    + "	CrossProduct as ("
    + "		select AREA, PERIODYEAR, PERIOD, INDCODE"
    + "		from"
    + "			(select distinct AREA from Base) as AREA"
    + "			CROSS JOIN"
    + "			(select distinct PERIODYEAR from Base) as PERIODYEAR"
    + "			CROSS JOIN"
    + "			(select distinct PERIOD from Base) as PERIOD"
    + "			CROSS JOIN"
    + "			(select distinct INDCODE from Base) as INDCODE"
    + "	)"
    + " select"
    + "     CrossProduct.AREA,"
    + "     ISNULL(Base.AREANAME, (select AREANAME from GEOG where AREATYPE = ? and AREA = CrossProduct.AREA and STFIPS = '36')) as AREANAME,"
    + "     CrossProduct.PERIODYEAR,"
    + "     CrossProduct.PERIOD,"
    + "     CrossProduct.INDCODE,"
    + "     ISNULL(Base.NAICSTITLE, (select top 1 NAICSTITLE from INDUSTRY_PUBLIC_VU where INDCODE = CrossProduct.INDCODE)) as NAICSTITLE,"
    + "     ISNULL(Base.ESTAB, -1) as ESTAB,"
    + "     ISNULL(Base.MNTH1EMP, -1) as MNTH1EMP,"
    + "     ISNULL(Base.MNTH2EMP, -1) as MNTH2EMP,"
    + "     ISNULL(Base.MNTH3EMP, -1) as MNTH3EMP,"
    + "     ISNULL(Base.AVGEMP, -1) as AVGEMP,"
    + "     ISNULL(Base.TOTWAGE, -1) as TOTWAGE,"
    + "     ISNULL(Base.ANN_AVG_SAL, -1) as ANN_AVG_SAL"
    + " from"
    + "     CrossProduct left outer join Base on"
    + "     CrossProduct.AREA = Base.AREA and"
    + "     CrossProduct.PERIODYEAR = Base.PERIODYEAR and"
    + "     CrossProduct.PERIOD = Base.PERIOD and"
    + "     CrossProduct.INDCODE = Base.INDCODE"
    + " order by"
    + "     BASE.AREATYPE, AREA, INDCODE, PERIODYEAR, PERIOD"
    + "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void writeOutput( boolean first,
                                PrintWriter out,
                                String area,
                                String areaName,
                                String year,
                                String period,
                                String periodType,
                                String indcode,
                                String naicsTitle,
                                long nNumEstablishments,
                                long nNumEmployed,
                                long nAnnualSalary,
                                long nWeeklySalary)
    {
        if(!first) {
            out.print(",");
        }
        out.print("["
                + "\"" + area + "\","
                + "\"" + areaName + "\","
                + "\"" + year + "\","
                + "\"" + period + "\","
                + "\"" + Utils.getPeriodName(periodType, period, year) + "\","
                + "\"" + indcode + "\","
                + "\"" + naicsTitle + "\","
                + nNumEstablishments + ","
                + nNumEmployed + ","
                + nAnnualSalary + ","
                + nWeeklySalary + "]");
    }

    protected String setString(String val, String sql) {
        int firstQM = sql.indexOf("?");
        if(firstQM != -1) {
            sql = sql.substring(0, firstQM) + val + sql.substring(firstQM + 1);
        }
        return sql;
    }

    protected String setNull(String sql) {
        return setString("NULL", sql);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();

        String callback = request.getParameter("callback");
        if(callback != null) {
            out.print(callback);
            out.print("(");
        }
        out.print("{");
        out.print("\"aaData\": [");

        //processRequest(request, response);

        Connection con = null;
        Statement ps = null;
        ResultSet rs = null;
        try {
            DecimalFormat df = new DecimalFormat("##.##");

            DataSource ds = (DataSource)getServletContext().getAttribute("ds");
            con = ds.getConnection();
            ps = con.createStatement();
            String sql = LABFORCE_PS;
            String periodType = request.getParameter("periodtype");
            int nPeriodType = Integer.parseInt(periodType);
            sql = setString(periodType.equals("03") ? "02" : periodType, sql);
            String startYear = request.getParameter("startyear");
            sql = setString((startYear == null) ? "1000" : startYear, sql);
            String endYear = request.getParameter("endyear");
            sql = setString((endYear == null) ? "3000" : endYear, sql);
            String startPeriod = request.getParameter("startperiod");
            int nOriginalStartPeriod = -1;
            if(nPeriodType == 3 && (startPeriod != null)) { // Monthly
                nOriginalStartPeriod = Integer.parseInt(startPeriod);
                int nStartPeriod = nOriginalStartPeriod;
                nStartPeriod = ((nStartPeriod - 1) / 3) + 1;
                startPeriod = Integer.toString(nStartPeriod);
            }
            sql = setString((startPeriod == null) ? "00" : startPeriod, sql);
            String endPeriod = request.getParameter("endperiod");
            int nOriginalEndPeriod = -1;
            if(nPeriodType == 3 && (endPeriod != null)) { // Monthly
                nOriginalEndPeriod = Integer.parseInt(endPeriod);
                int nEndPeriod = nOriginalEndPeriod;
                nEndPeriod = ((nEndPeriod - 1) / 3) + 1;
                endPeriod = Integer.toString(nEndPeriod);
            }
            sql = setString((endPeriod == null) ? "99" : endPeriod, sql);

            String stateWide = request.getParameter("isstatewide");
            String areaType = request.getParameter("areatype");
            if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                sql = setString("000036", sql);
            }
            else {
                sql = setNull(sql);
            }
            if(areaType != null) {
                sql = setString(areaType, sql);
            }
            else {
                sql = setNull(sql);
            }
            // Grab selected area parameters
            StringBuilder areaSql = new StringBuilder();
            for(int j = 0; j < MAX_NUM_AREAS; j++) {
                String areaParam = request.getParameter("a" + Integer.toString(j));
                if(areaParam != null) {
                    if(j != 0) {
                        areaSql.append(",");
                    }
                    areaSql.append(areaParam);
                }
            }
            if(areaSql.length() > 0) {
                sql = setString(areaSql.toString(), sql);
            }
            else {
                sql = setNull(sql);
            }

            // Grab selected industry code parameters
            StringBuilder industrySql = new StringBuilder();
            for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                String industryParam = request.getParameter("i" + Integer.toString(j));
                if(industryParam != null) {
                    if(j != 0) {
                        industrySql.append(",");
                    }
                    industrySql.append(industryParam);
                }
            }
            if(industrySql.length() > 0) {
                sql = setString(industrySql.toString(), sql);
            }
            else {
                sql = setNull(sql);
            }

            // And one more areatype
            if(areaType != null) {
                sql = setString(areaType, sql);
            }
            else if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                sql = setString("01", sql);
            }
            else {
                sql = setNull(sql);
            }

            rs = ps.executeQuery(sql);

            int row = rs.getRow();
            boolean first = true;

            while (rs.next()) {
                String area = rs.getString("AREA");
                String areaName = rs.getString("AREANAME");
                String year = rs.getString("PERIODYEAR");
                String period = rs.getString("PERIOD");
                int nPeriod = Integer.parseInt(period);
                String indcode = rs.getString("INDCODE");
                String naicsTitle = rs.getString("NAICSTITLE");

                String numEstablishments = rs.getString("ESTAB");
                long nNumEstablishments = Long.parseLong(numEstablishments);
                String numEmployed = rs.getString("AVGEMP");
                long nNumEmployed = Long.parseLong(numEmployed);
                String totalWage = rs.getString("TOTWAGE");
                long nTotalWage = Long.parseLong(totalWage);
                //
                if(nPeriodType == 3) { // Monthly
                    nTotalWage *= 3;
                }
                long nAnnualSalary = (nNumEmployed == 0) ? 0 : nTotalWage / nNumEmployed;
                long nWeeklySalary = (nNumEmployed == 0) ? 0 : (nTotalWage / 52) / nNumEmployed;

                if(nPeriodType == 3) {
                    int nMonth = ((nPeriod - 1) * 3);
                    nMonth++;
                    if(nMonth >= nOriginalStartPeriod && nMonth <= nOriginalEndPeriod) {
                        writeOutput(first,
                                    out,
                                    area,
                                    areaName,
                                    year,
                                    String.format("%02d", nMonth),
                                    periodType,
                                    indcode,
                                    naicsTitle,
                                    -1,
                                    Long.parseLong(rs.getString("MNTH1EMP")),
                                    -1,
                                    -1);
                        first = false;
                    }
                    nMonth++;
                    if(nMonth >= nOriginalStartPeriod && nMonth <= nOriginalEndPeriod) {
                        writeOutput(first,
                                    out,
                                    area,
                                    areaName,
                                    year,
                                    String.format("%02d", nMonth),
                                    periodType,
                                    indcode,
                                    naicsTitle,
                                    -1,
                                    Long.parseLong(rs.getString("MNTH2EMP")),
                                    -1,
                                    -1);
                        first = false;
                    }
                    nMonth++;
                    if(nMonth >= nOriginalStartPeriod && nMonth <= nOriginalEndPeriod) {
                        writeOutput(first,
                                    out,
                                    area,
                                    areaName,
                                    year,
                                    String.format("%02d", nMonth),
                                    periodType,
                                    indcode,
                                    naicsTitle,
                                    -1,
                                    Long.parseLong(rs.getString("MNTH3EMP")),
                                    -1,
                                    -1);
                        first = false;
                    }
                }
                else {
                    writeOutput(first,
                                out,
                                area,
                                areaName,
                                year,
                                period,
                                periodType,
                                indcode,
                                naicsTitle,
                                nNumEstablishments,
                                nNumEmployed,
                                nAnnualSalary,
                                nWeeklySalary);
                    first = false;
                }
            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            if(rs != null) {
                try {
                    rs.close();
                }
                catch(SQLException ex) {
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                }
                catch(SQLException ex) {
                }
            }
            if(con != null) {
                try {
                    con.close();
                }
                catch(SQLException ex) {
                }
            }
        }

        out.print("],");
        out.print("\"aoColumns\": [");
        out.print("{ \"sTitle\": \"Area\" },");
        out.print("{ \"sTitle\": \"Area Name\" },");
        out.print("{ \"sTitle\": \"Year\" },");
        out.print("{ \"sTitle\": \"Period\" },");
        out.print("{ \"sTitle\": \"Period Name\" },");
        out.print("{ \"sTitle\": \"Industry Code\" },");
        out.print("{ \"sTitle\": \"Industry Title\" },");
        out.print("{ \"sTitle\": \"Reporting Units\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Number Employed\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Annual Salary\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Weekly Salary\", \"sClass\": \"ta_right\" }");

        out.print("]");
        out.print("}");
        if(callback != null) {
            out.println(");");
        }
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
