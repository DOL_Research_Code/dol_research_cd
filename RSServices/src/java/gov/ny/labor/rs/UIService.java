/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gov.ny.labor.rs;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author Administrator
 */
public class UIService extends HttpServlet {

    private static int MAX_NUM_AREAS = 100;
    private static int MAX_NUM_INDUSTRIES = 150;
    private static int MAX_NUM_OCCUPATIONS = 150;

    // The repeated question marks let us use a single prepared statement that the database can cache the execution plan for.
    // For any unused parameters, we'll just insert null which the database should quickly optimize away.
    private static String UICLAIMS_PS = ""
    + " select"
    +     " UICLAIMS.AREA,"
    +     " AREANAME,"
    +     " PERIODYEAR,"
    +     " PERIOD,"
    +     " INDCODE,"
    +     " TITLE as NAICSTITLE,"
    +     " OCCCODE,"
    +     " SOCCODE.SOCTITLE as OCCTITLE,"
    +     " NEWCLAIMS,"
    +     " ORIGINALCLAIMS,"
    +     " INITIALCLAIMS,"
    +     " ADDITIONALCLAIMS,"
    +     " FIRSTPAYMENTS,"
    +     " FINALPAYMENTS,"
    +     " BENEFICIARIES,"
    +     " AMOUNTPAID,"
    +     " AVGBENEFIT,"
    +     " DURATION"
    + " from"
    +     " UICLAIMS,"
    +     " GEOG,"
    +     " NAICS_CODE,"
    +     " SOCCODE"
    + " where"
    +     " UICLAIMS.STFIPS = 36"
    +     " and UICLAIMS.PERIODTYPE = ?"
    +     " and UICLAIMS.PERIODYEAR >= ?"
    +     " and UICLAIMS.PERIODYEAR <= ?"
    +     " and UICLAIMS.PERIOD >= ?"
    +     " and UICLAIMS.PERIOD <= ?"
    +     " and ((UICLAIMS.AREATYPE = '01' and UICLAIMS.AREA = ?) or (UICLAIMS.AREATYPE = ? and UICLAIMS.AREA in (" + StringUtils.repeat("?",",",MAX_NUM_AREAS) + ")))"
    +     " and UICLAIMS.INDCODE in (" + StringUtils.repeat("?",",",MAX_NUM_INDUSTRIES) + ")"
    +     " and UICLAIMS.OCCCODE in (" + StringUtils.repeat("?",",",MAX_NUM_OCCUPATIONS) + ")"
    +     " and GEOG.STFIPS = UICLAIMS.STFIPS and GEOG.AREATYPE = UICLAIMS.AREATYPE and GEOG.AREA = UICLAIMS.AREA"
    +     " and NAICS_CODE.NAICS = UICLAIMS.INDCODE"
    +     " and SUBSTRING(SOCCODE.SOCCODE, 0, 2) = SUBSTRING(UICLAIMS.OCCCODE, 0, 2)"
    +     " and AGEGROUP = '00'"
    +     " and RACEETHN = '00'"
    +     " and GENDER = '0'"
    +     " and EDUCATION = '00'"
    + " order by"
    +     " UICLAIMS.AREATYPE, AREA, PERIODYEAR, PERIOD ";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        response.setHeader("Access-Control-Allow-Origin", "*");
        PrintWriter out = response.getWriter();

        String callback = request.getParameter("callback");
        if(callback != null) {
            out.print(callback);
            out.print("(");
        }
        out.print("{");
        out.print("\"aaData\": [");

        //processRequest(request, response);

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            DecimalFormat dfTenths = new DecimalFormat("#0.0");

            DataSource ds = (DataSource)getServletContext().getAttribute("ds");
            con = ds.getConnection();
            ps = con.prepareStatement(UICLAIMS_PS);
            int i = 0;

            String periodType = request.getParameter("periodtype");
            ps.setString(++i, periodType);
            String startYear = request.getParameter("startyear");
            ps.setString(++i, (startYear == null) ? "1000" : startYear);
            String endYear = request.getParameter("endyear");
            ps.setString(++i, (endYear == null) ? "3000" : endYear);
            String startPeriod = request.getParameter("startperiod");
            ps.setString(++i, (startPeriod == null) ? "00" : startPeriod);
            String endPeriod = request.getParameter("endperiod");
            ps.setString(++i, (endPeriod == null) ? "99" : endPeriod);
            String stateWide = request.getParameter("isstatewide");
            if(stateWide != null && stateWide.equalsIgnoreCase("1")) {
                ps.setString(++i, "000036");
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }
            String areaType = request.getParameter("areatype");
            if(areaType != null) {
                ps.setString(++i, areaType);
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }

            // Grab selected area parameters
            for(int j = 0; j < MAX_NUM_AREAS; j++) {
                String areaParam = request.getParameter("a" + Integer.toString(j));
                if(areaParam != null) {
                    ps.setString(++i, areaParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }
            // Grab selected industry code parameters
            for(int j = 0; j < MAX_NUM_INDUSTRIES; j++) {
                String industryParam = request.getParameter("i" + Integer.toString(j));
                if(industryParam != null) {
                    ps.setString(++i, industryParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }
            // Grab selected Occupation code parameters
            for(int j = 0; j < MAX_NUM_OCCUPATIONS; j++) {
                String occupationParam = request.getParameter("o" + Integer.toString(j));
                if(occupationParam != null) {
                    ps.setString(++i, occupationParam);
                }
                else {
                    ps.setNull(++i, Types.VARCHAR);
                }
            }

            rs = ps.executeQuery();

            boolean first = true;

            while (rs.next()) {
                String area = rs.getString("AREA");
                String areaName = rs.getString("AREANAME");
                String year = rs.getString("PERIODYEAR");
                String period = rs.getString("PERIOD");
                String industryCode = rs.getString("INDCODE");
                String industryTitle = rs.getString("NAICSTITLE");
                String occupationCode = rs.getString("OCCCODE");
                String occupationTitle = rs.getString("OCCTITLE");
                String newClaims = rs.getString("NEWCLAIMS");
                int nNewClaims = (newClaims != null ? Integer.parseInt(newClaims) : -1);
                String originalClaims = rs.getString("ORIGINALCLAIMS");
                int nOriginalClaims = (originalClaims != null ? Integer.parseInt(originalClaims) : -1);
                String initialClaims = rs.getString("INITIALCLAIMS");
                int nInitialClaims = (initialClaims != null ? Integer.parseInt(initialClaims) : -1);
                String additionalClaims = rs.getString("ADDITIONALCLAIMS");
                int nAdditionalClaims = (additionalClaims != null ? Integer.parseInt(additionalClaims) : -1);
                String firstPayments = rs.getString("FIRSTPAYMENTS");
                int nFirstPayments = (firstPayments != null ? Integer.parseInt(firstPayments) : -1);
                String finalPayments = rs.getString("FINALPAYMENTS");
                int nFinalPayments = (finalPayments != null ? Integer.parseInt(finalPayments) : -1);
                String beneficiaries = rs.getString("BENEFICIARIES");
                int nBeneficiaries = (beneficiaries != null ? Integer.parseInt(beneficiaries) : -1);
                String amountPaid = rs.getString("AMOUNTPAID");
                long lAmountPaid = (amountPaid != null ? Long.parseLong(amountPaid) : -1);
                String averageBenefit = rs.getString("AVGBENEFIT");
                int nAverageBenefit = (averageBenefit != null ? Integer.parseInt(averageBenefit) : -1);
                String duration = rs.getString("DURATION");
                double dDuration = (duration != null ? Double.parseDouble(duration) : 0.0);

                if(first) {
                    first = false;
                }
                else {
                    out.print(",");
                }
                out.print("["
                        + "\"" + area + "\","
                        + "\"" + areaName + "\","
                        + "\"" + year + "\","
                        + "\"" + period + "\","
                        + "\"" + Utils.getPeriodName(periodType, period, year) + "\","
                        + "\"" + industryCode + "\","
                        + (industryTitle.startsWith("\"") ? "" : "\"") + industryTitle + (industryTitle.endsWith("\"") ? "," : "\",")
                        + "\"" + occupationCode + "\","
                        + (occupationTitle.startsWith("\"") ? "" : "\"") + occupationTitle + (occupationTitle.endsWith("\"") ? "," : "\",")
                        + nNewClaims + ","
                        + nOriginalClaims + ","
                        + nInitialClaims + ","
                        + nAdditionalClaims + ","
                        + nFirstPayments + ","
                        + nFinalPayments + ","
                        + nBeneficiaries + ","
                        + lAmountPaid + ","
                        + nAverageBenefit + ","
                        + dDuration + "]");

            }
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            if(rs != null) {
                try {
                    rs.close();
                }
                catch(SQLException ex) {
                }
            }
            if(ps != null) {
                try {
                    ps.close();
                }
                catch(SQLException ex) {
                }
            }
            if(con != null) {
                try {
                    con.close();
                }
                catch(SQLException ex) {
                }
            }
        }

        out.print("],");
        out.print("\"aoColumns\": [");
        out.print("{ \"sTitle\": \"Area\" },");
        out.print("{ \"sTitle\": \"Area Name\" },");
        out.print("{ \"sTitle\": \"Year\" },");
        out.print("{ \"sTitle\": \"Period\" },");
        out.print("{ \"sTitle\": \"Period Name\" },");
        out.print("{ \"sTitle\": \"Industry Code\" },");
        out.print("{ \"sTitle\": \"Industry Title\" },");
        out.print("{ \"sTitle\": \"Occupation Code\" },");
        out.print("{ \"sTitle\": \"Occupation Title\" },");
        out.print("{ \"sTitle\": \"New Claims\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Original Claims\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Initial Claims\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Additional Claims\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"First Payments\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Final Payments\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Beneficiaries\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Amount Paid\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Average Benefit\", \"sClass\": \"ta_right\" },");
        out.print("{ \"sTitle\": \"Duration\", \"sClass\": \"ta_right\" },");

        out.print("]");
        out.print("}");
        if(callback != null) {
            out.println(");");
        }
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
