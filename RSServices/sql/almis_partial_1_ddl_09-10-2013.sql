USE [ALMIS_PARTIAL_1]
GO
/****** Object:  Table [dbo].[SOCCODE]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SOCCODE](
	[SOCCODE] [varchar](6) NOT NULL,
	[SOCTITLE] [varchar](100) NULL,
	[SOCDESC] [varchar](2000) NULL,
	[SOCTITLEL] [varchar](250) NULL,
	[OOHTRNTM] [char](1) NULL,
 CONSTRAINT [PK_SOCCODE] PRIMARY KEY CLUSTERED 
(
	[SOCCODE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERIODTY]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERIODTY](
	[PERIODTYPE] [varchar](2) NULL,
	[PERTYPDESC] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERIOD]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PERIOD](
	[PERIODYEAR] [varchar](4) NULL,
	[PERIODTYPE] [varchar](2) NULL,
	[PERIOD] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OESWAGE]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OESWAGE](
	[STFIPS] [varchar](2) NOT NULL,
	[AREATYPE] [varchar](2) NOT NULL,
	[AREA] [varchar](6) NOT NULL,
	[PERIODYEAR] [varchar](4) NOT NULL,
	[PERIODTYPE] [varchar](2) NOT NULL,
	[PERIOD] [varchar](2) NOT NULL,
	[INDCODTY] [varchar](2) NOT NULL,
	[INDCODE] [varchar](6) NOT NULL,
	[OCCODETYPE] [varchar](2) NOT NULL,
	[OCCCODE] [varchar](10) NOT NULL,
	[WAGESOURCE] [char](1) NOT NULL,
	[RATETYPE] [varchar](1) NOT NULL,
	[EMPCOUNT] [numeric](10, 0) NULL,
	[RESPONSE] [numeric](6, 0) NULL,
	[MEAN] [numeric](9, 2) NULL,
	[ENTRYWG] [numeric](9, 2) NULL,
	[EXPERIENCE] [numeric](9, 2) NULL,
	[PCT10] [numeric](9, 2) NULL,
	[PCT25] [numeric](9, 2) NULL,
	[MEDIAN] [numeric](9, 2) NULL,
	[PCT75] [numeric](9, 2) NULL,
	[PCT90] [numeric](9, 2) NULL,
	[UDPCT] [numeric](3, 0) NULL,
	[UDPCTWAGE] [numeric](9, 2) NULL,
	[UDRNGLOPCT] [numeric](3, 0) NULL,
	[UDRNGHIPCT] [numeric](3, 0) NULL,
	[UDRNGMEAN] [numeric](9, 2) NULL,
	[WPCTRELERR] [numeric](6, 2) NULL,
	[EPCTRELERR] [numeric](6, 2) NULL,
	[PANELCODE] [varchar](6) NULL,
	[SUPPRESS] [varchar](1) NULL,
	[MEANSUP] [char](1) NULL,
	[ENTRYWGSUP] [char](1) NULL,
	[EXPERSUP] [char](1) NULL,
	[PCT10SUP] [char](1) NULL,
	[PCT25SUP] [char](1) NULL,
	[MEDIANSUP] [char](1) NULL,
	[PCT75SUP] [char](1) NULL,
	[PCT90SUP] [char](1) NULL,
	[UDPCTWGSUP] [char](1) NULL,
	[UDRNGMNSUP] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NAICS_CODE]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NAICS_CODE](
	[NAICS] [varchar](6) NOT NULL,
	[TITLE] [varchar](400) NULL,
	[NAICS_LEVEL] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ALMIS_NAICS] PRIMARY KEY CLUSTERED 
(
	[NAICS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LABFORCE]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LABFORCE](
	[STFIPS] [varchar](2) NOT NULL,
	[AREATYPE] [varchar](2) NOT NULL,
	[AREA] [varchar](6) NOT NULL,
	[PERIODYEAR] [varchar](4) NOT NULL,
	[PERIODTYPE] [varchar](2) NOT NULL,
	[PERIOD] [varchar](2) NOT NULL,
	[ADJUSTED] [varchar](1) NOT NULL,
	[PRELIM] [varchar](1) NOT NULL,
	[BENCHMARK] [varchar](4) NOT NULL,
	[LABORFORCE] [numeric](9, 0) NULL,
	[EMPLAB] [numeric](9, 0) NULL,
	[UNEMP] [numeric](9, 0) NULL,
	[UNEMPRATE] [numeric](5, 1) NULL,
 CONSTRAINT [LABFORCE_PK] PRIMARY KEY CLUSTERED 
(
	[STFIPS] ASC,
	[AREATYPE] ASC,
	[AREA] ASC,
	[PERIODYEAR] ASC,
	[PERIODTYPE] ASC,
	[PERIOD] ASC,
	[ADJUSTED] ASC,
	[PRELIM] ASC,
	[BENCHMARK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INDUSTRY_PUBLIC_VU]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INDUSTRY_PUBLIC_VU](
	[AREATYPE] [varchar](2) NULL,
	[AREA] [varchar](6) NULL,
	[PERIODTYPE] [varchar](2) NULL,
	[PERIODYEAR] [varchar](4) NULL,
	[PERIOD] [varchar](2) NULL,
	[NAICSECT] [varchar](3) NULL,
	[INDCODE] [varchar](6) NULL,
	[NAICSTITLE] [varchar](80) NULL,
	[ESTAB] [numeric](8, 0) NULL,
	[MNTH1EMP] [numeric](9, 0) NULL,
	[MNTH2EMP] [numeric](9, 0) NULL,
	[MNTH3EMP] [numeric](9, 0) NULL,
	[AVGEMP] [numeric](9, 0) NULL,
	[TOTWAGE] [numeric](14, 0) NULL,
	[ANN_AVG_SAL] [numeric](14, 0) NULL,
	[SUPPRESS] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INDUSTRY]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INDUSTRY](
	[STFIPS] [varchar](2) NOT NULL,
	[AREATYPE] [varchar](2) NOT NULL,
	[AREA] [varchar](6) NOT NULL,
	[PERIODYEAR] [varchar](4) NOT NULL,
	[PERIODTYPE] [varchar](2) NOT NULL,
	[PERIOD] [varchar](2) NOT NULL,
	[INDCODTY] [varchar](2) NOT NULL,
	[INDCODE] [varchar](6) NOT NULL,
	[OWNERSHIP] [varchar](2) NOT NULL,
	[FIRMS] [numeric](8, 0) NULL,
	[ESTAB] [numeric](8, 0) NULL,
	[AVGEMP] [numeric](9, 0) NULL,
	[MNTH1EMP] [numeric](9, 0) NULL,
	[MNTH2EMP] [numeric](9, 0) NULL,
	[MNTH3EMP] [numeric](9, 0) NULL,
	[TOPEMPAV] [numeric](9, 0) NULL,
	[TOTWAGE] [numeric](14, 0) NULL,
	[AVGWKWAGE] [numeric](6, 0) NULL,
	[TAXWAGE] [numeric](14, 0) NULL,
	[CONTRIB] [numeric](9, 0) NULL,
	[SUPPRESS] [varchar](1) NULL,
	[DT_STAMP] [date] NULL,
 CONSTRAINT [INDUSTRY_PK] PRIMARY KEY CLUSTERED 
(
	[STFIPS] ASC,
	[AREATYPE] ASC,
	[AREA] ASC,
	[PERIODYEAR] ASC,
	[PERIODTYPE] ASC,
	[PERIOD] ASC,
	[INDCODTY] ASC,
	[INDCODE] ASC,
	[OWNERSHIP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GEOG]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GEOG](
	[STFIPS] [varchar](2) NULL,
	[AREATYPE] [varchar](2) NULL,
	[AREA] [varchar](6) NULL,
	[AREANAME] [varchar](60) NULL,
	[AREADESC] [varchar](2000) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CES_V2_5]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CES_V2_5](
	[STFIPS] [varchar](2) NOT NULL,
	[AREATYPE] [varchar](2) NOT NULL,
	[AREA] [varchar](6) NOT NULL,
	[PERIODYEAR] [varchar](4) NOT NULL,
	[PERIODTYPE] [varchar](2) NOT NULL,
	[PERIOD] [varchar](2) NOT NULL,
	[SERIESCODE] [varchar](8) NOT NULL,
	[ADJUSTED] [varchar](1) NOT NULL,
	[BENCHMARK] [varchar](4) NULL,
	[PRELIM] [char](1) NULL,
	[EMPCES] [numeric](9, 0) NULL,
	[EMPPRODWRK] [numeric](9, 0) NULL,
	[EMPFEMALE] [numeric](9, 0) NULL,
	[HOURS] [numeric](3, 1) NULL,
	[EARNINGS] [numeric](8, 2) NULL,
	[HOUREARN] [numeric](6, 2) NULL,
	[SUPPRECORD] [varchar](1) NULL,
	[SUPPHE] [varchar](1) NULL,
	[SUPPPW] [varchar](1) NULL,
	[SUPPFEM] [varchar](1) NULL,
	[HOURSALLWRKR] [numeric](3, 1) NULL,
	[EARNINGSALLWRKR] [numeric](8, 2) NULL,
	[HOUREARNALLWRKR] [numeric](6, 2) NULL,
	[SUPPHEALLWRKR] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CES_NAICS]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CES_NAICS](
	[NAICS] [varchar](8) NULL,
	[DESCRIPTION] [varchar](100) NULL,
	[NAICSCODE] [varchar](6) NULL,
	[NCSLVL] [varchar](2) NULL,
	[SERIESCODE] [varchar](8) NULL,
	[SORTORDER] [varchar](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CES_GEOG]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CES_GEOG](
	[AREA] [varchar](6) NULL,
	[AREATYPE] [varchar](2) NULL,
	[AREANAME] [varchar](60) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CES]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CES](
	[STFIPS] [varchar](2) NOT NULL,
	[AREATYPE] [varchar](2) NOT NULL,
	[AREA] [varchar](6) NOT NULL,
	[PERIODYEAR] [varchar](4) NOT NULL,
	[PERIODTYPE] [varchar](2) NOT NULL,
	[PERIOD] [varchar](2) NOT NULL,
	[SERIESCODE] [varchar](8) NOT NULL,
	[ADJUSTED] [varchar](1) NOT NULL,
	[BENCHMARK] [varchar](4) NULL,
	[EMPCES] [numeric](9, 0) NULL,
	[EMPPRODWRK] [numeric](9, 0) NULL,
	[EMPFEMALE] [numeric](9, 0) NULL,
	[HOURS] [numeric](3, 1) NULL,
	[EARNINGS] [numeric](8, 2) NULL,
	[HOUREARN] [numeric](6, 2) NULL,
	[SUPPRECORD] [varchar](1) NULL,
	[SUPPHE] [varchar](1) NULL,
	[SUPPPW] [varchar](1) NULL,
	[SUPPFEM] [varchar](1) NULL,
	[PRELIM] [char](1) NULL,
	[EMP_TYPE] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AREATYPE]    Script Date: 09/10/2013 15:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AREATYPE](
	[STFIPS] [varchar](2) NULL,
	[AREATYPE] [varchar](2) NULL,
	[AREATYNAME] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__INDUSTRY__STFIPS__060DEAE8]    Script Date: 09/10/2013 15:20:02 ******/
ALTER TABLE [dbo].[INDUSTRY] ADD  DEFAULT ((36)) FOR [STFIPS]
GO
/****** Object:  Default [DF__INDUSTRY__SUPPRE__07020F21]    Script Date: 09/10/2013 15:20:02 ******/
ALTER TABLE [dbo].[INDUSTRY] ADD  DEFAULT ((0)) FOR [SUPPRESS]
GO
/****** Object:  Default [DF__LABFORCE__STFIPS__7F60ED59]    Script Date: 09/10/2013 15:20:02 ******/
ALTER TABLE [dbo].[LABFORCE] ADD  DEFAULT ((36)) FOR [STFIPS]
GO
