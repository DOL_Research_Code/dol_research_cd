//Area Name,Period,No Of Establishments,Employment,One-Year Employment Gain/Loss (Percent),Average Weekly Wages,On-Year Weekly Wages Gain/Loss (Percent),Industry,FIPS,USPS
var regionData = [
["Capital Region",28873,485499,-0.7,774,3.1,36001],
["Central Region",19526,333097,-1.9,693,6.1,36003],
["Finger Lakes Region",28783,572533,1.4,687,3.0,36005],
["Hudson Valley Region",73637,852504,-1.3,887,4.6,36007],
["Long Island",102046,1175400,-1.3,992,5.2,36009],
["Mohawk Valley Region",11245,187711,1.0,651,5.0,36011],
["New York City",241118,3629524,-0.8,1154,3.8,36013],
["North Country Region",10484,147068,1.5,659,7.9,36015],
["Southern Region",14697,261028,4.4,793,4.1,36017],
["Western Region",34130,607529,-2.3,680,-1.3,36019]
];