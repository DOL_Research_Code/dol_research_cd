/*
 * Copyright (c) 2012 NBT Solutions.
 */

/**
 * Startup for DOL application.
 *
 */
NBT.App.DOL = OpenLayers.Class(NBT.App, {

    /**
     * Tracking which feature is currently hovered over
     */
    hoveringOver: {},

    dataCache: {},
	//proxyUrl: "/scripts/cgi-bin/proxy.cgi?url=",
	proxyUrl: "/cgi-bin/proxy.cgi?url=",
    //proxyUrl: window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/')+1) + 'simpleproxy.php?mode=native&url=',

    appDir: window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/')+1),

    /**
     * Selectable themes. This should be set in an ajax-pulled file.
     */
    thematicData: {
      none: {
        layer: 'Counties',
        title: 'County Population',
        headers: [ 'Name', 'Year 2000', 'Year 2010' ],
        idIndex: 0,
        idFieldName: 'NAME', // name of the field @ gis cloud.
        fields: [ 'Name', 'pop2000', 'pop2010' ],
        data: null
      },

      region_employment: {
        layer: 'Regions',
        title: '2011Q1 Regional Employment',
        url: 'http://localhost:10088/DOL/NBT_Demo/icf.nbtsolutions.net_files/icf.nbtsolutions.net/demo/data/qcew_lmr_2011_q1_data.js',
        headers: [ 'Name', 'Establishments', 'Total Employment' ],
        idIndex: 0, // where the ID field is in the fields in data.
        idFieldName: 'LM_REGION', // name of the field @ gis cloud.
		classifyIndex: 2,
        fields: [ 'Name', 'Establishments', 'Employed' ],
        data: null
      },

      region_establishments: {
        layer: 'Regions',
        title: '2011Q1 Regional Establishments',
        url: 'http://localhost:10088/DOL/NBT_Demo/icf.nbtsolutions.net_files/icf.nbtsolutions.net/demo/data/qcew_lmr_2011_q1_data.js',
        headers: [ 'Name', 'Establishments', 'Total Employment' ],
        idIndex: 0, // where the ID field is in the fields in data.
        idFieldName: 'LM_REGION', // name of the field @ gis cloud.
        classifyIndex: 1,
        fields: [ 'Name', 'Establishments', 'Employed' ],
        data: null
      },

      region_avgwage: {
        layer: 'Regions',
        title: '2011Q1 Average Weekly Wage',
        url: 'http://localhost:10088/DOL/NBT_Demo/icf.nbtsolutions.net_files/icf.nbtsolutions.net/demo/data/qcew_lmr_2011_q1_data.js',
        headers: [ 'Name', 'Total Employment', 'Weekly Wage' ],
        idIndex: 0, // where the ID field is in the fields in data.
        idFieldName: 'LM_REGION', // name of the field @ gis cloud.
		classifyIndex: 4,
        fields: [ 'Name', 'unused', 'Employed', 'unused', 'Weekly Wage' ],
        data: null
      },

      employment: {
        layer: 'Counties',
        title: 'Employment, 2010',
        url: 'http://localhost:10088/DOL/NBT_Demo/icf.nbtsolutions.net_files/icf.nbtsolutions.net/demo/data/qcew_county_2011_q1_data.js',
        headers: [ 'Name', 'Establishments', 'Total Employment' ],
        idIndex: 0, // where the ID field is in the fields in data.
        idFieldName: 'NAME', // name of the field @ gis cloud.
        classifyIndex: 2,
        fields: [ 'NAME', 'Establishments', 'Employed' ],// 'Emp Change', 'Wages', 'Wage Change', 'FIPS' ],
        data: null
      },

      establishments: {
        layer: 'Counties',
        title: 'Establishments, 2010',
        url: 'http://localhost:10088/DOL/NBT_Demo/icf.nbtsolutions.net_files/icf.nbtsolutions.net/demo/data/qcew_county_2011_q1_data.js',
        headers: [ 'Name', 'Establishments', 'Total Employment' ],
        idIndex: 0, // where the ID field is in the fields in data.
        idFieldName: 'NAME', // name of the field @ gis cloud.
        classifyIndex: 1,
        fields: [ 'NAME', 'Establishments', 'Employed' ],//, 'Emp Change', 'Wages', 'Wage Change', 'FIPS' ],
        data: null
      }
    },

    currentTheme: 'none',

    startingTheme: 'none',

    /**
     * Maintain selected features through zoom and featureType changes.
     */
    selectedFeatures: [],

    /**
     * Lookup map using GIS Cloud layer Name -> ID to select the 'right' layer
     * when selecting a theme.
     */
    layerIdNames: null,

    preMapInit: function()
    {
      var themeSelect = $('#theme');
      themeSelect.val('none');
      // TODO: allow this by query string.
      var selected = themeSelect.val();
      this.startingTheme = selected;

      //set up on-click
      themeSelect.change(function() {
          app.selectTheme(themeSelect.val());
      });

      // set up 'legend' click:
      $('#legendbox h5').click(function() {
          $('#legend').toggle(250);
          $('#legendbox #x').toggle(250);
      });
      $('#legendbox #x').click(function() {
          $('#legend').hide(250);
          $('#legendbox #x').hide(250);
      });
    },

    getMapOptions: function()
    {
      return {
        layers: [new OpenLayers.Layer.OSM()],
        zoom: 6,
        center: OpenLayers.Layer.SphericalMercator.forwardMercator(-75.9271,42.7818),
        controls: [
          //update controls to add in 'predictive' zoom so we can swap out data before doing it.
          new OpenLayers.Control.Navigation({
              defaultDblClick: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = app.map.zoom + 1;
                OpenLayers.Control.Navigation.prototype.defaultDblClick.apply(this, arguments);
              },
              defaultDblRightClick: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = app.map.zoom - 1;
                OpenLayers.Control.Navigation.prototype.defaultDblRightClick.apply(this, arguments);
              },
              wheelChange: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = app.map.zoom; // Beats me I don't have a wheel :)
                OpenLayers.Control.Navigation.prototype.wheelChange.apply(this, arguments);
              }
          }),
          new OpenLayers.Control.Zoom({
              onZoomClick: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = (e.buttonElement.hash == '#zoomIn') ? app.map.zoom + 1 : map.zoom - 1;
                OpenLayers.Control.Zoom.prototype.onZoomClick.apply(this, arguments);
              }
          })
        ]
      };
    },

    postMapInit: function()
    {
      var baseStyle = {
        strokeWidth: 2,
        strokeColor: '#a36d00',
        fillColor: '#e7ffb3',
        fillOpacity: 0.4
      };

      var lyr = new OpenLayers.Layer.Vector('Counties', {
          strategies: [new OpenLayers.Strategy.BBOX()],
          extractAttributes: true,
          styleMap: new OpenLayers.StyleMap({ 'default': baseStyle }),
          projection: new OpenLayers.Projection('EPSG:3857'),
          visibility: true,
          protocol: new OpenLayers.Protocol.WFS({
              url: 'http://23.23.167.112:8080/geoserver/cite/ows?service=WFS&',//version=1.1.0&request=GetFeature&typeName=test:cnty',
              featureType: 'NY_Counties_075',//'cnty1000',
              featureNS: 'cite',
              srsName: 'EPSG:3857',
              srsNameInQuery: true,
              outputFormat: 'json',
              readFormat: new OpenLayers.Format.GeoJSON()
          })
      });



      // fired by selectFeature below.
      lyr.events.register('featureselected', this, this.selectFeature);
      lyr.events.register('featureunselected', this, this.onFeatureRemoved);
      lyr.events.register('moveend', this, function(e) {
          if (this.currentTheme == 'none')
          {
            return;
          }

          var theme = this.thematicData[this.currentTheme];
          var sf = this.getSelectControl();

          $('#data table tr').each(function() {
              var row = $(this);
              var rowf = row.data('feature');
              if (rowf)
              {
                var name = rowf.attributes[theme.idFieldName];
                for (var i = 0; i < lyr.features.length; i++)
                {
                  if (lyr.features[i].attributes[theme.idFieldName] == name)
                  {
                    lyr.features[i].noHover = true;
                    row.data('feature', lyr.features[i]);
                    sf.select(lyr.features[i]);
                  }
                }
              }
          });
      });
      lyr.events.register('refresh', this, function(e) {
      });

      this.map.addLayer(lyr);

      // add Regions layer.
      var lyr2 = new OpenLayers.Layer.Vector('Regions', {
          strategies: [new OpenLayers.Strategy.BBOX()],
          extractAttributes: true,
          styleMap: new OpenLayers.StyleMap({ 'default': baseStyle }),
          projection: new OpenLayers.Projection('EPSG:3857'),
          visibility: true,
          protocol: new OpenLayers.Protocol.WFS({
              url: 'http://23.23.167.112:8080/geoserver/cite/ows?service=WFS&',//version=1.1.0&request=GetFeature&typeName=test:cnty',
              featureType: 'NY_LaborMarketRegions_075',//'cnty1000',
              featureNS: 'cite',
              srsName: 'EPSG:3857',
              srsNameInQuery: true,
              outputFormat: 'json',
              readFormat: new OpenLayers.Format.GeoJSON()
          })
      });


      // fired by selectFeature below.
      lyr2.events.register('featureselected', this, this.selectFeature);
      lyr2.events.register('featureunselected', this, this.onFeatureRemoved);
      lyr2.events.register('moveend', this, function(e) {
          if (this.currentTheme == 'none')
          {
            return;
          }

          var theme = this.thematicData[this.currentTheme];
          var sf = this.getSelectControl();

          $('#data table tr').each(function() {
              var row = $(this);
              var rowf = row.data('feature');
              if (rowf)
              {
                var name = rowf.attributes[theme.idFieldName];
                for (var i = 0; i < lyr2.features.length; i++)
                {
                  if (lyr2.features[i].attributes[theme.idFieldName] == name)
                  {
                    lyr2.features[i].noHover = true;
                    row.data('feature', lyr2.features[i]);
                    sf.select(lyr2.features[i]);
                  }
                }
              }
          });
      });
      lyr2.events.register('refresh', this, function(e) {
      });

      this.map.addLayer(lyr2);

	  // add Regions layer end.
	  
	  this.layerUpdater = new Array();
      this.layerUpdater[0] = new NBT.Thematic.LayerUpdater({layer: lyr});
      this.layerUpdater[1] = new NBT.Thematic.LayerUpdater({layer: lyr2});

      this.map.events.register('movestart', this, function(e) {
          if (this.map.zoomChanged)
          {
            //this.layerUpdater[0].updateFeatureTypeOnZoom();
          }
          this.map.zoomChanged = false; // because movestart event happens on a drag as well.
      });

      var selectFeature = new OpenLayers.Control.SelectFeature(
          this.map.getLayersByClass(/Vector/), // getFeatureLayers eventually
          {
            selectStyle: {
              strokeColor: '#ffdd00',
              fillColor: '#ff9900',
              fillOpacity: 0.6
            },
            toggle: true,
            multiple: true,
            hover: true,
            highlightOnly: true,
            outFeature: function(feature) {
              // override to allow hover + click highlighting
              var selected = (OpenLayers.Util.indexOf(
                feature.layer.selectedFeatures, feature) > -1);
              if(!selected) {
                this.unhighlight(feature);
              }
              app.unHoverFeature();
            },
            clickFeature: function(feature) {
              // override to allow hover + select toggle on click.
              var selected = (OpenLayers.Util.indexOf(
                feature.layer.selectedFeatures, feature) > -1);
              if(selected) {
                  if(this.toggleSelect()) {
                      this.unselect(feature);
                  } else if(!this.multipleSelect()) {
                      this.unselectAll({except: feature});
                  }
              } else {
                  if(!this.multipleSelect()) {
                      this.unselectAll({except: feature});
                  }
                  if (app.currentTheme != 'none')
                  {
                    this.select(feature);
                  }
              }
            }
          });
      selectFeature.handlers['feature'].stopDown = false;
      selectFeature.events.register('featurehighlighted', this, function(e) {
          if (this.shouldDoHover(e.feature))
          {
            this.hoverInterval = setInterval(function() {
                e.mouseX = app.mouseX;
                e.mouseY = app.mouseY;
                app.hoverFeature(e);
                clearInterval(app.hoverInterval);
                app.hoverInterval = null;
            }, 500);
          }
        });
      selectFeature.events.register('featureunhighlighted', this, this.unHoverFeature);

      this.map.addControl(selectFeature);
      selectFeature.activate();

      $(document).mousemove( function(e) {
          app.mouseX = e.pageX;
          app.mouseY = e.pageY;
      });
    },

    /**
     * We can't use the standard selectedFeatures array because the features
     * will get swapped out as the user zooms in and out. So, we maintain the
     * names of the features here.
     */
    isFeatureSelected: function(id) {
      return this.selectedFeatures.indexOf(id);
    },

    getSelectControl: function() {
      return this.map.getControlsByClass(/SelectFeature/)[0];
    },

    selectFeature: function(e) {
      if (this.currentTheme == 'none')
      {
        this.unSelectFeature(e);
      }
      else
      {
        if (e.feature)
        {
		  this.onFeatureAdded(e.feature);
        }
        else
        {
          this.statusMessage('No feature data to display');
        }
      }

    },

    /**
     * Unselect a feature - need seperate access to unselect a single
     * feature by clicking the X in the tabular data.
     */
    unselectFeature: function(e)
    {
      e.feature = this.getFeatureByFeature(e.feature);
      this.getSelectControl().unselect(e.feature);
      this.onFeatureRemoved(e);
    },

    /**
     * Problem is, when switching featureTypes and loading new Vectors, the
     * OL classes can get 'lost'. But we can look up the feature we want in the
     * current set by using the name of the feature we had originally.
     * // XXX doesn't appear to be working.
     */
    getFeatureByFeature: function(feature)
    {
      if (feature.layer) return feature; // if layer is still attached we're ok

      if (this.currentTheme == 'none') return feature;
      var theme = this.thematicData[this.currentTheme];
      var id = feature.attributes[theme.idFieldName];
      for (var i = 0; i < this.map.layers[1].features; i++)
      {
        if (this.map.layers[1].features[i].attributes[id] == feature.attributes[id])
        {
          return this.map.layers[1].features[i];
        }
      }

      return feature; // just avoiding nulls.
    },

    hoverFeature: function(e)
    {
      if (e.feature)
      {
        var fld = 'NAME'; // TODO: expand this to getIdField() and reuse.
        if (this.currentTheme != 'none')
        {
          fld = this.thematicData[this.currentTheme]['idFieldName'] || 'name';
        }
        var location = e.feature.attributes[fld];
        var hover = $('#hover');
        hover.data('feature', e.feature);
        hover.css('top', e.mouseY + 5);
        hover.css('left', e.mouseX + 5);
        hover.html('<h3>' + location + '</h3>').show();
        var ul = $('<ul>');

        if (this.currentTheme != 'none')
        {
          var theme = this.thematicData[this.currentTheme];
          for (var i = 0; i < theme.headers.length; i++)
          {
            if (i != theme['idIndex'] && theme.fields[i] != 'unused') // ignore ID and unused fields for this
            {
              ul.append('<li><strong>' + theme.headers[i] + ':</strong> ' + e.feature.attributes[theme.fields[i]] + '</li>');
            }
          }
          hover.append(ul);
		alert("hovering :"+location);
//		highlightChart(location);
		}
        else
        {
          //hover.append('<ul><li><strong>2000 Pop.:</strong> ' + e.data.pop2000 + '</li></ul>');
        }
      }
    },

    /**
     * What to do when a feature is no longer hovered-over.
     */
    unHoverFeature: function(e)
    {
      clearInterval(app.hoverInterval);
      app.hoverInterval = null;
      setTimeout(function() {
          app.clearHover();
      }, 500);
    },

    statusMessage: function(message)
    {
      alert(message); // meant to be replaced.
    },

    onFeatureAdded: function(feature)
    {
      var attributes = feature.attributes;
      var theme = this.thematicData[this.currentTheme];

      var found = false;
      var name = attributes[theme.idFieldName];
							   
	alert("clicked :"+name);
	//adjustFilterForMap(name);
					   
      /*for (var i = 0; i < this.selectedFeatures.length; i++)
      {
        if (this.selectedFeatures[i] == name)
        {
          found = true;
        }
      }
      if (!found) this.selectedFeatures.push(name);

      // first, make sure there's a ul to
      var table = $('#data table');
      if (table.length == 0)
      {
        var d = $('#data');
        d.empty();
        //d.parent().prepend
        d.append('<h3>Theme: ' + theme.title + '</h3>');
        table = $('<table>');
        var tr = $('<tr>');
        for (var i = 0; i < theme.headers.length; i++)
        {
          tr.append($('<th>').text(theme.headers[i]));
        }
        tr.append('<th title="Remove row">X</th>');
        table.append(tr);
        d.append(table);
      }

      // add it as a row to the tabular data, only if it's not already there:
      var intable = false;
      $('#data table tr').each(function() {
          var row = $(this);
          var rowf = row.data('feature');
          if (rowf && rowf.attributes[theme.idFieldName] == name) // from above
          {
            intable = true;
          }
      });
      if (intable) return;

      var tr = $('<tr>');
      tr.data('feature', feature);

      for (var i = 0; i < theme.headers.length; i++)
      {
        if (theme.fields[i] != 'unused')
        {
          tr.append($('<td>').text(attributes[theme.fields[i]]));
        }
      }
      var x = $('<td>').text('X');
      x.css('cursor', 'pointer');
      x.click(function() {
          var row = $(this).parent();
          app.unselectFeature({feature: feature});
      });
      tr.append(x);

      table.append(tr);

      var ex = $('#export');
      if (ex.length == 0)
      {
        var u = $('<div id="data-tools" class="ux-button-section">');
        var d = $('<div class="ux-button-container">');
        u.append(d);
        ex = $('<input type="submit" class="button page" id="export">');
        ex.val('Export Data (json)');
        ex.click( function() {
            alert( JSON.stringify(app.exportData()) );
        });
        d.append(ex);
        $('#databox').append(u);
      }
*/
    },

    onFeatureRemoved: function(e)
    {
      var theme = this.thematicData[this.currentTheme];
      var toRemove = e.feature.attributes[theme.idFieldName];
      for (var i = 0; i < this.selectedFeatures.length; i++)
      {
        if (this.selectedFeatures[i] == toRemove)
        {
          this.selectedFeatures.splice(i, 1);
          break;
        }
      }

      $('#data table tr').each(function() {
          var row = $(this);
          if (row.data('feature') == e.feature)
          {
            row.remove();
            if ($('#data table tr').length <= 1) // only the header row.
            {
              $('#data-tools').remove();
            }
          }
      });
    },

    getSelectedFeatures: function() {
      // TODO: Consider adding the thematic details like title and field names
      return this.selectedFeatures;
    },

    /**
     * Should we show the hover popup? Answer is yes unless it's already showing
     * or if we're re-highlighting features based on a zoom/data change.
     */
    shouldDoHover: function(feature) {
      // a temporary setting from re-highlighting features on zoom/data change:
      if (feature.noHover)
      {
        feature.noHover = false;
        return false;
      }

      if (feature.id != this.hoveringOver.featureId ||
        feature.layer.name != this.hoveringOver.layerName)
      {
        this.hoveringOver = { layerName: feature.layer.name, featureId: feature.id };
        if (this.hoverInterval)
        {
          clearInterval(this.hoverInterval);
        }
        return true;
      }

      return false;
    },

    clearHover: function() {
      $('#hover').html('').hide();
      this.hoveringOver = {};
    },

    selectTheme: function(theme) {
      
	  var legendtext='';
	  // drop all selected features, ensure a fresh start:
      this.getSelectControl().unselectAll();

      if (this.thematicData[theme])
      {
        this.currentTheme = theme;
        var meta = this.thematicData[theme];
        if (meta['url'])
        {
          $.ajax({
            url: meta['url']
          }).done( function(data) {
            eval(data);
            
            //var meta = this.thematicData[theme];
            var theLayerUpdater;
            var keyFieldName;
        	if (meta['url'])
        	{
          		if(meta['url'].indexOf('ounty') != -1) {
         	   		meta.data = countyData;
         	   		keyFieldName = meta.idFieldName;
         	   		theLayerUpdater = app.layerUpdater[0];
          		}
          		else {
            		meta.data = regionData;
         	   		keyFieldName = meta.idFieldName;
         	   		theLayerUpdater = app.layerUpdater[1];
          		}
          	}
            
            //meta.data = dolData;

            theLayerUpdater.addArrayData(
              meta.fields,
              keyFieldName,
              meta.data);
/*
            app.layerUpdater.addArrayData(
              meta.fields,
              'NAME',
              meta.data);
*/
            var cl = new NBT.Thematic.Classifier();
            var classes = cl.calculateClasses(meta.data, meta.classifyIndex, 5);

            var baseStyle = {
              strokeWidth: 1,
              strokeColor: '#ffdd00',
              fillOpacity: 0.5
            };

            var colors = [ '#ff0000', '#dd0000', '#bb0000', '#990000', '#770000' ];
            for (var i in colors )
            {
              classes[i].style = {
                fillColor: colors[i]
              };
            }

            var rules = cl.buildRules(meta.fields[meta.classifyIndex], classes, baseStyle);
            var layers = app.map.getLayersByName(meta.layer);
            for (var i = 0; i < layers.length; i++)
            {
              layers[i].styleMap.styles['default'].addRules(rules);
              layers[i].redraw();
            }
				
				  legendtext +='<ul style="margin: 0; margin-bottom: 5px; padding: 0; list-style: none;">';
			
			for (var i = 0; i < classes.length; i++)
			{
				 
				  legendtext += '<li style="font-size: 80%; list-style: none; margin-left: 0; line-height: 18px; margin-bottom: 2px;"><span style="background-color:'+colors[i]+';display: block; float: left; height: 16px; width: 30px; margin-right: 5px; margin-left: 0; border: 1px solid #999; opacity: 0.5;"></span>'+classes[i].low + ' - ' +classes[i].high + '</li>';
			}
				  legendtext +='</ul>';
				  // change the legend:
				  var leg = $('#legend');
				  leg.empty();
				  if (theme != 'none')
				  {
				  leg.append('<h6>' + meta['title'] + '</h6>');
				  //leg.append('<img src="img/' + theme + '.png" />');
				  leg.append(legendtext);
				  }
				  else
				  {
				  leg.append('<h6>Select a theme</h6>');
				  leg.hide(250);
				  $('#legendbox #x').hide(250);
				  }	  	  

          }); // load the thematic data
        }

        for (var i = 1; i < this.map.layers.length; i++)
        {
          if (this.map.layers[i].name == meta.layer || this.map.layers[i].name == "OpenStreetMap") {
            this.map.layers[i].setVisibility(true);
          }
          else
          {
            this.map.layers[i].setVisibility(false);
          }
        }

        var d = $('#data');
        d.empty();
        var text = 'Click an area to include its data here...';
        if (theme == 'none')
        {
          text = 'Choose a theme then click features to select data';
        }
        d.append('<p class="ux-msg">' + text + '</p>');

        
      }
      else
      {
        alert('Unknown theme chosen');
      }

    },

    /**
     * Grab selected data, which uses this.selectedFeatures to grab the current
     * set, and this.thematicData to pull 'interesting' attributes (and headers)
     * all thrown together in a single object suitable for json'ing.
     * TODO: consider finding the polygon and including it. can't do it now
     * because the giscloud output is in screen coords.
     */
    exportData: function() {
      var data = {};
      if (this.currentTheme == 'none' ||
        typeof(this.thematicData[this.currentTheme]) == 'undefined')
      {
        return data;
      }

      data.headers = this.thematicData[this.currentTheme].headers;
      data.rows = [];
      for (var i = 0; i < this.selectedFeatures.length; i++)
      {
        var base = this.selectedFeatures[i];
        // has to be in data cache or you wouldn't be here:
        var feat = this.dataCache[base.layerId][base.featureId];
        if (typeof(feat) != 'undefined' && feat.thematicData)
        {
          data.rows.push(feat.thematicData);
        }
      }

      return data;
    }

});
$(document).ready(function() {
    window.app = new NBT.App.DOL();
});

