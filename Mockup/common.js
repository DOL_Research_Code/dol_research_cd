google.load('visualization', '1', {
	packages: ['coreChart']
});
var globalJson;
var visualization;
var globalChartData;
var globalGroupedData;

var regionCounties = {
	"Capital Region": ["000001", "000021", "000039", "000083", "000091", "000093", "000113", "000115"],
	"Central Region": ["000011", "000023", "000053", "000067", "000075"],
	"Finger Lakes Region": ["000037", "000051", "000055", "000069", "000073", "000099", "000117", "000121", "000123"],
	"Hudson Valley Region": ["000027", "000071", "000079", "000087", "000105", "000111", "000119", ],
	"Long Island Region": ["000059", "000103"],
	"Mohawk Valley Region": ["000035", "000043", "000057", "000065", "000077", "000095"],
	"New York City Region": ["000005", "000047", "000061", "000081", "000085"],
	"North Country Region": ["000019", "000031", "000033", "000041", "000045", "000049", "000089"],
	"Southern Tier Region": ["000007", "000015", "000017", "000025", "000097", "000101", "000107", "000109"],
	"Western New York Region": ["000003", "000009", "000013", "000029", "000063"]
};

//
// Generic helpers
//
function isNonNullData(data) {
	var dataColumnIndex = parseInt($('#resultset').val(), 10);
	for (var j = 0; j < data.length; j++) {
		for (var colIndex = 0; colIndex < data[j].length; colIndex++) {
			if (data[j][dataColumnIndex] != null) {
				return true;
			}
		}
	}
	return false;
}

function arrayToString(values) {
	return values.toString();
}

function formatNumber(x) {
	if (x != null) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		if (parts.length > 1) {
			parts[1] = parts[1].substring(0, 3);
		}
		return parts.join(".");
	}
	else {
		return "";
	}
}

function formatPositiveNumber(x) {
	if(x < 0) {
		return "N/A";
	}
	else {
		return formatNumber(x);
	}
}

function formatNumberExactlyOneDecimalPlace(x) {
	return parseFloat(x).toFixed(1);
}

function formatDollar(x) {
	if(x > 0) {
		return "\$" + formatNumber(x);
	}
	else {
		return "N/A";
	}
}

function formatLargeDollar(x) {
	if(x > 187200) {
		return ">\$187,200" ;
	}
	else {
		return formatDollar(x);
	}
}

function formatPercent(x) {
	return formatNumberExactlyOneDecimalPlace(x) + "%";
}

//
// Parameters tab functions
//


function isInList(OptionList, target) {
	for (var x = OptionList.length - 1; x >= 0; x--) {
		if(target === OptionList[x].text) {
			return true;
		}
	}
	return false;
}

function clearOptions(OptionList) {
	// Always clear an option list from the last entry to the first
	for (var x = OptionList.length; x >= 0; x--) {
		OptionList[x] = null;
	}
}

function addToOptionList(optionList, optionValue, optionText) {
	// Add option to the bottom of the list
	optionList[optionList.length] = new Option(optionText, optionValue);
}

function addToOptionListSelected(optionList, optionValue, optionText) {
	// Add option to the bottom of the list
	optionList[optionList.length] = new Option(optionText, optionValue, false, true);
}

function addToSelected() {	
	$('#listofchoices option:selected').appendTo('#listofchosen');
	$("#listofchosen").html($("#listofchosen option").sort(function (a, b) {
		return a.value == b.value ? 0 : a.value < b.value ? -1 : 1
	}))
	return false;
}

function addAllToSelected() {
	$('#listofchoices option').appendTo('#listofchosen');
	$("#listofchosen").html($("#listofchosen option").sort(function (a, b) {
		return a.value == b.value ? 0 : a.value < b.value ? -1 : 1
	}))
	return false;
}

function removeFromSelected() {
	$('#listofchosen option:selected').appendTo('#listofchoices');
	$("#listofchoices").html($("#listofchoices option").sort(function (a, b) {
		return a.value == b.value ? 0 : a.value < b.value ? -1 : 1
	}))
	return false;
}

function removeAllFromSelected() {
	$('#listofchosen option').appendTo('#listofchoices');
	$("#listofchoices").html($("#listofchoices option").sort(function (a, b) {
		return a.value == b.value ? 0 : a.value < b.value ? -1 : 1
	}))
	return false;
}

//
// Map functions
//
function drawMap() {
	var data = $.extend(true, [], globalJson);

	if(window.app instanceof NBT.App.DOL) {
		var dataColumnIndex = parseInt($('#resultset').val(), 10);
		var regionType = parseInt($('#regiontype').val(), 10);
		var theme = 'county';
		if (regionType == 10) {
			theme = 'region';
		}
		else if (regionType == 15) {
			theme = 'wia';
		}
		else if (regionType == 21) {
			theme = 'msa';
		}
		window.app.selectThemeWithData(theme, data['aaData'], data['aoColumns'], 'Title Here', 1, dataColumnIndex);
	}
}


//
// Chart functions
//

function getUniqueChartValues(chartData, columnIndex) {
	var uniques = [];
	var numRows = chartData.getNumberOfRows();
	for(i = 0; i < numRows; i++) {
		var val = chartData.getValue(i, columnIndex);
		if($.inArray(val, uniques) == -1) {
			uniques.push(val);
		}
	}
	
	return uniques;
}

function drawChart() {
	var data = $.extend(true, [], globalJson);
	var chartData = google.visualization.arrayToDataTable(data["aaData"], true);
	globalChartData = chartData;
	var indexParamsObj = { groupByColumnArr: [], columnDisplayColumnIndex: 0, rowDisplayColumnIndex: 0};
	loadChartIndexParams($('#primaryaxis').val(), indexParamsObj);
	var distinctColumnValues = getUniqueChartValues(chartData, indexParamsObj.columnDisplayColumnIndex);
	var numDistinctColumnValues = distinctColumnValues.length;
	var distinctRowValues = chartData.getDistinctValues(indexParamsObj.rowDisplayColumnIndex);
	var numDistinctRowValues = distinctRowValues.length;
	var dataColumnIndex = parseInt($('#resultset').val(), 10); // The column that contains the actual targeted data specified on the parameters tab
	// Build up the additional columns, two columns for each data item (one for the comma delimited string of all values, one for the correct value)
	var columnObjArr = [];
	for (h = 0; h < numDistinctColumnValues; h++) {
		var strColumnObj = {
			'column': dataColumnIndex,
			'aggregation': arrayToString,
			'type': 'string'
		};
		columnObjArr.push(strColumnObj);
		var numColumnObj = {
			'column': dataColumnIndex,
			'aggregation': google.visualization.data.count,
			'type': 'number',
			'label': distinctColumnValues[h]
		};
		columnObjArr.push(numColumnObj);
	}
	// Make the google charts group-by call
	var groupedData = google.visualization.data.group(
		chartData,
		indexParamsObj.groupByColumnArr,
		columnObjArr
	);
	// Copy the proper value(s) from the list of values into the next column
	for (i = 0; i < groupedData.getNumberOfRows(); i++) {
		// Set the correct row header (column 0) string
		groupedData.setValue(i, 0, distinctRowValues[i]);
		var jj = 0;
		for (j = indexParamsObj.groupByColumnArr.length; j < groupedData.getNumberOfColumns(); j += 2) {
			var str = groupedData.getValue(i, j);
			var arr = str.split(',');
			var val = 0;
			for (k = 0; k < arr.length / numDistinctColumnValues; k++) {
				// Wish there was a nicer way to express this rather than having to special case 
				if ($('#primaryaxis').val() != 2) {
					val += parseFloat(arr[(k * numDistinctColumnValues) + jj], 10)
				}
				else {
					val += parseFloat(arr[(jj * (arr.length / numDistinctColumnValues)) + k], 10)
				}
			}
			groupedData.setValue(i, j + 1, val);
			jj++;
		}
	}
	// Remove every other column working right to left
	var formatter = new google.visualization.NumberFormat({
		pattern: '###,###.###'
	});
	for (j = groupedData.getNumberOfColumns() - 2; j >= indexParamsObj.groupByColumnArr.length; j -= 2) {
		// Format numeric columns with commas
		formatter.format(groupedData, j + 1);
		// Remove column
		groupedData.removeColumn(j);
	}
	// Hack for when groupByColumnArr.length > 1
	for (k = indexParamsObj.groupByColumnArr.length; k > 1; k--) {
		groupedData.removeColumn(k - 1);
	}
	
	globalGroupedData = groupedData;

	if(currentTabIndex == 2) {
		if ($('#primaryaxis').val() == 2) { // Period
			$('#chartType').val(3);
			changeChartType(3);
			//default to line chart if by period
		}
		else {
			$('#chartType').val(1);
			changeChartType(1); // default to bar chart
		}
	}
}

function chartDataTableToCSV() {
	var csvData = [];
	var tmpArr = [];
	var tmpStr = '';
	if (globalGroupedData == null) {
		return "";
	}
	for (var i = 0; i < globalGroupedData.getNumberOfColumns(); i++) {
		// replace double-quotes with double-double quotes for CSV compatibility
		tmpStr = globalGroupedData.getColumnLabel(i).replace('/"/g', '""');
		tmpArr.push('"' + tmpStr + '"');
	}
	csvData.push(tmpArr);
	for (var i = 0; i < globalGroupedData.getNumberOfRows(); i++) {
		tmpArr = [];
		for (var j = 0; j < globalGroupedData.getNumberOfColumns(); j++) {
			switch (globalGroupedData.getColumnType(j)) {
			case 'string':
				// replace double-quotes with double-double quotes for CSV compatibility
				tmpStr = globalGroupedData.getValue(i, j).replace('/"/g', '""');
				tmpArr.push('"' + tmpStr + '"');
				break;
			case 'number':
				tmpArr.push(globalGroupedData.getValue(i, j));
				break;
			case 'boolean':
				tmpArr.push((globalGroupedData.getValue(i, j)) ? 'True' : 'False');
				break;
			default:
			}
		}
		csvData.push(tmpArr.join(','));
	}
	var output = csvData.join('\n');
	return output;
};

function getBarChartHeight(minHeight, numRows, numCols) {
	return Math.max(400, (numRows * (Math.ceil(numCols/3)) * 18));
}

function changeChartType(type) {

	var filterChart = true;
	if (globalParentName == "oes") {
		filterChart = false;
	}
	var byAxisName = $("#primaryaxis option:selected").text();
	if (type == 1) {
		visualization = new google.visualization.BarChart(document.getElementById('chart_div'));
		if ($('#zeroorigin').is(':checked')) {
			visualization.draw(globalGroupedData, {
				title: "",
				width: 600,
				height: getBarChartHeight(400, globalGroupedData.getNumberOfRows(), globalGroupedData.getNumberOfColumns()),
				chartArea:{top:20, height: getBarChartHeight(400, globalGroupedData.getNumberOfRows(), globalGroupedData.getNumberOfColumns()) - 100},
				vAxis: {
					title: byAxisName
				},
				hAxis: {
					title: $('#resultset option:selected').html(),
					minValue: 0
				}
			});
		}
		else {
			visualization.draw(globalGroupedData, {
				title: "",
				width: 600,
				height: getBarChartHeight(400, globalGroupedData.getNumberOfRows(), globalGroupedData.getNumberOfColumns()),
				chartArea:{top:20, height: getBarChartHeight(400, globalGroupedData.getNumberOfRows(), globalGroupedData.getNumberOfColumns()) - 100},
				vAxis: {
					title: byAxisName
				},
				hAxis: {
					title: $('#resultset option:selected').html()
				}
			});
		}
		if (filterChart) {
			google.visualization.events.addListener(visualization, 'select', adjustFilterForChart);
		}
		// Resize the div based on the number of data elements
		$("#chart_div").height(Math.max(400, (globalGroupedData.getNumberOfRows() * 18)));
		
	}
	if (type == 2) {
		visualization = new google.visualization.ColumnChart(document.getElementById('chart_div'));
		if ($('#zeroorigin').is(':checked')) {
			visualization.draw(globalGroupedData, {
				title: "",
				width: 600,
				height: 400,
				vAxis: {
					title: $('#resultset option:selected').html(),
					minValue: 0
				},
				hAxis: {
					title: byAxisName
				}
			});
		}
		else {
			visualization.draw(globalGroupedData, {
				title: "",
				width: 600,
				height: 400,
				vAxis: {
					title: $('#resultset option:selected').html()
				},
				hAxis: {
					title: byAxisName
				}
			});
		}
		if (filterChart) {
			google.visualization.events.addListener(visualization, 'select', adjustFilterForChart);
		}
		// Resize the div to the fixed size in case changed for horizontal bar
		$("#chart_div").height(400);
	}
	if (type == 3) {
		visualization = new google.visualization.LineChart(document.getElementById('chart_div'));
		if ($('#zeroorigin').is(':checked')) {
			visualization.draw(globalGroupedData, {
				title: "",
				pointSize: 3,
				width: 600,
				height: 400,
				vAxis: {
					title: $('#resultset option:selected').html(),
					minValue: 0
				},
				hAxis: {
					title: byAxisName
				}
			});
		}
		else {
			visualization.draw(globalGroupedData, {
				title: "",
				pointSize: 3,
				width: 600,
				height: 400,
				vAxis: {
					title: $('#resultset option:selected').html()
				},
				hAxis: {
					title: byAxisName
				}
			});
		}
		if (filterChart) {
			google.visualization.events.addListener(visualization, 'select', adjustFilterForChart);
		}
		// Resize the div to the fixed size in case changed for horizontal bar
		$("#chart_div").height(400);
	}
	// For debuggging
	if (type == -1) {
		visualization = new google.visualization.Table(document.getElementById('chart_div'));
		visualization.draw(globalGroupedData, {
			title: "",
			width: 600,
			height: 400,
			vAxis: {
				title: "vAxis"
			},
			hAxis: {
				title: "hAxis",
				minValue: 0
			}
		});
	}
	
}

function highlightChart(regionName) {
	for (var i = 0; i < globalGroupedData.getNumberOfColumns(); i++) {
		if (regionName == globalGroupedData.getColumnLabel(i)) {
			visualization.setSelection([{
				row: null,
				column: i
			}]);
		}
	}
}

function adjustFilterForChart(e) {
	var selection = visualization.getSelection();
	var refresh = false;
	for (var i = 0; i < selection.length; i++) {
		var item = selection[i];
		if (item.row == null && item.column != null) {
			//if by is geography
			if ($('#primaryaxis').val() == "1") {
				if ($('#periodtype').val() == "01") {
					var yearId = globalGroupedData.getColumnLabel(item.column);
					adjustForPeriod(yearId, null);
				}
				else if ($('#periodtype').val() == "03") {
					var monthYear = globalGroupedData.getColumnLabel(item.column);
					var yearId = "20" + monthYear.substring(1,3);
					var monthId = monthYear.substring(4,6);
					adjustForPeriod(yearId, monthId);
				}
			}
			else if ($('#primaryaxis').val() == "2") {
				var region = globalGroupedData.getColumnLabel(item.column);
				adjustForRegion(region);
			}
		}
	}
	if (refresh) {
		refreshData();
	}
}

//
// Table functions
//

function drawTable() {
	if(globalVisitedTableTab) {		
		var data = $.extend(true, [], globalJson);
		var dataArray = data["aaData"];
		var resultsetValues = $("#resultset>option").map(function () {
			return parseInt($(this).val());
		});
		for (var j = 0; j < dataArray.length; j++) {
			for (var colIndex = 0; colIndex < dataArray[j].length; colIndex++) {
				if ($.inArray(colIndex, resultsetValues) > -1) {
					dataArray[j][colIndex] = formatColumn(colIndex, dataArray[j][colIndex]);					
				}
			}
			if(shouldAdjustForRegion(dataArray[j][1])) {
				dataArray[j][1] = "<a href=\"javascript:adjustForRegion(\'" + dataArray[j][1] + "\');\">" + dataArray[j][1] + "</a>";
			}
			if ($('#periodtype').val() == "03") {
				dataArray[j][4] = "<a href=\"javascript:adjustForPeriod(" + dataArray[j][2] + ",\'" + dataArray[j][3] + "\');\">" + dataArray[j][4] + "</a>";
			}
			else if ($('#periodtype').val() == "01") {
				dataArray[j][4] = "<a href=\"javascript:adjustForPeriod(" + dataArray[j][2] + ",null);\">" + dataArray[j][4] + "</a>";
			}
			if (globalParentName == "qcew" || globalParentName == "ces" || globalParentName == "oes") {
				dataArray[j][6] = "<a href=\"javascript:adjustForIndustry(\'" + dataArray[j][5] + "\');\">" + dataArray[j][6] + "</a>";
			}
			if (globalParentName == "oes") {
				dataArray[j][8] = "<a href=\"javascript:adjustForOccupation(\'" + dataArray[j][7] + "\');\">" + dataArray[j][8] + "</a>";
			}
		}
		
		var theTable = $('#datatable').dataTable({
			"bDestroy": true,
			"bLengthChange": true,
			"iDisplayLength": 25,
			"aLengthMenu": [[25, 100, -1], [25, 100, "All"]],			
			"bPaginate": true,
			"bProcessing": true,
			"sPaginationType": "two_button",
			"aaData": dataArray,
			"aoColumns": data["aoColumns"]
		});

		// Hide columns that should not be shown
		var visibleColumnArr = getVisibleTableColumns(parseInt($('#resultset').val()));
		for (i = 0; i < data["aaData"][0].length; i++) {
			if($.inArray(i, visibleColumnArr) == -1) {
				theTable.fnSetColumnVis(i, false);
			}			
		}
		theTable.fnAdjustColumnSizing(true);
	}
}

function dataTableToCSV() {
	var csvData = [];
	var tmpArr = [];
	var tmpStr = '';
	if (globalJson == null) {
		return "";
	}
	for (var i = 0; i < globalJson["aoColumns"].length; i++) {
		// replace double-quotes with double-double quotes for CSV compatibility
		tmpStr = globalJson["aoColumns"][i]["sTitle"].replace('/"/g', '""');
		tmpArr.push('"' + tmpStr + '"');
	}
	csvData.push(tmpArr);
	for (var i = 0; i < globalJson["aaData"].length; i++) {
		tmpArr = [];
		for (var j = 0; j < globalJson["aaData"][i].length; j++) {
			tmpStr = globalJson["aaData"][i][j];
			tmpArr.push('"' + tmpStr + '"');
		}
		csvData.push(tmpArr.join(','));
	}
	var output = csvData.join('\n');
	return output;
}

//
// YBM Table functions
//

function getYBMaaDataWorker(targetGeogIndex) {
	var startMonthValue = parseInt($('#periodstartmonth').val(), 10);
	var endMonthValue = parseInt($('#periodendmonth').val(), 10);
	var startYearValue = parseInt($('#periodstartyear').val(), 10);
	var endYearValue = parseInt($('#periodendyear').val(), 10);
	var dataColumnIndex = parseInt($('#resultset').val(), 10);

	var data = $.extend(true, [], globalJson);
	var origDataArray = data["aaData"];	
	var newDataArray = [];
	var origRowIndex = 0;
	var numGeogs = $("#listofchosen").children().length;

	// Load up the array of aaDatas.
	for(var geogIndex = 0; geogIndex < numGeogs; geogIndex++) {
		var perGeogData = [];
		var geogName = "";
		for(var year = startYearValue; year <= endYearValue; year++) {
			var rowData = [];
			rowData.push(year.toString());
			for(var month = startMonthValue; month <= endMonthValue; month++) {
				var value = origDataArray[origRowIndex][dataColumnIndex];
				rowData.push(formatColumn(dataColumnIndex, value));
				geogName = origDataArray[origRowIndex][1];
				origRowIndex++;
			}
			perGeogData.push(rowData);
		}
		if(targetGeogIndex < 0 || geogIndex == targetGeogIndex) {
			newDataArray.push({'geogName':geogName, 'data':perGeogData});
		}
		
		// If we've run out of data, we're done, regardless of how many geogs we were expecting
		if(origRowIndex >= origDataArray.length) {
			break;
		}
	}

	return newDataArray;
}

function getYBMaaData() {
	return  getYBMaaDataWorker(-1);
}

function padLeadingZero(n) {
    return (n < 10) ? ("0" + n) : n;
}

function getYBMaoColumns() {
	var columnHeaders = [];
	columnHeaders.push({'sTitle': 'Year'});
	
	var startMonthValue = parseInt($('#periodstartmonth').val(), 10);
	var endMonthValue = parseInt($('#periodendmonth').val(), 10);
	
	for(var i = startMonthValue;  i <= endMonthValue; i++) {
		var monthLabel = $("#periodstartmonth option[value='" + padLeadingZero(i) + "']").text();
		monthLabel = monthLabel.substring(0, 3);
		columnHeaders.push({'sTitle': monthLabel, 'sClass': 'ta_small'});
	}
	
	return columnHeaders;
}

var ybmtableconvertcsvEventHandler = function(event) {
	var target = event.target;
	var greatGrandparent = target.parentNode.parentNode.parentNode;
	var csv = 'data:application/csv;charset=utf-8,' + encodeURIComponent(ybmDataTableToCSV(greatGrandparent.id));					
	$(this).attr('download', generateFilenameTitle() + "_for_" + target.id.replace(/ /g, '_') + "_ybmtable.csv"); 
	$(this).attr('href', csv); 
}

var numYBMTables = 0;
function drawYBMTables() {
	var newDataArray = getYBMaaData();
	var aoColumns = getYBMaoColumns();
	
	var origWrapper = document.getElementById("ybmwrapper0");	
	var tabs5 = document.getElementById('tabs-5');
	
	// Clean up any previous tables... but leave one around to use as a template to clone
	while (tabs5.childElementCount > 1) {
		tabs5.removeChild(tabs5.lastChild);
	}	

	numYBMTables = newDataArray.length;
	for(var i = 0; i < numYBMTables; i++) {
		if(i >= 1) {
			var cloneWrapper = origWrapper.cloneNode(true);
			cloneWrapper.setAttribute('id', 'ybmwrapper' + i.toString());
			tabs5.appendChild(cloneWrapper);
		}
		$('#ybmwrapper' + i).find('.datatable_caption').html(newDataArray[i].geogName);
		$('#ybmwrapper' + i).find('.ybmtableconvertcsv').click(ybmtableconvertcsvEventHandler);
		$('#ybmwrapper' + i).find('.ybmtablebuttonclass').attr("id", newDataArray[i].geogName);		
		var theTable = $('#ybmwrapper' + i).find('.data-table').dataTable({
			"bDestroy": true,
			"bLengthChange": false,
			"iDisplayLength": 25,
			"aLengthMenu": [[25, 100, -1], [25, 100, "All"]],			
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"bProcessing": true,
			"aaData": newDataArray[i].data,
			"aoColumns": aoColumns
		});
	}
		
}

function ybmDataTableToCSV(id) {
	var index = parseInt(id.substring(10));
	
	var csvData = [];
	var tmpArr = [];
	var tmpStr = '';
	var aoColumns = getYBMaoColumns();
	var aaData = getYBMaaDataWorker(index)[0].data;

	for (var i = 0; i < aoColumns.length; i++) {
		// replace double-quotes with double-double quotes for CSV compatibility
		tmpStr = aoColumns[i]["sTitle"].replace('/"/g', '""');
		tmpArr.push('"' + tmpStr + '"');
	}
	csvData.push(tmpArr);
	for (var i = 0; i < aaData.length; i++) {
		tmpArr = [];
		for (var j = 0; j < aaData[i].length; j++) {
			tmpStr = aaData[i][j];
			tmpArr.push('"' + tmpStr + '"');
		}
		csvData.push(tmpArr.join(','));
	}
	var output = csvData.join('\n');
	return output;
}

var prevRegiontype = null;
var prevSelectedRegions = [];
var prevPeriodtype = null;
var prevStartyear = null;
var prevEndyear = null;
var prevStartmonth = null;
var prevEndmonth = null;

function clearPreviousParameters() {
	prevRegiontype = null;
	prevSelectedRegions = [];
	prevPeriodtype = null;
	prevStartyear = null;
	prevEndyear = null;
	prevStartmonth = null;
	prevEndmonth = null;
}

function adjustBackToPrevious() {
	if(prevRegiontype != null) {
		adjustBackToPreviousRegion();
	}
	else if(prevPeriodtype != null) {
		adjustBackToPreviousPeriod();
	}
}

function adjustBackToPreviousRegion() {
	removeAllFromSelected();
	$('#regiontype').val(prevRegiontype);
	populateGeographies();
	for(var i = 0; i < prevSelectedRegions.length; i++) {
		$("#listofchoices option[value='" + prevSelectedRegions[i] + "']").prop("selected", true);
	}
	addToSelected();
	clearPreviousParameters();
	refreshData();
}

function adjustBackToPreviousPeriod() {
	$('#periodtype').val(prevPeriodtype);
	adjustPeriod();
	
	$('#periodstartyear').val(prevStartyear);
	$('#periodendyear').val(prevEndyear);
	$('#periodstartmonth').val(prevStartmonth);
	$('#periodendmonth').val(prevEndmonth);

	clearPreviousParameters();
	refreshData();
}

function adjustForRegion(regionName) {
	if(!shouldAdjustForRegion(regionName)) {
		return;
	}

	// Hold on to the current values
	prevRegiontype = $('#regiontype').val()
	var chosenOptions = document.getElementById('listofchosen').options;
	prevSelectedRegions = [];
	for(var i = 0; i < chosenOptions.length; i++) {
		prevSelectedRegions.push(chosenOptions[i].value);
	}

	removeAllFromSelected();
	if ($('#entirestate').prop("checked")) {
		$('#entirestate').prop("checked", false);
		entireStateChanged();
		$('#regiontype').val("10");
		populateGeographies();
		removeAllFromSelected();
		addAllToSelected();
	}
	else if ($('#regiontype').val() == "10") {
		$('#regiontype').val("04");
		populateGeographies();
		removeAllFromSelected();
		for (i = 0; i < regionCounties[regionName].length; i++) {
			for (var j = 0; j < document.frmParams.listofchoices.options.length; j++) {
				if ($("#listofchoices option").eq(j).val() == regionCounties[regionName][i]) {
					$("#listofchoices option").eq(j).appendTo('#listofchosen');
					i--;
				}
			}
		}
	}
	else {
		removeAllFromSelected();
		for (var i = 0; i < document.frmParams.listofchoices.options.length; i++) {
			if ($("#listofchoices option").eq(i).html() == regionName) {
				$("#listofchoices option").eq(i).appendTo('#listofchosen');
				i--;
			}
		}
	}
	
	refreshData();
}

function adjustForPeriod(yearId, monthId) {
	prevPeriodtype = $('#periodtype').val();
	prevStartyear = $('#periodstartyear').val();
	prevEndyear = $('#periodendyear').val();
	prevStartmonth = $('#periodstartmonth').val();
	prevEndmonth = $('#periodendmonth').val();
	
	if ($('#periodtype').val() == "01") {
		$('#periodtype').val("03");
		adjustPeriod();
	}
	$('#periodstartyear').val(yearId);
	$('#periodendyear').val(yearId);
	if (monthId != null) {
		$('#periodstartmonth').val(monthId);
		$('#periodendmonth').val(monthId);
	}
	refreshData();
}

function adjustForIndustry(industryId) {
	$("#btnDeselectAll").click();
	var hasChildren = $("#naicsTree").dynatree("getTree").getNodeByKey(industryId).hasChildren();
	if (hasChildren) {
		$("#naicsTree").dynatree("getTree").getNodeByKey(industryId).visit(function (childNode) {
			childNode.select(true);
		})
	}
	else {
		$("#naicsTree").dynatree("getTree").selectKey(industryId);
	}
	refreshData();
}

function adjustForOccupation(occId) {
	$("#btnDeselectAllSoc").click();
	var hasChildren = $("#socTree").dynatree("getTree").getNodeByKey(occId).hasChildren();
	if (hasChildren) {
		$("#socTree").dynatree("getTree").getNodeByKey(occId).visit(function (childNode) {
			childNode.select(true);
		})
	}
	else {
		$("#socTree").dynatree("getTree").selectKey(occId);
	}
	refreshData();
}

//
// Generic data request and manipulation functions
//

function refreshData() {
	$('html, body').css("cursor", "wait");
	var xmlhttp;
	var url = globalWSBaseUrl;
	url += globalParentName.toUpperCase();
	url += "Service";
	url += generateQueryString();
	xmlhttp = $.ajax({
		url: url,
		type: 'GET',
		dataType: 'jsonp',
		success: function (json) {
			// Set the cursor back
			$('html, body').css("cursor", "auto");
			// do stuff with json (in this case an array)
			globalJson = json;
			if (json["aaData"].length > 0) {
				// This order seems to work the best.
				if (isNonNullData(json['aaData'])) {
					$("#chart_div").show();
					drawChart();
					$("#chartType").prop("disabled", false);
					$("#primaryaxis").prop("disabled", false);
					
					drawTable();
					drawYBMTables();
					drawMap();
				}
				else {
					$("#chartType").prop("disabled", true);
					$("#primaryaxis").prop("disabled", true);
					$("#chart_div").hide()

					alert("The specified data set does not contain any values. Please adjust your parameters and try again.")
				}
			}
			else {
				alert("No data available. Please adjust your parameters and try again.")
			}
		},
		error: function () {
			// Set the cursor back
			$('html, body').css("cursor", "auto");
			// Notify the user
			alert("Unable to communicate with the server.");
		},
	});
}

