

<?php

function generateIndustryData() {
	/*$serverName = "50.19.104.225";
	$connectionInfo = array("Database"=>"ALMIS_PARTIAL_1", "UID"=>"sa", "PWD"=>"Password123!");
	$conn = sqlsrv_connect($serverName, $connectionInfo);
	
	if($conn === false) {
		die(print_r( sqlsrv_errors(), true));
	}

	$stmt = sqlsrv_query($conn, "select TITLE, NAICS, NAICS_LEVEL from NAICS_CODE where NAICS_LEVEL = 2 or NAICS_LEVEL = 3 order by NAICS");
	if($stmt === false) {
		die( print_r( sqlsrv_errors(), true) );
	}

	echo "var naicsData = [\n";
	
	$prevLevel = -1;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC) ) {
		$level = $row[2];
		
		if($prevLevel == -1) {
		}
		else if($level > $prevLevel) {
			echo ",\nchildren: [\n";
		}
		else if($level == $prevLevel) {
			echo "},\n";
		}
		else if($level < $prevLevel) {
			echo "}\n]\n},\n";
		}
		
		echo "{title:\"".$row[0]."\", key: \"".$row[1]."\"";
	
		$prevLevel = $level;	
	}
	
	echo "}\n";
	if($prevLevel == 3) {
		echo "]\n}\n";
	}
	echo "];\n";*/
		
	try {
		$hostname = "50.19.104.225";
		///$port = 10060;
		$dbname = "ALMIS_PARTIAL_1";
		$username = "sa";
		$pw = "Password123!";
		$dbh = new PDO ("dblib:host=$hostname;dbname=$dbname","$username","$pw");
	} catch (PDOException $e) {
		echo "Failed to get DB handle: " . $e->getMessage() . "\n";
		exit;
	}
	
	echo "var naicsData = [\n";
	
	$prevLevel = -1;
	
	$stmt = $dbh->prepare("select TITLE, NAICS, NAICS_LEVEL from NAICS_CODE where NAICS_LEVEL = 2 or NAICS_LEVEL = 3 order by NAICS");
	$stmt->execute();
	while ($row = $stmt->fetch()) {
		$level = $row[2];
		
		if($prevLevel == -1) {
		}
		else if($level > $prevLevel) {
			echo ",\nchildren: [\n";
		}
		else if($level == $prevLevel) {
			echo "},\n";
		}
		else if($level < $prevLevel) {
			echo "}\n]\n},\n";
		}
		
		echo "{title:\"".$row[0]."\", key: \"".$row[1]."\"";
		
		$prevLevel = $level;	
	}

	
	echo "}\n";
	if($prevLevel == 3) {
		echo "]\n}\n";
	}
	echo "];\n";
	
	unset($dbh); unset($stmt);
}

function generateOptionList($firstParam, $sql) {
	
	/*$serverName = "50.19.104.225";
	$connectionInfo = array("Database"=>"ALMIS_PARTIAL_1", "UID"=>"sa", "PWD"=>"Password123!");
	$conn = sqlsrv_connect($serverName, $connectionInfo);

	if($conn === false) {
		die(print_r( sqlsrv_errors(), true));
	}

	$stmt = sqlsrv_query( $conn, $sql);
	if($stmt === false) {
		die( print_r( sqlsrv_errors(), true) );
	}

	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC) ) {
		  echo "addToOptionList(".$firstParam.", \"".$row[0]."\", \"".$row[1]."\");\n";
	}

	sqlsrv_free_stmt( $stmt);*/
	
	try {
		$hostname = "50.19.104.225";
		///$port = 10060;
		$dbname = "ALMIS_PARTIAL_1";
		$username = "sa";
		$pw = "Password123!";
		$dbh = new PDO ("dblib:host=$hostname;dbname=$dbname","$username","$pw");
	} catch (PDOException $e) {
		echo "Failed to get DB handle: " . $e->getMessage() . "\n";
		exit;
	}
	$stmt = $dbh->prepare($sql);
	$stmt->execute();
	while ($row = $stmt->fetch()) {
		 echo "addToOptionList(".$firstParam.", \"".$row[0]."\", \"".$row[1]."\");\n";
	}
	unset($dbh); unset($stmt);
	
}

function generateYearOptionList($firstParam) {
	for($i = 2000; $i <= date('Y'); $i++) {
		$selected = "";
		if ($i == date('Y')) {
			$selected = "Selected";
		}
		echo "addToOptionList".$selected."(".$firstParam.", \"".$i."\", \"".$i."\");\n";
	}
}

?>

        <script type="text/javascript">
		<?php generateIndustryData(); ?> 
		
			function generateQueryString() {
				// ?periodtype=01&startyear=1990&endyear=1992&areatype=04&area0=000001&area1=000003"
				var buffer = "";
				buffer += "?periodtype=";
				buffer += document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value;
				if(document.frmParams.periodstartyear && ((typeof document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex] != 'undefined') && document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex] != null)) {
					buffer += "&startyear=";
					buffer += document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex].value;
					if(document.frmParams.periodendyear && ((typeof document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != 'undefined') && document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != null)) {
						buffer += "&endyear=";
						buffer += document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex].value;
					}
					else {
						buffer += "&endyear=";
						buffer += document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex].value;
					}
				}
				else if(document.frmParams.periodendyear && ((typeof document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != 'undefined') && document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != null)) {
					buffer += "&endyear=";
					buffer += document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex].value;
				}
				if(document.frmParams.periodstartmonth && ((typeof document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex] != 'undefined') && document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex] != null)) {
					buffer += "&startperiod=";
					buffer += document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex].value;
					if(document.frmParams.periodendmonth && ((typeof document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != 'undefined') && document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != null)) {
						buffer += "&endperiod=";
						buffer += document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex].value;
					}
					else {
						buffer += "&endperiod=";
						buffer += document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex].value;
					}
				}
				else if(document.frmParams.periodendmonth && ((typeof document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != 'undefined') && document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != null)) {
					buffer += "&endperiod=";
					buffer += document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex].value;
				}
				
				if(document.frmParams.entirestate.checked) {
					buffer += "&areatype=01&a0=000036";
				}
				else {
					buffer += "&areatype=";
					buffer += document.frmParams.regiontype[document.frmParams.regiontype.selectedIndex].value;
					for (var i = 0; i < document.frmParams.listofchosen.options.length; i++) {
						buffer += "&a";
						buffer += i;
						buffer += "=";
						buffer += document.frmParams.listofchosen.options[i].value;
					}
				}
				
				var selectedNodes = $("#naicsTree").dynatree("getSelectedNodes");
				for(var j = 0; j < selectedNodes.length; j++) {
					buffer += "&i";
					buffer += j;
					buffer += "=";
					buffer += selectedNodes[j].data.key;				
				}
				
				return buffer;
			}
		
			function isInList(OptionList, target) {
                for (var x = OptionList.length - 1; x >= 0; x--) {
                    if(target === OptionList[x].text) {
						return true;
					}
                }
				return false;
			}

            function clearOptions(OptionList) {
                // Always clear an option list from the last entry to the first
                for (var x = OptionList.length; x >= 0; x--) {
                    OptionList[x] = null;
                }
            }

            function addToOptionList(optionList, optionValue, optionText) {
                // Add option to the bottom of the list
                optionList[optionList.length] = new Option(optionText, optionValue);
            }

            function addToOptionListSelected(optionList, optionValue, optionText) {
                // Add option to the bottom of the list
                optionList[optionList.length] = new Option(optionText, optionValue, false, true);
            }

            function populateGeographies() {
                var regionTypeList = document.frmParams.regiontype;

                // Clear out the list
                clearOptions(document.frmParams.listofchoices);
                clearOptions(document.frmParams.listofchosen);

                if (regionTypeList[regionTypeList.selectedIndex].value == "04") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='04'"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "10") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='10'"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "15") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='15'"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "21") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='21'"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "51") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='51'"); ?> 
                }
            }

            function addToSelected() {
				
				$('#listofchoices option:selected').appendTo('#listofchosen');

/*                for (var i = 0; i < document.frmParams.listofchoices.options.length; i++) {
                    if (document.frmParams.listofchoices.options[i].selected) {
						document.frmParams.listofchosen[document.frmParams.listofchosen.length] = document.frmParams.listofchoices.options[i];
						i--;
                    }
                }*/
                return false;
            }

            function addAllToSelected() {
				$('#listofchoices option').appendTo('#listofchosen');
                /*while (document.frmParams.listofchoices.options.length > 0) {
                    document.frmParams.listofchosen[document.frmParams.listofchosen.length] = document.frmParams.listofchoices.options[0];
                }*/
                return false;
            }

            function removeFromSelected() {
				$('#listofchosen option:selected').appendTo('#listofchoices');
                /*for (var i = 0; i < document.frmParams.listofchosen.options.length; i++) {
                    if (document.frmParams.listofchosen.options[i].selected) {
                        document.frmParams.listofchoices[document.frmParams.listofchoices.length] = document.frmParams.listofchosen.options[i];
                        i--;
                    }
                }*/
                return false;
            }

			function removeAllFromSelected() {
				$('#listofchosen option').appendTo('#listofchoices');
				/*for (var i = 0; i < document.frmParams.listofchosen.options.length; i++) {
					
						document.frmParams.listofchoices[document.frmParams.listofchoices.length] = document.frmParams.listofchosen.options[i];
						i--;
					
				}*/
				return false;
			}

            function entireStateChanged() {
                if (document.frmParams.entirestate.checked) {
                    document.frmParams.regiontype.disabled = true;
                    document.frmParams.listofchoices.disabled = true;
                    document.frmParams.addButton.disabled = true;
                    document.frmParams.addAllButton.disabled = true;
                    document.frmParams.removeButton.disabled = true;
                    document.frmParams.listofchosen.disabled = true;
                }
                else {
                    document.frmParams.regiontype.disabled = false;
                    document.frmParams.listofchoices.disabled = false;
                    document.frmParams.addButton.disabled = false;
                    document.frmParams.addAllButton.disabled = false;
                    document.frmParams.removeButton.disabled = false;
                    document.frmParams.listofchosen.disabled = false;
                }
            }

            function adjustPeriod() {
                if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "01") {
                    document.frmParams.periodendyear.disabled = false;
                    document.frmParams.periodstartmonth.disabled = true;
                    document.frmParams.periodendmonth.disabled = true;
                    document.getElementById('startmonthlabel').innerHTML = "N/A";
                    document.getElementById('endmonthlabel').innerHTML = "N/A";
                    // Year values
                    clearOptions(document.frmParams.periodstartyear);
                    clearOptions(document.frmParams.periodendyear);
					
					<?php generateYearOptionList("document.frmParams.periodstartyear"); ?> 
					<?php generateYearOptionList("document.frmParams.periodendyear"); ?> 
                    // No month values for yearly
                    clearOptions(document.frmParams.periodstartmonth);
                    clearOptions(document.frmParams.periodendmonth);
                }
                else if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "02") {
                    document.frmParams.periodendyear.disabled = false;
                    document.frmParams.periodstartmonth.disabled = false;
                    document.frmParams.periodendmonth.disabled = false;
                    document.getElementById('startmonthlabel').innerHTML = "Start Quarter:";
                    document.getElementById('endmonthlabel').innerHTML = "End Quarter:";
                    // Year values
                    clearOptions(document.frmParams.periodstartyear);
                    clearOptions(document.frmParams.periodendyear);
					<?php generateYearOptionList("document.frmParams.periodstartyear"); ?> 
					<?php generateYearOptionList("document.frmParams.periodendyear"); ?> 
                    // Quarter values use month controls
                    clearOptions(document.frmParams.periodstartmonth);
                    clearOptions(document.frmParams.periodendmonth);
                    addToOptionListSelected(document.frmParams.periodstartmonth, "01", "Q1");
                    addToOptionList(document.frmParams.periodstartmonth, "02", "Q2");
                    addToOptionList(document.frmParams.periodstartmonth, "03", "Q3");
                    addToOptionList(document.frmParams.periodstartmonth, "04", "Q4");
                    addToOptionList(document.frmParams.periodendmonth, "01", "Q1");
                    addToOptionList(document.frmParams.periodendmonth, "02", "Q2");
                    addToOptionList(document.frmParams.periodendmonth, "03", "Q3");
                    addToOptionListSelected(document.frmParams.periodendmonth, "04", "Q4");
                }
                else if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "03") {
                    document.frmParams.periodendyear.disabled = false;
                    document.frmParams.periodstartmonth.disabled = false;
                    document.frmParams.periodendmonth.disabled = false;
                    document.getElementById('startmonthlabel').innerHTML = "Start Month:";
                    document.getElementById('endmonthlabel').innerHTML = "End Month:";
                    // Year values
                    clearOptions(document.frmParams.periodstartyear);
                    clearOptions(document.frmParams.periodendyear);
					<?php generateYearOptionList("document.frmParams.periodstartyear"); ?> 
					<?php generateYearOptionList("document.frmParams.periodendyear"); ?> 
                    //
                    clearOptions(document.frmParams.periodstartmonth);
                    clearOptions(document.frmParams.periodendmonth);
                    addToOptionList(document.frmParams.periodstartmonth, "01", "January");
                    addToOptionList(document.frmParams.periodstartmonth, "02", "February");
                    addToOptionList(document.frmParams.periodstartmonth, "03", "March");
                    addToOptionList(document.frmParams.periodstartmonth, "04", "April");
                    addToOptionList(document.frmParams.periodstartmonth, "05", "May");
                    addToOptionList(document.frmParams.periodstartmonth, "06", "June");
                    addToOptionList(document.frmParams.periodstartmonth, "07", "July");
                    addToOptionList(document.frmParams.periodstartmonth, "08", "August");
                    addToOptionList(document.frmParams.periodstartmonth, "09", "September");
                    addToOptionList(document.frmParams.periodstartmonth, "10", "October");
                    addToOptionList(document.frmParams.periodstartmonth, "11", "November");
                    addToOptionList(document.frmParams.periodstartmonth, "12", "December");
                    addToOptionList(document.frmParams.periodendmonth, "01", "January");
                    addToOptionList(document.frmParams.periodendmonth, "02", "February");
                    addToOptionList(document.frmParams.periodendmonth, "03", "March");
                    addToOptionList(document.frmParams.periodendmonth, "04", "April");
                    addToOptionList(document.frmParams.periodendmonth, "05", "May");
                    addToOptionList(document.frmParams.periodendmonth, "06", "June");
                    addToOptionList(document.frmParams.periodendmonth, "07", "July");
                    addToOptionList(document.frmParams.periodendmonth, "08", "August");
                    addToOptionList(document.frmParams.periodendmonth, "09", "September");
                    addToOptionList(document.frmParams.periodendmonth, "10", "October");
                    addToOptionList(document.frmParams.periodendmonth, "11", "November");
                    addToOptionListSelected(document.frmParams.periodendmonth, "12", "December");
                }

                if (document.frmParams.periodendyear.disabled == false) {
                    if (document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex].value < document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex].value) {
                        document.frmParams.periodendyear.selectedIndex = document.frmParams.periodstartyear.selectedIndex;
                    }
                }
            }

        </script>

<form action="/" name="frmParams" class="ux-form-top">
    <div class="grid_11">
        <div class="dol-container">
            <h3>Data Set and Display Options</h3>
            <div class="grid_5">
                <label for="resultset">
                    <span>Data Set:</span>
                    <select name="resultset" id="resultset">
                        <option value="5">Labor Force</option>
                        <option value="6">Employed</option>
                        <option value="7">Unemployed</option>
                        <option value="8">Unemployment Rate</option>
                        <option value="9">Absolute Change Labor Force</option>
                        <option value="10">Percent Change Labor Force</option>
                        <option value="11">Absolute Change Employed</option>
                        <option value="12">Percent Change Employed</option>
                        <option value="13">Absolute Change Unemployed</option>
                        <option value="14">Percent Change Unemployed</option>
                        <option value="15">Absolute Change Unemployment Rate</option>
                    </select>
                </label>
            </div>
            <div class="grid_5">
                <label for="primaryaxis">
                    <span>By:</span>
                    <select name="primaryaxis" id="primaryaxis">
                        <option value="1">Geography</option>
                        <option value="2">Period</option>
                    </select>
                </label>
            </div>
        </div>
        <!-- end .dol-container -->
    </div>
    <div class="grid_11">
        <div class="dol-container">
            <h3>Geography Filter</h3>
            <div class="grid_11">
                <div class="grid_6 alpha">
                    <label for="regiontype">
                        <span>Type:</span>
                        <select name="regiontype" id="regiontype" onchange="populateGeographies();">
                            <option value="04">County</option>
                            <option value="10">Labor Market Region</option>
                            <option value="15">Workforce Investment Area</option>
                            <option value="21">Metropolitan Statistical Area</option>
                            <option value="51">Cities, Towns & Villages</option>
                        </select>
                    </label>
                </div>
                <div class="grid_5 omega">
                    <p>&nbsp;</p>
                    <span>
                        <input type="checkbox" name="entirestate" id="entirestate" onchange="entireStateChanged();" /> 
                        State-wide data?
                    </span>
                </div>
            </div>
            <div class="grid_4 ux-selection-component-left">
                <label for="listofchoices"><span>Available Values:</span>
                    <select name="listofchoices" id="listofchoices" multiple="multiple" size="7">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_2 ux-selection-component-middle">
                <div class="button-container">
                    <input type="button" value="Add" id="addButton" class="button next icon-right width-100" onclick="addToSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Add All" id="addAllButton" class="button next icon-right width-100" onclick="addAllToSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Remove" id="removeButton" class="button previous width-100" onclick="removeFromSelected();" />
                </div>
            </div>
            <div class="grid_4 ux-selection-component-right">
                <label for="listofchosen"><span>Selected Values:</span>
                    <select name="listofchosen" id="listofchosen" multiple="multiple" size="7">
                    </select>
                </label>
            </div>
        </div>
        <!-- end .dol-container -->
    </div>
    <div class="grid_11">
        <div class="dol-container">
            <h3>Period Filter</h3>
            <div class="grid_3">
                <label for="periodtype">
                    <span>Display:</span>
                    <select name="periodtype" id="periodtype" onchange="adjustPeriod();">
                        <option value="03">Monthly</option>
                        <option value="01">Yearly</option>
                    </select>
                </label>
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodstartyear">
                    <span>Start Year:</span>
                    <select name="periodstartyear" id="periodstartyear" onChange="validateForStartYear();">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodendyear">
                    <span>End Year:</span>
                    <select name="periodendyear" id="periodendyear" onChange="validateForEndYear();">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_3">
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodstartmonth">
                    <span id="startmonthlabel">Start Month: </span>
                    <select name="periodstartmonth" id="periodstartmonth" onChange="validateForStartMonth();">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodendmonth">
                    <span id="endmonthlabel">End Month:</span>
                    <select name="periodendmonth" id="periodendmonth" onChange="validateForEndMonth();">
                        <option></option>
                    </select>
                </label>
            </div>
        </div> <!-- end .dol-container -->
    </div>
	<div class="grid_11">
		<div class="dol-container">
			<h3>Industry Filter</h3>
			&nbsp;<a href="#" id="btnSelectAll">Select all</a>&nbsp;&nbsp;
			<a href="#" id="btnDeselectAll">Deselect all</a>
			<div id="naicsTree">
			</div>
		</div>
		<!-- end .dol-container -->
	</div>	
</form>


<script type="text/javascript">
	function validateForStartYear() {
		
		if($('#periodendyear').val() != null){
			if($('#periodstartyear').val() > $('#periodendyear').val()){
				
				$('#periodendyear').val($('#periodstartyear').val()); 
				
			}		
		}
	}

	function validateForEndYear() {
		
		if($('#periodstartyear').val() != null){
			if($('#periodstartyear').val() > $('#periodendyear').val()){
				
				$('#periodstartyear').val($('#periodendyear').val()); 
				
			}		
		}
	}

	function validateForStartMonth() {
		
		if($('#periodendmonth').val() != null){
			if($('#periodstartmonth').val() > $('#periodendmonth').val()){
				
				$('#periodendmonth').val($('#periodstartmonth').val()); 
				
			}		
		}
	}

	function validateForEndMonth() {
		
		if($('#periodstartmonth').val() != null){
			if($('#periodstartmonth').val() > $('#periodendmonth').val()){
				
				$('#periodstartmonth').val($('#periodendmonth').val()); 
				
			}		
		}
	}

</script>
