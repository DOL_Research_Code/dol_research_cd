

<?php

function generateIndustryData() {
	echo "var naicsData = [{title: \"Total Nonfarm (00000000)\", key: \"00000000\"},{title: \"Total Private (05000000)\", key: \"05000000\", children: [{title: \"Goods-Producing (06000000)\", key: \"06000000\", children: [{title: \"Mining and Logging (10000000)\", key: \"10000000\"},{title: \"Construction (20000000)\", key: \"20000000\"},{title: \"Manufacturing (30000000)\", key: \"30000000\"}]},{title: \"Service-Providing (07000000)\", key: \"07000000\",children: [{title: \"Private Service-Providing (08000000)\", key: \"08000000\",children :[{title: \"Trade, Transportation, and Utilities (40000000)\", key: \"40000000\",children: [{title: \"Mining (41000000)\", key: \"41000000\"},{title: \"Construction (42000000)\", key: \"42000000\"},{title: \"Manufacturing (43000000)\", key: \"43000000\"}]},{title: \"Information (50000000)\", key: \"50000000\"},{title: \"Financial Activities (55000000)\", key: \"55000000\"},{title: \"Professional and Business Services (60000000)\", key: \"60000000\"},{title: \"Education and Health Services (65000000)\", key: \"65000000\"},{title: \"Leisure and Hospitality (70000000)\", key: \"70000000\"},{title: \"Other Services (80000000)\", key: \"80000000\"}]}]}]},{title: \"Government (90000000)\", key: \"90000000\", children: [{title: \"Federal (90910000)\", key: \"90910000\"},{title: \"State (90920000)\", key: \"90920000\"},{title: \"Local (90930000)\", key: \"90930000\"}]}];";

	$serverName = DB_HOST;
	$connectionInfo = array("Database"=>DB_DATABASE, "UID"=>DB_USER, "PWD"=>DB_PASSWORD);
	$conn = sqlsrv_connect($serverName, $connectionInfo);
	
	if($conn === false) {
		die(print_r( sqlsrv_errors(), true));
	}

	$stmt = sqlsrv_query($conn, "select NAICS, DESCRIPTION from CES_NAICS where NAICS not in (00000000,05000000,06000000,10000000,20000000,30000000,07000000,08000000,40000000,41000000,42000000,43000000,50000000,55000000,60000000,65000000,70000000,80000000,90000000,90910000,90920000,90930000) order by NAICS");
	if($stmt === false) {
		die( print_r( sqlsrv_errors(), true) );
	}

	echo "var additionalNaicsData = [\n";
	
	$prevLevel = -1;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC) ) {
		if($prevLevel > 0) {
			echo "},";
		}		 
		echo "{title:\"".$row[1]." (".$row[0].")\", key: \"".$row[0]."\"";
		$prevLevel = 1;	
	}

	echo "}\n";
	echo "];\n";
}

?>

        <script type="text/javascript">
		<?php generateIndustryData(); ?> 
		
			function generateQueryString() {
				// ?periodtype=01&startyear=1990&endyear=1992&areatype=04&area0=000001&area1=000003"
				var buffer = "";
				buffer += "?periodtype=";
				buffer += document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value;
				if(document.frmParams.periodstartyear && ((typeof document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex] != 'undefined') && document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex] != null)) {
					buffer += "&startyear=";
					buffer += document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex].value;
					if(document.frmParams.periodendyear && ((typeof document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != 'undefined') && document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != null)) {
						buffer += "&endyear=";
						buffer += document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex].value;
					}
					else {
						buffer += "&endyear=";
						buffer += document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex].value;
					}
				}
				else if(document.frmParams.periodendyear && ((typeof document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != 'undefined') && document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex] != null)) {
					buffer += "&endyear=";
					buffer += document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex].value;
				}
				if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value != "01") {
					if(document.frmParams.periodstartmonth && ((typeof document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex] != 'undefined') && document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex] != null)) {
						buffer += "&startperiod=";
						buffer += document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex].value;
						if(document.frmParams.periodendmonth && ((typeof document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != 'undefined') && document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != null)) {
							buffer += "&endperiod=";
							buffer += document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex].value;
						}
						else {
							buffer += "&endperiod=";
							buffer += document.frmParams.periodstartmonth[document.frmParams.periodstartmonth.selectedIndex].value;
						}
					}
					else if(document.frmParams.periodendmonth && ((typeof document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != 'undefined') && document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex] != null)) {
						buffer += "&endperiod=";
						buffer += document.frmParams.periodendmonth[document.frmParams.periodendmonth.selectedIndex].value;
					}
				}
				if(document.frmParams.seasonadjust.checked) {
					buffer += "&seasonAdjusted=1";
				}
				
				if(document.frmParams.entirestate.checked) {
					buffer += "&isstatewide=1";
				}
				
				buffer += "&areatype=";
				buffer += document.frmParams.regiontype[document.frmParams.regiontype.selectedIndex].value;
				for (var i = 0; i < document.frmParams.listofchosen.options.length; i++) {
					buffer += "&a";
					buffer += i;
					buffer += "=";
					buffer += document.frmParams.listofchosen.options[i].value;
				}
				
				var selectedNodes = $("#naicsTree").dynatree("getSelectedNodes");
				for(var j = 0; j < selectedNodes.length; j++) {
					buffer += "&i";
					buffer += j;
					buffer += "=";
					buffer += selectedNodes[j].data.key;				
				}
				
				return buffer;
			}
		

			function populatePrimaryAxis() {
				$('#primaryaxis').append('<option value="1">Geography</option>'); 
				$('#primaryaxis').append('<option value="2">Period</option>'); 		  
				$('#primaryaxis').append('<option value="3">Industry</option>'); 		  
			}

			function loadChartIndexParams(primaryAxisVal, indexParamsObj) {
				if (primaryAxisVal == 1) { // Geography
					indexParamsObj.groupByColumnArr.push(1);
					indexParamsObj.columnDisplayColumnIndex = 4;
					indexParamsObj.rowDisplayColumnIndex = 1;
				}
				else if (primaryAxisVal == 2) { // Period
					indexParamsObj.groupByColumnArr.push(2);
					indexParamsObj.groupByColumnArr.push(3);
					indexParamsObj.columnDisplayColumnIndex = 1;
					indexParamsObj.rowDisplayColumnIndex = 4;
				}
				else if (primaryAxisVal == 3) { // Industry
					indexParamsObj.groupByColumnArr.push(6);
					indexParamsObj.columnDisplayColumnIndex = 4;
					indexParamsObj.rowDisplayColumnIndex = 6;
				}
			}
			
            function populateGeographies() {
                var regionTypeList = document.frmParams.regiontype;

                // Clear out the list
                clearOptions(document.frmParams.listofchoices);
                clearOptions(document.frmParams.listofchosen);

                if (regionTypeList[regionTypeList.selectedIndex].value == "04") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='04' AND AREA in (select distinct AREA from CES_V2_5 where AREATYPE='04') order by AREANAME"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "10") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='10' order by AREA"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "15") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='15' order by AREANAME"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "21") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND (AREATYPE='21' or AREATYPE='23') order by AREANAME"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "51") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='51' order by AREANAME"); ?> 
                }
            }

            function adjustPeriod() {
                if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "01") {
					$('#seasonadjustspan').hide();
					$('#seasonadjust').prop("checked", false);
                    document.frmParams.periodendyear.disabled = false;
                    document.frmParams.periodstartmonth.disabled = true;
                    document.frmParams.periodendmonth.disabled = true;
                    document.getElementById('startmonthlabel').innerHTML = "N/A";
                    document.getElementById('endmonthlabel').innerHTML = "N/A";
                    // Year values
                    clearOptions(document.frmParams.periodstartyear);
                    clearOptions(document.frmParams.periodendyear);
					
					<?php generateYearOptionList("document.frmParams.periodstartyear"); ?> 
					<?php generateYearOptionList("document.frmParams.periodendyear"); ?> 
                    // No month values for yearly
                    clearOptions(document.frmParams.periodstartmonth);
                    clearOptions(document.frmParams.periodendmonth);
                }
                else if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "02") {
					$('#seasonadjustspan').hide();
					$('#seasonadjust').prop("checked", false);
                    document.frmParams.periodendyear.disabled = false;
                    document.frmParams.periodstartmonth.disabled = false;
                    document.frmParams.periodendmonth.disabled = false;
                    document.getElementById('startmonthlabel').innerHTML = "Start Quarter:";
                    document.getElementById('endmonthlabel').innerHTML = "End Quarter:";
                    // Year values
                    clearOptions(document.frmParams.periodstartyear);
                    clearOptions(document.frmParams.periodendyear);
					<?php generateYearOptionList("document.frmParams.periodstartyear"); ?> 
					<?php generateYearOptionList("document.frmParams.periodendyear"); ?> 
                    // Quarter values use month controls
                    clearOptions(document.frmParams.periodstartmonth);
                    clearOptions(document.frmParams.periodendmonth);
                    addToOptionListSelected(document.frmParams.periodstartmonth, "01", "Q1");
                    addToOptionList(document.frmParams.periodstartmonth, "02", "Q2");
                    addToOptionList(document.frmParams.periodstartmonth, "03", "Q3");
                    addToOptionList(document.frmParams.periodstartmonth, "04", "Q4");
                    addToOptionList(document.frmParams.periodendmonth, "01", "Q1");
                    addToOptionList(document.frmParams.periodendmonth, "02", "Q2");
                    addToOptionList(document.frmParams.periodendmonth, "03", "Q3");
                    addToOptionListSelected(document.frmParams.periodendmonth, "04", "Q4");
                }
                else if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "03") {
					$('#seasonadjustspan').show();
                    document.frmParams.periodendyear.disabled = false;
                    document.frmParams.periodstartmonth.disabled = false;
                    document.frmParams.periodendmonth.disabled = false;
                    document.getElementById('startmonthlabel').innerHTML = "Start Month:";
                    document.getElementById('endmonthlabel').innerHTML = "End Month:";
                    // Year values
                    clearOptions(document.frmParams.periodstartyear);
                    clearOptions(document.frmParams.periodendyear);
					<?php generateYearOptionList("document.frmParams.periodstartyear"); ?> 
					<?php generateYearOptionList("document.frmParams.periodendyear"); ?> 
                    //
                    clearOptions(document.frmParams.periodstartmonth);
                    clearOptions(document.frmParams.periodendmonth);
                    addToOptionList(document.frmParams.periodstartmonth, "01", "January");
                    addToOptionList(document.frmParams.periodstartmonth, "02", "February");
                    addToOptionList(document.frmParams.periodstartmonth, "03", "March");
                    addToOptionList(document.frmParams.periodstartmonth, "04", "April");
                    addToOptionList(document.frmParams.periodstartmonth, "05", "May");
                    addToOptionList(document.frmParams.periodstartmonth, "06", "June");
                    addToOptionList(document.frmParams.periodstartmonth, "07", "July");
                    addToOptionList(document.frmParams.periodstartmonth, "08", "August");
                    addToOptionList(document.frmParams.periodstartmonth, "09", "September");
                    addToOptionList(document.frmParams.periodstartmonth, "10", "October");
                    addToOptionList(document.frmParams.periodstartmonth, "11", "November");
                    addToOptionList(document.frmParams.periodstartmonth, "12", "December");
                    addToOptionList(document.frmParams.periodendmonth, "01", "January");
                    addToOptionList(document.frmParams.periodendmonth, "02", "February");
                    addToOptionList(document.frmParams.periodendmonth, "03", "March");
                    addToOptionList(document.frmParams.periodendmonth, "04", "April");
                    addToOptionList(document.frmParams.periodendmonth, "05", "May");
                    addToOptionList(document.frmParams.periodendmonth, "06", "June");
                    addToOptionList(document.frmParams.periodendmonth, "07", "July");
                    addToOptionList(document.frmParams.periodendmonth, "08", "August");
                    addToOptionList(document.frmParams.periodendmonth, "09", "September");
                    addToOptionList(document.frmParams.periodendmonth, "10", "October");
                    addToOptionList(document.frmParams.periodendmonth, "11", "November");
                    addToOptionListSelected(document.frmParams.periodendmonth, "12", "December");
                }

                if (document.frmParams.periodendyear.disabled == false) {
                    if (document.frmParams.periodendyear[document.frmParams.periodendyear.selectedIndex].value < document.frmParams.periodstartyear[document.frmParams.periodstartyear.selectedIndex].value) {
                        document.frmParams.periodendyear.selectedIndex = document.frmParams.periodstartyear.selectedIndex;
                    }
                }
            }

			function onChangePeriod() {
				adjustPeriod();
				if (document.frmParams.periodtype[document.frmParams.periodtype.selectedIndex].value == "03") {
					$('#maintab').tabs('enable', 4);
				}
				else {
					$('#maintab').tabs('disable', 4);
				}
			}
			
			function showHideIndustryTree() {
			}
			
			function getVisibleTableColumns(resultsetCol) {
				var arr = new Array();
				arr.push(resultsetCol);
				arr.push(1);
				arr.push(4);
				arr.push(6);
				arr.push(7);
				
				return arr;
			}

			function formatColumn(colIndex, x) {
				if(colIndex == 9) {
					return formatPercent(x);
				}
				else if ((colIndex == 11 || colIndex == 12 || colIndex == 14 || colIndex == 15)) {
					return formatDollar(x);
				}
				
				return formatNumber(x);
			}

			function getPageTitle() {
				return "Employment Estimates for New York State and Metropolitan Areas";
			}
			
			function generateFilenameTitle() {
				return "CES_" 
						+ $('#resultset option:selected').text().replace(/ /g, '_') + "_"
						+ $('#periodtype option:selected').text() + "_" 
						+ $('#periodstartmonth option:selected').text() + "_" + $('#periodstartyear option:selected').text() + "_" 
						+ $('#periodendmonth option:selected').text() + "_" + $('#periodendyear option:selected').text()
						+ ($('#seasonadjust').prop('checked') ? "_seasonally_adjusted" : "")
						+ "_by_" + ($('#listofchosen').children().length == 0 ? "" : $('#regiontype option:selected').text().replace(/ /g, '_'))
						+ (document.frmParams.entirestate.checked ? (($('#listofchosen').children().length == 0) ? "Statewide" : "_and_Statewide") : "");
			}
			
			function generateTitle() {
				return "CES: " 
						+ $('#resultset option:selected').text() + ", "
						+ $('#periodtype option:selected').text() + ": " 
						+ $('#periodstartmonth option:selected').text() + " " + $('#periodstartyear option:selected').text() + " - " 
						+ $('#periodendmonth option:selected').text() + " " + $('#periodendyear option:selected').text()
						+ ($('#seasonadjust').prop('checked') ? " seasonally adjusted" : "")
						+ " by " + ($('#listofchosen').children().length == 0 ? "" : $('#regiontype option:selected').text())
						+ (document.frmParams.entirestate.checked ? (($('#listofchosen').children().length == 0) ? "Statewide" : " and Statewide") : "");
			}
			
			function shouldAdjustForRegion(region) {
				return true;
			}
			
        </script>

<form action="/" name="frmParams" class="ux-form-top">
    <div class="grid_11">
        <div class="dol-container">
            <h3>Data Type and Display Options</h3>
            <div class="grid_5">
                <label for="resultset">
                    <span>Data Type:</span>
                    <select name="resultset" id="resultset">
						<option value="7">Number Employed</option>
						<option value="8">Absolute Change Employed</option>
						<option value="9">Percent Change Employed</option>
						<option value="10">Hours (Production)</option>	
						<option value="11">Weekly Earnings (Production)</option>
						<option value="12">Hourly Earnings (Production)</option>
						<option value="13">Hours (All)</option>	
						<option value="14">Weekly Earnings (All)</option>
						<option value="15">Hourly Earnings (All)</option>
                    </select>
                </label>
            </div>
			<div class="grid_4 omega">
				<p>&nbsp;</p>
				<span id="seasonadjustspan">
					<input type="checkbox" name="seasonadjust" id="seasonadjust" /> 
					Seasonally Adjusted?
				</span>
			</div>			
        </div>
        <!-- end .dol-container -->
    </div>
    <div class="grid_11">
        <div class="dol-container">
            <h3>Geography Filter</h3>
            <div class="grid_11">
                <div class="grid_6 alpha">
                    <label for="regiontype">
                        <span>Type:</span>
                    </label>
					<input type="checkbox" name="entirestate" id="entirestate" /> State-wide data?
					<select name="regiontype" id="regiontype" onchange="populateGeographies();">
						<option value="04">County</option>
						<option value="21">Metropolitan Statistical Area</option>
					</select>
                </div>
                <div class="grid_5 omega">
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="grid_4 ux-selection-component-left">
                <label for="listofchoices"><span>Available Areas:</span>
                    <select name="listofchoices" id="listofchoices" multiple="multiple" size="7">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_2 ux-selection-component-middle">
                <div class="button-container">
                    <input type="button" value="Add" id="addButton" class="button next icon-right width-100" onclick="addToSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Add All" id="addAllButton" class="button next icon-right width-100" onclick="addAllToSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Remove" id="removeButton" class="button previous width-100" onclick="removeFromSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Remove All" id="removeAllButton" class="button previous width-100" onclick="removeAllFromSelected();" />
                </div>
            </div>
            <div class="grid_4 ux-selection-component-right">
                <label for="listofchosen"><span>Selected Areas:</span>
                    <select name="listofchosen" id="listofchosen" multiple="multiple" size="7">
                    </select>
                </label>
            </div>
        </div>
        <!-- end .dol-container -->
    </div>
    <div class="grid_11">
        <div class="dol-container">
            <h3>Period Filter</h3>
            <div class="grid_3">
                <label for="periodtype">
                    <span>Period Type:</span>
                    <select name="periodtype" id="periodtype" onchange="onChangePeriod();">
                        <option value="03">Monthly</option>
                        <option value="01">Annual Average</option>
                    </select>
                </label>
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodstartyear">
                    <span>Start Year:</span>
                    <select name="periodstartyear" id="periodstartyear" onChange="validateForStartYear();">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodendyear">
                    <span>End Year:</span>
                    <select name="periodendyear" id="periodendyear" onChange="validateForEndYear();">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_3">
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodstartmonth">
                    <span id="startmonthlabel">Start Month: </span>
                    <select name="periodstartmonth" id="periodstartmonth" onChange="validateForStartMonth();">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_4 alpha omega">
                <label for="periodendmonth">
                    <span id="endmonthlabel">End Month:</span>
                    <select name="periodendmonth" id="periodendmonth" onChange="validateForEndMonth();">
                        <option></option>
                    </select>
                </label>
            </div>
        </div> <!-- end .dol-container -->
    </div>
	<div class="grid_11">
		<div class="dol-container">
			<h3>Industry Filter</h3>
			&nbsp;<a href="#" id="btnSelectAll">Select all</a>&nbsp;&nbsp;
			<a href="#" id="btnDeselectAll">Deselect all</a>
			<div id="naicsTree">
			</div>
		</div>
		<!-- end .dol-container -->
	</div>	
</form>


<script type="text/javascript">
	function validateForStartYear() {
		if($('#periodendyear').val() != null){
			if($('#periodstartyear').val() > $('#periodendyear').val()){
				$('#periodendyear').val($('#periodstartyear').val()); 
			}		
		}
	}

	function validateForEndYear() {
		if($('#periodstartyear').val() != null){
			if($('#periodstartyear').val() > $('#periodendyear').val()){
				$('#periodstartyear').val($('#periodendyear').val()); 
			}		
		}
	}

	function validateForStartMonth() {
		if($('#periodendmonth').val() != null){
			if($('#periodstartmonth').val() > $('#periodendmonth').val()){
				$('#periodendmonth').val($('#periodstartmonth').val()); 
			}		
		}
	}

	function validateForEndMonth() {
		if($('#periodstartmonth').val() != null){
			if($('#periodstartmonth').val() > $('#periodendmonth').val()){
				$('#periodstartmonth').val($('#periodendmonth').val()); 
			}		
		}
	}

</script>
