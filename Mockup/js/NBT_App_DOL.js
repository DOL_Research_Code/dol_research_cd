/*
 * Copyright (c) 2012 NBT Solutions.
 */

/**
 * Startup for DOL application.
 *
 */
NBT.App.DOL = OpenLayers.Class(NBT.App, {

    /**
     * Tracking which feature is currently hovered over
     */
    hoveringOver: {},

    dataCache: {},

	proxyUrl: "/cgi-bin/proxy.py?url=",

    appDir: window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/')+1),
	
	/**
	 * Layers
	 */
	 lyrs: {
		countyLyr: new OpenLayers.Layer.Vector('Counties', {
          strategies: [new OpenLayers.Strategy.BBOX()],
          extractAttributes: true,
          styleMap: new OpenLayers.StyleMap({ 'default': {
															strokeWidth: 2,
															strokeColor: '#a36d00',
															fillColor: '#e7ffb3',
															fillOpacity: 0.4
														  }
	      }),
          projection: new OpenLayers.Projection('EPSG:3857'),
          visibility: true,
          protocol: new OpenLayers.Protocol.WFS({
              url: globalGISBaseUrl,//version=1.1.0&request=GetFeature&typeName=test:cnty',
              featureType: 'NY_Counties_10',
              featureNS: 'cite',
              srsName: 'EPSG:3857',
              srsNameInQuery: true,
              outputFormat: 'json',
              readFormat: new OpenLayers.Format.GeoJSON()
          })
        }),

		wiaLyr: new OpenLayers.Layer.Vector('WIAs', {
          strategies: [new OpenLayers.Strategy.BBOX()],
          extractAttributes: true,
          styleMap: new OpenLayers.StyleMap({ 'default': {
															strokeWidth: 2,
															strokeColor: '#a36d00',
															fillColor: '#e7ffb3',
															fillOpacity: 0.4
														  }
	      }),
          projection: new OpenLayers.Projection('EPSG:3857'),
          visibility: true,
          protocol: new OpenLayers.Protocol.WFS({
              url: globalGISBaseUrl,//version=1.1.0&request=GetFeature&typeName=test:cnty',
              featureType: 'NY_WIA_80',
              featureNS: 'cite',
              srsName: 'EPSG:3857',
              srsNameInQuery: true,
              outputFormat: 'json',
              readFormat: new OpenLayers.Format.GeoJSON()
          })
        }),

		msaLyr: new OpenLayers.Layer.Vector('MSAs', {
          strategies: [new OpenLayers.Strategy.BBOX()],
          extractAttributes: true,
          styleMap: new OpenLayers.StyleMap({ 'default': {
															strokeWidth: 2,
															strokeColor: '#a36d00',
															fillColor: '#e7ffb3',
															fillOpacity: 0.4
														  }
	      }),
          projection: new OpenLayers.Projection('EPSG:3857'),
          visibility: true,
          protocol: new OpenLayers.Protocol.WFS({
              url: globalGISBaseUrl,//version=1.1.0&request=GetFeature&typeName=test:cnty',
              featureType: 'NY_MSA_80',
              featureNS: 'cite',
              srsName: 'EPSG:3857',
              srsNameInQuery: true,
              outputFormat: 'json',
              readFormat: new OpenLayers.Format.GeoJSON()
          })
        }),
		
		regionLyr: new OpenLayers.Layer.Vector('Regions', {
          strategies: [new OpenLayers.Strategy.BBOX()],
          extractAttributes: true,
          styleMap: new OpenLayers.StyleMap({ 'default': {
															strokeWidth: 2,
															strokeColor: '#a36d00',
															fillColor: '#e7ffb3',
															fillOpacity: 0.4
														  }
		  }),
          projection: new OpenLayers.Projection('EPSG:3857'),
          visibility: true,
          protocol: new OpenLayers.Protocol.WFS({
              url: globalGISBaseUrl,//version=1.1.0&request=GetFeature&typeName=test:cnty',
              featureType: 'NY_Regions_10',
              featureNS: 'cite',
              srsName: 'EPSG:3857',
              srsNameInQuery: true,
              outputFormat: 'json',
              readFormat: new OpenLayers.Format.GeoJSON()
          })
        })
	 },
	
    /**
     * Selectable themes. This should be set in an ajax-pulled file.
     */
    thematicData: {
      none: {
		lyr: 'none',
        layer: 'none',
        idFieldName: 'NONE', // name of the field @ gis cloud.
        title: '',
        idIndex: 0,
		classifyIndex: 0, // The zero based column index of the target data value
        fields: null,
		lyrUpdater: null,
        data: null
      },

      county: {
		lyr: 'countyLyr',
        layer: 'Counties',
		idFieldName: 'NAME', // name of the field in the shapefile.
        title: null, //'2011 Employment by County',
        idIndex: null, // The zero based index of the ID field (the county name field, etc.).
		classifyIndex: null, // The zero based column index of the target data value
		fields: null, // [ 'unused', 'NAME', 'unused', 'unused', 'unused', 'unused', 'Employed' ] The names of specified columns
		lyrUpdater: null,
        data: null
      },

      wia: {
		lyr: 'wiaLyr',
        layer: 'WIAs',
		idFieldName: 'WIB_NAME', // name of the field in the shapefile.
        title: null, //'2011 Employment by County',
        idIndex: null, // The zero based index of the ID field (the county name field, etc.).
		classifyIndex: null, // The zero based column index of the target data value
		fields: null, // [ 'unused', 'NAME', 'unused', 'unused', 'unused', 'unused', 'Employed' ] The names of specified columns
		lyrUpdater: null,
        data: null
      },

      msa: {
		lyr: 'msaLyr',
        layer: 'MSAs',
		idFieldName: 'NAME', // name of the field in the shapefile.
        title: null, //'2011 Employment by County',
        idIndex: null, // The zero based index of the ID field (the county name field, etc.).
		classifyIndex: null, // The zero based column index of the target data value
		fields: null, // [ 'unused', 'NAME', 'unused', 'unused', 'unused', 'unused', 'Employed' ] The names of specified columns
		lyrUpdater: null,
        data: null
      },
	  
      region: {
		lyr: 'regionLyr',
        layer: 'Regions',
		idFieldName: 'LM_REGION', // name of the field in the shapefile.
        title: null, //'2011 Employment by County',
        //headers: null, //[ 'Name', 'Establishments', 'Total Employment' ]
        idIndex: null, // The zero based index of the ID field (the county name field, etc.).
		classifyIndex: null, // The zero based column index of the target data value
		fields: null, // [ 'unused', 'NAME', 'unused', 'unused', 'unused', 'unused', 'Employed' ] The names of specified columns
		lyrUpdater: null,
        data: null
      }	  
    },

    currentTheme: 'none',
    startingTheme: 'none',

    /**
     * Maintain selected features through zoom and featureType changes.
     */
    selectedFeatures: [],

    /**
     * Lookup map using GIS Cloud layer Name -> ID to select the 'right' layer
     * when selecting a theme.
     */
    layerIdNames: null,

    preMapInit: function()
    {
      var themeSelect = $('#theme');
      themeSelect.val('none');
      // TODO: allow this by query string.
      var selected = themeSelect.val();
      this.startingTheme = selected;

      //set up on-click
      themeSelect.change(function() {
          //app.selectTheme(themeSelect.val());
      });

      // set up 'legend' click:
      $('#legendbox h5').click(function() {
          $('#legend').toggle(250);
          $('#legendbox #x').toggle(250);
      });
      $('#legendbox #x').click(function() {
          $('#legend').hide(250);
          $('#legendbox #x').hide(250);
      });
    },

    getMapOptions: function()
    {
      return {
        layers: [new OpenLayers.Layer.OSM()],
		units: "degrees",
		zoom: 7,
		tileSize: new OpenLayers.Size(200, 200),
        center: OpenLayers.Layer.SphericalMercator.forwardMercator(-76,42.8),
        controls: [
          //update controls to add in 'predictive' zoom so we can swap out data before doing it.
          new OpenLayers.Control.Navigation({
              defaultDblClick: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = app.map.zoom + 1;
                OpenLayers.Control.Navigation.prototype.defaultDblClick.apply(this, arguments);
              },
              defaultDblRightClick: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = app.map.zoom - 1;
                OpenLayers.Control.Navigation.prototype.defaultDblRightClick.apply(this, arguments);
              },
              wheelChange: function(e) {
				// Removed per DOL request. http://support.garnetriver.net/itracker/module-projects/view_issue.do?id=76
              }
          }),
          new OpenLayers.Control.Zoom({
              onZoomClick: function(e) {
                app.map.zoomChanged = true;
                app.map.newZoom = (e.buttonElement.hash == '#zoomIn') ? app.map.zoom + 1 : map.zoom - 1;
                OpenLayers.Control.Zoom.prototype.onZoomClick.apply(this, arguments);
              }
          })
        ]
      };
    },

    postMapInit: function()
    {

	  //this.lyrs['countyLyr'].setVisibility(false);
      this.lyrs['countyLyr'].events.register('featureselected', this, this.selectFeature);
      this.lyrs['countyLyr'].events.register('featureunselected', this, this.onFeatureRemoved);
      this.lyrs['countyLyr'].events.register('refresh', this, function(e) {
      });
      this.map.addLayer(this.lyrs['countyLyr']);

	  //this.lyrs['wiaLyr'].setVisibility(false);
      this.lyrs['wiaLyr'].events.register('featureselected', this, this.selectFeature);
      this.lyrs['wiaLyr'].events.register('featureunselected', this, this.onFeatureRemoved);
      this.lyrs['wiaLyr'].events.register('refresh', this, function(e) {
      });
      this.map.addLayer(this.lyrs['wiaLyr']);

	  //this.lyrs['wiaLyr'].setVisibility(false);
      this.lyrs['msaLyr'].events.register('featureselected', this, this.selectFeature);
      this.lyrs['msaLyr'].events.register('featureunselected', this, this.onFeatureRemoved);
      this.lyrs['msaLyr'].events.register('refresh', this, function(e) {
      });
      this.map.addLayer(this.lyrs['msaLyr']);
	  
	  //this.lyrs['regionLyr'].setVisibility(false);
      this.lyrs['regionLyr'].events.register('featureselected', this, this.selectFeature);
      this.lyrs['regionLyr'].events.register('featureunselected', this, this.onFeatureRemoved);
      this.lyrs['regionLyr'].events.register('refresh', this, function(e) {
      });

      this.map.addLayer(this.lyrs['regionLyr']);

	  // add Regions layer end.	  
	  var countyLayerUpdater = new NBT.Thematic.LayerUpdater({layer: this.lyrs['countyLyr']});
	  var wiaLayerUpdater = new NBT.Thematic.LayerUpdater({layer: this.lyrs['wiaLyr']});
	  var msaLayerUpdater = new NBT.Thematic.LayerUpdater({layer: this.lyrs['msaLyr']});
	  var regionLayerUpdater = new NBT.Thematic.LayerUpdater({layer: this.lyrs['regionLyr']});
	  this.thematicData['county'].lyrUpdater = countyLayerUpdater;
	  this.thematicData['wia'].lyrUpdater = wiaLayerUpdater;
	  this.thematicData['msa'].lyrUpdater = msaLayerUpdater;
	  this.thematicData['region'].lyrUpdater = regionLayerUpdater;
	  
      this.map.events.register('movestart', this, function(e) {
          if (this.map.zoomChanged)
          {
		    // This is where the different layers per zoom level were loaded. KH.
            //this.layerUpdater[0].updateFeatureTypeOnZoom();
          }
          this.map.zoomChanged = false; // because movestart event happens on a drag as well.
      });

      var selectFeature = new OpenLayers.Control.SelectFeature(
          this.map.getLayersByClass(/Vector/), // getFeatureLayers eventually
          {
            selectStyle: {
              strokeColor: '#ffdd00',
              fillColor: '#ff9999',
              fillOpacity: 0.6
            },
            toggle: true,
            multiple: true,
            hover: true,
            highlightOnly: true,
            outFeature: function(feature) {
              // override to allow hover + click highlighting
              var selected = (OpenLayers.Util.indexOf(
                feature.layer.selectedFeatures, feature) > -1);
              if(!selected) {
                this.unhighlight(feature);
              }
              app.unHoverFeature();
            },
            clickFeature: function(feature) {
              // override to allow hover + select toggle on click.
              var selected = (OpenLayers.Util.indexOf(
                feature.layer.selectedFeatures, feature) > -1);
              if(selected) {
                  if(this.toggleSelect()) {
                      this.unselect(feature);
                  } else if(!this.multipleSelect()) {
                      this.unselectAll({except: feature});
                  }
              } else {
                  if(!this.multipleSelect()) {
                      this.unselectAll({except: feature});
                  }
                  if (app.currentTheme != 'none')
                  {
                    this.select(feature);
                  }
              }
            }
          });
      selectFeature.handlers['feature'].stopDown = false;
      selectFeature.events.register('featurehighlighted', this, function(e) {
          if (this.shouldDoHover(e.feature))
          {
            this.hoverInterval = setInterval(function() {
                e.mouseX = app.mouseX;
                e.mouseY = app.mouseY;
                app.hoverFeature(e);
                clearInterval(app.hoverInterval);
                app.hoverInterval = null;
            }, 500);
          }
        });
      selectFeature.events.register('featureunhighlighted', this, this.unHoverFeature);

      this.map.addControl(selectFeature);
      selectFeature.activate();

      $(document).mousemove( function(e) {
          app.mouseX = e.pageX;
          app.mouseY = e.pageY;
      });
	  
      for (var i = 1; i < this.map.layers.length; i++)
      {
	      var layerName = this.map.layers[i].name;
		  if(layerName.indexOf("OpenLayers") != 0) {	
              this.map.layers[i].setVisibility(false);
		  }
      }
    },

    /**
     * We can't use the standard selectedFeatures array because the features
     * will get swapped out as the user zooms in and out. So, we maintain the
     * names of the features here.
     */
    isFeatureSelected: function(id) {
      return this.selectedFeatures.indexOf(id);
    },

    getSelectControl: function() {
      return this.map.getControlsByClass(/SelectFeature/)[0];
    },

    selectFeature: function(e) {
      if (this.currentTheme == 'none')
      {
        this.unSelectFeature(e);
      }
      else
      {
        if (e.feature)
        {
          this.onFeatureAdded(e.feature);
        }
        else
        {
          this.statusMessage('No feature data to display');
        }
      }

    },

    /**
     * Unselect a feature - need seperate access to unselect a single
     * feature by clicking the X in the tabular data.
     */
    unselectFeature: function(e)
    {
      e.feature = this.getFeatureByFeature(e.feature);
      this.getSelectControl().unselect(e.feature);
      this.onFeatureRemoved(e);
    },

    /**
     * Problem is, when switching featureTypes and loading new Vectors, the
     * OL classes can get 'lost'. But we can look up the feature we want in the
     * current set by using the name of the feature we had originally.
     * // XXX doesn't appear to be working.
     */
    getFeatureByFeature: function(feature)
    {
      if (feature.layer) return feature; // if layer is still attached we're ok

      if (this.currentTheme == 'none') return feature;
      var theme = this.thematicData[this.currentTheme];
      var id = feature.attributes[theme.idFieldName];
      for (var i = 0; i < this.map.layers[1].features; i++)
      {
        if (this.map.layers[1].features[i].attributes[id] == feature.attributes[id])
        {
          return this.map.layers[1].features[i];
        }
      }

      return feature; // just avoiding nulls.
    },

    hoverFeature: function(e)
    {
      if (e.feature)
      {
        var idFld = 'NAME'; // TODO: expand this to getIdField() and reuse.
        if (this.currentTheme != 'none')
        {
          var theme = this.thematicData[this.currentTheme];
          idFld = theme['idFieldName'];
          var location = e.feature.attributes[idFld];
          var periodName = e.feature.attributes['Period Name'];
		  var dataColumnName = theme['fields'][ theme['classifyIndex'] ];
		  var hover = $('#hover');
          hover.data('feature', e.feature);
          hover.css('top', e.mouseY + 5);
          hover.css('left', e.mouseX + 5);
          hover.html('<h3>' + location + '</h3>').show();
          var ul = $('<ul>');
		  ul.append('<li><strong>Period:</strong> ' + periodName + '</li>');
		  ul.append('</ul>');
		  ul.append('<ul>');
		  ul.append('<li><strong>' + dataColumnName + ':</strong> ' + formatColumn(theme['classifyIndex'], e.feature.attributes[dataColumnName]) + '</li>');
		  ul.append('</ul>');
          hover.append(ul);
          //highlightChart(location);
        }
      }
    },

    /**
     * What to do when a feature is no longer hovered-over.
     */
    unHoverFeature: function(e)
    {
      clearInterval(app.hoverInterval);
      app.hoverInterval = null;
      setTimeout(function() {
          app.clearHover();
      }, 500);
    },

    statusMessage: function(message)
    {
      alert(message); // meant to be replaced.
    },

    onFeatureAdded: function(feature)
    {
      var attributes = feature.attributes;
      var theme = this.thematicData[this.currentTheme];
      var found = false;
      var name = attributes[theme.idFieldName];
      
	  adjustForRegion(name);
    },

    onFeatureRemoved: function(e)
    {
      var theme = this.thematicData[this.currentTheme];
      var toRemove = e.feature.attributes[theme.idFieldName];
      for (var i = 0; i < this.selectedFeatures.length; i++)
      {
        if (this.selectedFeatures[i] == toRemove)
        {
          this.selectedFeatures.splice(i, 1);
          break;
        }
      }

      $('#data table tr').each(function() {
          var row = $(this);
          if (row.data('feature') == e.feature)
          {
            row.remove();
            if ($('#data table tr').length <= 1) // only the header row.
            {
              $('#data-tools').remove();
            }
          }
      });
    },

    getSelectedFeatures: function() {
      // TODO: Consider adding the thematic details like title and field names
      return this.selectedFeatures;
    },

    /**
     * Should we show the hover popup? Answer is yes unless it's already showing
     * or if we're re-highlighting features based on a zoom/data change.
     */
    shouldDoHover: function(feature) {
      // a temporary setting from re-highlighting features on zoom/data change:
      if (feature.noHover)
      {
        feature.noHover = false;
        return false;
      }

      if (feature.id != this.hoveringOver.featureId ||
        feature.layer.name != this.hoveringOver.layerName)
      {
        this.hoveringOver = { layerName: feature.layer.name, featureId: feature.id };
        if (this.hoverInterval)
        {
          clearInterval(this.hoverInterval);
        }
        return true;
      }

      return false;
    },

    clearHover: function() {
      $('#hover').html('').hide();
      this.hoveringOver = {};
    },

    selectThemeWithData: function(theme, aaData, aoColumns, title, idIndex, classifyIndex) {
	   var legendtext='';

      // drop all selected features, ensure a fresh start:
      this.getSelectControl().unselectAll();

      if (this.thematicData[theme])
      {
		var fieldArr = new Array();
		for(var i in aoColumns) {
			fieldArr[i] = aoColumns[i]['sTitle'];
		}
	  
        this.currentTheme = theme;
        var meta = this.thematicData[theme];
		meta.title = title;
		meta.idIndex = idIndex;
		meta.classifyIndex = classifyIndex;
		fieldArr[idIndex] = meta.idFieldName;
		meta.fields = fieldArr;
		//meta.headers = [fieldArr[classifyIndex] ]; 
		meta.data = aaData;

		            
		var theLayerUpdater = meta.lyrUpdater;
		var keyFieldName = meta.idFieldName;

		
		theLayerUpdater.addArrayData(
		  meta.fields,
		  keyFieldName,
		  meta.data);


		var cl = new NBT.Thematic.Classifier();
		var classes = cl.calculateClasses(meta.data, meta.classifyIndex, 5);

		var baseStyle = {
		  strokeWidth: 1,
		  strokeColor: '#ffdd00',
		  fillOpacity: 0.5
		};

		var colors = [ '#DD00DD','#0000DD', '#00CCCC', '#00AA00',  '#DDAA00' ];
		for (var i in classes )
		{
		  classes[i].style = {
			fillColor: colors[i]
		  };
		}

		var rules = cl.buildRules(meta.fields[meta.classifyIndex], classes, baseStyle);
		var layers = app.map.getLayersByName(meta.layer);
		for (var i = 0; i < layers.length; i++)
		{
		  //layers[i].loaded = false;
		  layers[i].styleMap.styles['default'].addRules(rules);
		  layers[i].redraw();
		  //layers[i].setVisibility(true);
		  //layers[i].refresh({force: true});
		}
		
		legendtext +='<ul style="margin: 0; margin-bottom: 5px; padding: 0; list-style: none;">';
		   
		for (var i = 0; i < classes.length; i++)
		{
		   
		   legendtext += '<li style="font-size: 80%; list-style: none; margin-left: 0; line-height: 18px; margin-bottom: 2px;"><span style="background-color:'+colors[i]+';display: block; float: left; height: 16px; width: 30px; margin-right: 5px; margin-left: 0; border: 1px solid #999; opacity: 0.5;"></span>'+formatNumber(classes[i].low) + ' - ' +formatNumber(classes[i].high) + '</li>';
		}
		legendtext +='</ul>';
		   // change the legend:
		var leg = $('#legend');
		leg.empty();
		if (theme != 'none')
		{
		   leg.append('<h6>' + $('#resultset option:selected').html() + '</h6>');
		   //leg.append('<img src="img/' + theme + '.png" />');
		   leg.append(legendtext);
		}
		else
		{
		   leg.append('<h6>Select a theme</h6>');
		   leg.hide(250);
		   $('#legendbox #x').hide(250);
		 }	  	  					   
        for (var i = 1; i < this.map.layers.length; i++)
        {
		  var layerName = this.map.layers[i].name;
          if (layerName == meta.layer || layerName == "OpenStreetMap") {
            this.map.layers[i].setVisibility(true);
          }
          else if(layerName.indexOf("OpenLayers") != 0)
          {
            this.map.layers[i].setVisibility(false);
          }
        }
		

        var d = $('#data');
        d.empty();
        var text = 'Click an area to include its data here...';
        if (theme == 'none')
        {
          text = 'Choose a theme then click features to select data';
        }
        d.append('<p class="ux-msg">' + text + '</p>');

   
      }
      else
      {
        alert('Unknown theme chosen');
      }
    },
	
    /**
     * Grab selected data, which uses this.selectedFeatures to grab the current
     * set, and this.thematicData to pull 'interesting' attributes (and headers)
     * all thrown together in a single object suitable for json'ing.
     * TODO: consider finding the polygon and including it. can't do it now
     * because the giscloud output is in screen coords.
     */
	/* 
    exportData: function() {
      var data = {};
      if (this.currentTheme == 'none' ||
        typeof(this.thematicData[this.currentTheme]) == 'undefined')
      {
        return data;
      }

      data.headers = this.thematicData[this.currentTheme].headers;
      data.rows = [];
      for (var i = 0; i < this.selectedFeatures.length; i++)
      {
        var base = this.selectedFeatures[i];
        // has to be in data cache or you wouldn't be here:
        var feat = this.dataCache[base.layerId][base.featureId];
        if (typeof(feat) != 'undefined' && feat.thematicData)
        {
          data.rows.push(feat.thematicData);
        }
      }

      return data;
    }
	*/
});


