/*
 * Copyright (c) 2012 NBT Solutions.
 */

/**
 * Read incoming data structure and add given data to a Vector Layer's attributes.
 *
 * @requires NBT.js
 */
NBT.Thematic.LayerUpdater = OpenLayers.Class(NBT.Class, {

    layer: null,

    // XXX: Must not keep this here, dummy!
    zoomToFeatureType: {
      'default':  'NY_Counties',//'cnty500',
      '0':  'NY_Counties',//'cnty1000',
      '1':  'NY_Counties',//'cnty1000',
      '2':  'NY_Counties',//'cnty1000',
      '3':  'NY_Counties',//'cnty1000',
      '4':  'NY_Counties',//'cnty1000',
      '5':  'NY_Counties',//'cnty1000',
      '6':  'NY_Counties',//'cnty1000',
      '7':  'NY_Counties',//'cnty1000',
      '8':  'NY_Counties',//'cnty500',
      '9':  'NY_Counties',//'cnty500',
      '10':  'NY_Counties',//'cnty200',
      '11':  'NY_Counties',//'cnty100',
      '12':  'NY_Counties',//'cnty100',
      '13':  'NY_Counties',//'cnty50',
      '14':  'NY_Counties',//'cnty20',
      '15':  'NY_Counties',//'cnty20',
      '16':  'NY_Counties',//'cnty',
      '17':  'NY_Counties',//'cnty',
      '18':  'NY_Counties',//'cnty'
    },

    initialize: function(options)
    {
        OpenLayers.Util.extend(this, options);
    },

    /**
     * Add data based on simple array of arrays. Requires a 'key' array to be
     * passed in too so we know what attributes to create.
     */
    addArrayData: function(fieldNames, keyField, data)
    {
      this.addedFieldNames = fieldNames;
      this.addedKeyField = keyField;
      this.addedData = data;

      this.layer.events.register('featuresadded', this, this.updateLayerData);

      this.updateLayerData();
    },

    updateLayerData: function()
    {
      if (!this.addedFieldNames)
      {
        return; // no data has been added, nothing to update.
      }

      // TODO: Find a better way to find the layer feature.
      var fieldNames = this.addedFieldNames;
      var keyField = this.addedKeyField;
      var data = this.addedData;

      var idField = ''; // used to find matching Feature.
      var idFieldNum = 0;
      if (isNaN(keyField))
      {
        idField = keyField;
        for (var i in fieldNames)
        {
          if (fieldNames[i].toUpperCase() == keyField.toUpperCase())
          {
            idFieldNum = i;
          }
        }
      }
      else
      {
        // TODO: regex this against field names in feature[0] and match the case
        idField = fieldNames[keyField];
        idFieldNum = keyField;
      }

	  
	  
      for (var f in this.layer.features) // For each feature (as f)
      {
	    var bFound = false;
        for (var i in data) // For each data (as i)
        {
          if (this.layer.features[f].attributes[idField] == data[i][idFieldNum])
          {
		    bFound = true;
            for (var j in fieldNames) // For each field name (as j)
            {
              if (!this.layer.features[f].attributes[fieldNames[j]])
              {
                this.layer.features[f].attributes[fieldNames[j]] = data[i][j];
              }
            }
			break; // No reason to keep looking through data
		  }
	    }
		
		if(!bFound)
		{
          for (var j in fieldNames) // For each field name (as j)
		  {
		    if(fieldNames[j] != idField)
			{
		      this.layer.features[f].attributes[fieldNames[j]] = null;
			} 
		  }
		}
	  }
	  
      this.layer.redraw();
    },

    updateFeatureTypeOnZoom: function()
    {
      if (this.layer.map.zoomChanged)
      {
        // maintain current selection across zoom:
        var currentFT = this.layer.protocol.options.featureType;

        // TODO: find some elegant way to set this programmatically (remove hardcoded zoom levels)
        // perhaps determine the future zoom's scale (meters per pixel) and choose?
        var newZoom = this.layer.map.newZoom;
        var newFT = this.zoomToFeatureType['default'];
        if (this.zoomToFeatureType[newZoom])
        {
          newFT = this.zoomToFeatureType[newZoom];
        }

        if (currentFT != newFT)
        {
          // console.log('new FT');
          this.layer.protocol.options.featureType = newFT;
          this.layer.refresh({force:true});
        }
      }

      this.layer.map.zoomChanged = false;
    },

    CLASS_NAME: 'NBT.Thematic.LayerUpdater'
});
