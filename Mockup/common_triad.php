<?php 
global $parentName;
include("config.php");
?>


<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Labor Statistics</title>

			<link rel="shortcut icon" href="http://labor.ny.gov/css/apps/v.0.4.3/favicon.ico" type="image/x-icon" />
			<link rel="stylesheet" type="text/css" href="http://labor.ny.gov/css/apps/v.0.4.3/css/ux-style.min.css" />	
			<script type="text/javascript" src="http://labor.ny.gov/css/apps/v.0.4.3/js/modernizr.min.js"></script>

			<!-- Map support -->
			<link rel="stylesheet" type="text/css" href="css/dol.css"></link>

			<!-- Google Chart support -->
            <script type="text/javascript" src="http://www.google.com/jsapi"></script>
	</head>

	<body class=" ">

	<div id='hover' class="dol-container"></div>
		<div id="dol-skip-navigation">
			<a tabindex="1" id="skip-to-content" href="#content-anchor" class="ux-no-block">Skip to Content</a>	
			<a tabindex="2" 
							id="skip-to-nav" href="#navigation-anchor"
			 class="ux-no-block" >Skip to Navigation</a>
		</div>
			
		<div id="ux-notification-container">
			<div class="ux-content-wrapper">
				<div id="ux-action-required-notifications" class="ux-notice-outer">
						<div class="arn-tray ux-notice-inner">
						</div>
				</div>
				<div id="ux-generic-notifications">
				</div>
			</div>
		</div>
		
		<div id="gov-banner-outer" class="container_16">
			<div id="gov-banner-inner">
				<a href="http://www.ny.gov/"><img id="nys-image" src="http://labor.ny.gov/css/apps/v.0.4.3/images/banner_img1.png" alt="NY.gov Portal" /></a>
				<a href="http://www.nysegov.com/citGuide.cfm?superCat=102&cat=449&content=main"><img id="nys-agency-listing" src="http://labor.ny.gov/css/apps/v.0.4.3/images/banner_img2.png" alt="State Agency Listing" /></a>
				<!-- search graphic-->
				<a onclick="document.getElementById('sw_searchbox').style.visibility= 'visible'; document.getElementById('searchgraphic').style.visibility= 'hidden';" href="#">
				<img alt="Search all of NY.gov" src="http://labor.ny.gov/css/apps/v.0.4.3/images/banner_img3.png" style="float: right; border: medium none; visibility: hidden;" id="searchgraphic"></a>
				<!--state wide search box-->
				<form method="get" action="http://search.cio.ny.gov/" style="float: right; position: relative; top: 2px; left: 155px; visibility: visible;" id="sw_searchbox" target="_blank">
					<p>
						<input type="hidden" name="sort" value="date:D:L:d1" />
						<input type="hidden" name="output" value="xml_no_dtd" />
						<input type="hidden" name="ie" value="UTF-8" />
						<input type="hidden" name="oe" value="UTF-8" />
						<input type="hidden" name="client" value="default_frontend" />
						<input type="hidden" name="proxystylesheet" value="default_frontend" />
						<input type="hidden" name="site" value="default_collection" />
						<label for="searchbox"><input type="text" title="Search" id="searchbox" maxlength="256" name="q" size="15" style="position: relative; top: -10px; cursor: pointer;" /></label>
						<label for="searchbutton"><input type="submit" value="Search NY.GOV" id="searchbutton" style="position: relative; width: 110px; top: -10px;" /></label>
					</p>
				</form>
				<!--end wide search box-->
				<script type="text/javascript">
					// If JavaScript is on, manipulate search control objects.  
					// Otherwise this will be ignored and search controls will be shown by default
					document.getElementById('sw_searchbox').style.visibility = 'hidden';
					document.getElementById('searchgraphic').style.visibility= 'visible';         
				</script> 
			</div>
		</div>
		<div id="dol-header" class="container_16">
			<h1 id="dol-header-nys-labor">
				<a href="http://www.labor.ny.gov" class="accessibility-text">New York State Department of Labor</a>
				<span id="dol-header-application">Labor Statistics</span>
			</h1>

			<div id="site-wide-search">
				<form name="_ipsubmit" id="search-box-form" method="get" action="http://search.cio.ny.gov" autocomplete="off" target="_blank" >
					<label for="q" id="search-label" class="accessibility-text">Search Department of Labor:</label>
					<input type="text" id="q" name="q" value="" size="21" title="Input Search" class="text_box site-wide-search-text-box" placeholder="Search DOL" autocomplete="off" />
					<input type="submit" title="Submit Search" value="GO" name="submit" class="submit site-wide-search-button accessibility-text" id="search-submit" />
					<input type="hidden" name="entqr" value="0" />
					<input type="hidden" name="ud" value="1" />
					<input type="hidden" name="sort" value="date:D:L:d1" />
					<input type="hidden" name="output" value="xml_no_dtd" />
					<input type="hidden" name="oe" value="UTF-8" />
					<input type="hidden" name="ie" value="UTF-8" />
					<input type="hidden" name="client" value="labor_frontend" />
					<input type="hidden" name="proxystylesheet" value="labor_frontend" />
					<input type="hidden" name="site" value="labor_collection" />
				</form>
			</div>

		 	<div id="header-mega-menu">
		   		<div id="mega-menu">
		   			<a name="mega-menu-anchor" id="mega-menu-anchor" tabindex="-1"></a>
		   			<ul id="menu">
		   			</ul>
		   		</div>
			</div>
		</div>	<!-- Mega menu entry below this line if needed -->

		<div id="page-wrap" class="group">
			<div id="content" class="container_16 group">
				<noscript>
					<div class="grid_16">
						<div class="ux-msg warning">
							<ul>
								<li><strong>Javascript is currently disabled or restricted on your computer. </strong> Certain functionalities of this application may be limited. </li>
							</ul>
						</div>
					</div>
				</noscript>
				<a name="navigation-anchor" id="navigation-anchor" tabindex="-1"></a>
				<div id="dol-content-navigation" class="grid_4">
										
					<div id="navigation-vertical">
						<ul>
							<li>
								<a href="#">Nav goes here</a>
							</li>
						</ul>
					</div>
				</div>
			   	<a name="content-anchor" id="content-anchor" tabindex="-1"></a>		
				<div class="grid_12 ux-content-grid ">
					<div id="page-info">
						<ul class="dol-breadcrumbs group">
							<li class="home breadcrumb-external"><a href="http://www.labor.ny.gov">Home</a></li>
					        <li><?php echo strtoupper($parentName); ?></li>
						</ul>
					</div>
					<h2><?php echo strtoupper($parentName); ?> Data Selection</h2>	
					<!-- ============================================================= -->
					<!-- =================== CONTENT STARTS HERE ===================== -->
					<!-- ============================================================= -->

                    <div class="jQTabs" id="maintab">
                        <ul>
                            <li id="tab-li-1"><a href="#tabs-1" id="tabs-1link">Data Selection</a></li>
                            <li id="tab-li-2"><a href="#tabs-2" id="tabs-2link">Map</a></li>
                            <li id="tab-li-3"><a href="#tabs-3" id="tabs-3link">Chart</a></li>
                            <li id="tab-li-4"><a href="#tabs-4" id="tabs-4link">Table</a></li>
                            <li id="tab-li-5"><a href="#tabs-5" id="tabs-5link">YBM</a></li>
                        </ul>
                        <div id="tabs-1">
                            <div class="ux-content-wrapper">
                                <?php include($parentName . "_param_tab.php"); ?>
								<div class="grid_11" id="dataselection">
									<input type="button" value="Display" onclick="$('#tabs-4link').click();" />
									After selecting your data, click the "Display" button to the left to display a table of your data or click the "Map" or "Chart" tabs above.
								</div>
                            </div>							
                        </div>
                        <div id="tabs-2" >
                            <div class="ux-content-wrapper">
                                <div class="grid_11">
                                    <div class="dol-container">
                                        <h3>Map</h3>										
                                        <div class="dol" id="app">
                                            <div id="legendbox">
                                              <div id="x">X</div>
                                              <div id="legend"></div>
                                              <h5>Map Legend</h5>
                                            </div>
                                            <div id='map'></div>
											<div id='tools'></div>
											
                                        </div>
                                    </div>
                                    <!-- end .dol-container -->
                                </div>
								<div class="grid_11">
									 <input type="button" id="previousParametersMapButton" value="Previous Parameters" onclick="adjustBackToPrevious();" />									 
								</div>
							</div>
						</div>
                        <div id="tabs-3" >
                            <div class="ux-content-wrapper">
                                <div class="grid_11">
                                    <div class="dol-container">
                                        <h3>Chart</h3>
											<label for="chartType">
												<span>Chart Type:</span>
												<select id="chartType" name="chartType" onChange="changeChartType(this.options[this.selectedIndex].value)" Display="none">
													<option value="1">Horizontal Bar</option>
													<option value="2">Vertical Bar</option>
													<option value="3">Line</option>
												</select>
											</label>
											<label for="primaryaxis">
												<span>By:</span>
												<select name="primaryaxis" id="primaryaxis" onChange="drawChart()">
												</select>
											</label>
											
											<input type="checkbox" name="zeroorigin" id="zeroorigin" onchange="changeChartType($('#chartType').val())" /> 
											Origin at Zero
                                        <div id="chart_div"></div>
										
                                    </div>
									<div class="grid_11" id="chartcsvdiv" >
										Download chart:
										<a download="table.csv" href="#" id="convertcsv"><input type="button" value="CSV" /></a>
									</div>
                                </div>
								<div class="grid_11">
									 <input type="button" id="previousParametersChartButton" value="Previous Parameters" onclick="adjustBackToPrevious();" />									 
								</div>
							</div>
						</div>
                        <div id="tabs-4" >
                            <div class="ux-content-wrapper">
                                <div class="grid_11">
                                    <div class="dol-container">
                                        <h3 id="tabletitle">Table</h3>
                                        <table class="data-table datatable_custom_class" id="datatable">
                                            <caption id="datatable_caption">Data</caption>
                                        </table>
                                    </div>
                                    <!-- end .dol-container -->
                                </div>
                                <div class="grid_11" id="tablecsvdiv" >
                                    Download table:
                                    <a download="table.csv" href="#" id="tableconvertcsv"><input type="button" value="CSV" /></a>
                                </div>
								<div class="grid_11">
									 <input type="button" id="previousParametersTableButton" value="Previous Parameters" onclick="adjustBackToPrevious();" />									 
								</div>
                            </div>
                        </div>
                        <div id="tabs-5" >
                            <div id="ybmwrapper0" class="ux-content-wrapper">
                                <div class="grid_11">
                                    <div class="dol-container">
                                        <table class="data-table datatable_custom_class">
                                            <caption class="datatable_caption">YBM</caption>
                                        </table>
                                    </div>
                                </div>
								
                                <div class="grid_11">
                                    Download table:
                                    <a download="table.csv" href="#" class="ybmtableconvertcsv"><input type="button" value="CSV" id="ybmcsvbutton" class="ybmtablebuttonclass" /></a>
                                </div>
								
                            </div>
                        </div>
                    </div>
					<!-- ============================================================= -->
					<!-- =================== CONTENT ENDS HERE ======================= -->
					<!-- ============================================================= -->

				</div>
			</div>
		</div>	 

		<div id="dol-footer">
			<div id="dol-footer-inner" class="container_12">
				<div id="dol-footer-wrapper">
					<!-- begin global footer area -->
						<ul id="global-footer-menu">
							<li><a href="http://labor.ny.gov/secure/contact/" target="_blank">Contact Us</a></li>
							<li><a href="http://www.labor.ny.gov/agencyinfo/accessibility.shtm" target="_blank">Accessibility Policy</a></li>
							<li><a href="http://labor.ny.gov/privacy.shtm" target="_blank">Privacy Policy</a></li>
							<li><a href="http://labor.ny.gov/utilities/document-readers.shtm" target="_blank">Document Readers</a></li>
						</ul>
					<!-- end global footer area -->
				</div>
			</div>
		</div> <!-- end #footer -->

		<script type="text/javascript" src="http://labor.ny.gov/css/apps/v.0.4.3/js/ux-script.min.js"></script>
		

		<!-- Additional scripts below this line -->
        <script type="text/javascript">
			var globalParentName = "<?php echo $parentName?>";
			var globalWSBaseUrl = "<?php echo WS_BASE_URL?>";
			var globalGISHost = "<?php echo GIS_HOST?>"; 
			var globalGISBaseUrl = "<?php echo GIS_BASE_URL?>";
		</script>
        <script type="text/javascript" src="common.js"></script>

<?php

		function generateOptionList($firstParam, $sql) {
			$serverName = DB_HOST;
			$connectionInfo = array("Database"=>DB_DATABASE, "UID"=>DB_USER, "PWD"=>DB_PASSWORD);
			$conn = sqlsrv_connect($serverName, $connectionInfo);

			if($conn === false) {
				die(print_r( sqlsrv_errors(), true));
			}

			$stmt = sqlsrv_query( $conn, $sql);
			if($stmt === false) {
				die( print_r( sqlsrv_errors(), true) );
			}

			while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC) ) {
				$areaName = $row[1];
				//if(strstr($areaName, " County")) {
				//	$areaName = substr($areaName, 0, strlen($areaName) - 7);
				//}
			
				echo "addToOptionList(".$firstParam.", \"".$row[0]."\", \"".$areaName."\");\n";
			}

			sqlsrv_free_stmt( $stmt);
		}

		function generateYearOptionList($firstParam) {
			for($i = 1990; $i <= date('Y'); $i++) {
				$selected = "";
				if ($i == date('Y')) {
					$selected = "Selected";
				}
				echo "addToOptionList".$selected."(".$firstParam.", \"".$i."\", \"".$i."\");\n";
			}
		}
?>	
	
        <script type="text/javascript">
			var globalNeedsRefresh = true;
			var globalVisitedTableTab = false;
			var globalVisitedChartTab = false;
			var currentTabIndex = -1;
			$(document).ready(function () {		  
				//
				$('h2').text(getPageTitle() + " Data Selection");
				$("#chartType").prop("disabled", true);
				$("#primaryaxis").prop("disabled", true);
				
                ux.load("jqueryTabs", function () {
                    $('#maintab').tabs({
                        show: function (event, ui) {
							currentTabIndex = ui.index;
							if(ui.index == 0) {
								$('h2').text(getPageTitle() + " Data Selection");
								globalNeedsRefresh = true;
								globalVisitedChartTab = false;
								clearPreviousParameters();
							}
							if(ui.index == 1) {
								if((!window.app) || (!(window.app instanceof NBT.App.DOL))) {
									window.app = new NBT.App.DOL();
									if(!globalNeedsRefresh) {
										drawMap();
									}
								}
							}
							if(ui.index == 2) {
								if(!globalVisitedChartTab) {
									globalVisitedChartTab = true;
									if ($('#primaryaxis').val() == 2) { // Period
										$('#chartType').val(3);
										changeChartType(3);
										//default to line chart if by period
									}
									else {
										$('#chartType').val(1);
										changeChartType(1); // default to bar chart
									}
								}
							}
							if(ui.index == 3) {
								globalVisitedTableTab = true;
								if(!globalNeedsRefresh) {
									drawTable();
								}
							}
                            if (ui.index != 0)  {
								//var pageTitle = "<?php echo strtoupper($parentName); ?>";
								//pageTitle += " - ";
								//pageTitle += $('#resultset option:selected').text()
								$('h2').text(generateTitle());

								if(globalNeedsRefresh) {
									globalNeedsRefresh = false;
									refreshData();
								}
                            }
                        }
                    });
                });

				// Hide the YBM tab for everything except ces and laus
				if("<?php echo $parentName?>" == "qcew" || "<?php echo $parentName?>" == "oes" || "<?php echo $parentName?>" == "ui") {
					$('#tab-li-5').hide();
				}
				
				// Load the table
				ux.load("dataTables");
				
				// Populate the chart "By:" dropdown
				populatePrimaryAxis();
				
                // Make initial call to populate the geography selection list
                populateGeographies();

                // Make the initial call to populate the period lists
                adjustPeriod();
				
                // Dynatree
				if("<?php echo $parentName?>" == "qcew" || "<?php echo $parentName?>" == "oes" || "<?php echo $parentName?>" == "ces" || "<?php echo $parentName?>" == "ui"){
					ux.load("jqueryDynaTree", function () {
						$(function () {
							// --- Initialize sample trees
							$("#naicsTree").dynatree({
								checkbox: true,
								selectMode: 2,
								children: naicsData,
								onSelect: function (select, node) {
									// Get a list of all selected nodes, and convert to a key array:
									var selKeys = $.map(node.tree.getSelectedNodes(), function (node) {
										return node.data.key;
									});
									$("#echoSelection3").text(selKeys.join(", "));

									// Get a list of all selected TOP nodes
									var selRootNodes = node.tree.getSelectedNodes(true);
									// ... and convert to a key array:
									var selRootKeys = $.map(selRootNodes, function (node) {
										return node.data.key;
									});
									$("#echoSelectionRootKeys3").text(selRootKeys.join(", "));
									$("#echoSelectionRoots3").text(selRootNodes.join(", "));
								},
								onDblClick: function (node, event) {
									node.toggleSelect();
								},
								onKeydown: function (node, event) {
									if (event.which == 32) {
										node.toggleSelect();
										return false;
									}
								},
								onPostInit: function (isReloading, isError) {
									showHideIndustryTree();
								}
								
							});
							
						});
						$("#btnSelectAll").click(function () {
							$("#naicsTree").dynatree("getRoot").visit(function (node) {
								node.select(true);
							});
							return false;
						});
						$("#btnDeselectAll").click(function () {
							$("#naicsTree").dynatree("getRoot").visit(function (node) {
								node.select(false);
							});
							return false;
						});
						$(".custom").click(function () {
							$("#naicsTree").dynatree("enable")
							return true;
						});
						$(".noncustom").click(function () {
							$("#naicsTree").dynatree("disable")
							return true;
						});
							
						if("<?php echo $parentName?>" == "ces") {
							for (var j = 0; j < additionalNaicsData.length; j++) {
								if (additionalNaicsData[j]["key"] > 10000000 && additionalNaicsData[j]["key"] < 20000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("10000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 20000000 && additionalNaicsData[j]["key"] < 30000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("20000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 30000000 && additionalNaicsData[j]["key"] < 40000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("30000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 41000000 && additionalNaicsData[j]["key"] < 42000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("41000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 42000000 && additionalNaicsData[j]["key"] < 43000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("42000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 43000000 && additionalNaicsData[j]["key"] < 50000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("43000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 50000000 && additionalNaicsData[j]["key"] < 55000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("50000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 55000000 && additionalNaicsData[j]["key"] < 60000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("55000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 60000000 && additionalNaicsData[j]["key"] < 65000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("60000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 65000000 && additionalNaicsData[j]["key"] < 70000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("65000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 70000000 && additionalNaicsData[j]["key"] < 80000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("70000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 80000000 && additionalNaicsData[j]["key"] < 90000000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("80000000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 90910000 && additionalNaicsData[j]["key"] < 90920000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("90910000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 90920000 && additionalNaicsData[j]["key"] < 90930000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("90920000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
								if (additionalNaicsData[j]["key"] > 90930000) {
									$("#naicsTree").dynatree("getTree").getNodeByKey("90930000").addChild({
										title: additionalNaicsData[j]["title"],
										key: additionalNaicsData[j]["key"]
									});
								}
							}
						}							
					});
				}
							  
				if("<?php echo $parentName?>" == "oes" || "<?php echo $parentName?>" == "ui") {
					ux.load("jqueryDynaTree", function () {
						$(function () {
							// --- Initialize sample trees
							$("#socTree").dynatree({
								checkbox: true,
								selectMode: 2,
								children: socData,
								onSelect: function (select, node) {
									// Get a list of all selected nodes, and convert to a key array:
									var selKeys = $.map(node.tree.getSelectedNodes(), function (node) {
										return node.data.key;
									});
									$("#echoSelection3").text(selKeys.join(", "));
									// Get a list of all selected TOP nodes
									var selRootNodes = node.tree.getSelectedNodes(true);
									// ... and convert to a key array:
									var selRootKeys = $.map(selRootNodes, function (node) {
										return node.data.key;
									});
									$("#echoSelectionRootKeys3").text(selRootKeys.join(", "));
									$("#echoSelectionRoots3").text(selRootNodes.join(", "));
								},
								onDblClick: function (node, event) {
									node.toggleSelect();
								},
								onKeydown: function (node, event) {
									if (event.which == 32) {
										node.toggleSelect();
										return false;
									}
								},
								onPostInit: function (isReloading, isError) {
									showHideOccupationTree();
								}
								
							});
						});
						$("#btnSelectAllSoc").click(function() {
							$("#socTree").dynatree("getRoot").visit(function (node) {node.select(true);});
							return false;
						});
						$("#btnDeselectAllSoc").click(function() {
							$("#socTree").dynatree("getRoot").visit(function (node) {node.select(false);});
							return false;
						});
						$(".custom").click(function() {
							$("#socTree").dynatree("enable")
							return true;
						});
						$(".noncustom").click(function() {
							$("#socTree").dynatree("disable")
							return true;
						});
					});
				}
			
				$('#convertcsv').click(function() {
					var csv = 'data:application/csv;charset=utf-8,' + encodeURIComponent(chartDataTableToCSV());					
					$(this).attr('download', generateFilenameTitle() + "_chart.csv"); 
					$(this).attr('href', csv); 
				});
			
				$('#tableconvertcsv').click(function() {										   
					var csv = 'data:application/csv;charset=utf-8,' + encodeURIComponent(dataTableToCSV());					
					$(this).attr('download', generateFilenameTitle() + "_table.csv"); 
					$(this).attr('href', csv); 
				});

				// Map initialization
				LazyLoad.js('js/OpenLayers.js', function () {					
					LazyLoad.js('js/NBT.js', function () {
						LazyLoad.js('js/NBT/Class.js', function () { });
						LazyLoad.js('js/NBT/App.js', function () { });
						LazyLoad.js('js/NBT/Provider.js', function () { });
						LazyLoad.js('js/NBT/Provider/Bing.js', function () { });
						LazyLoad.js('js/NBT/Provider/Geoserver.js', function () { });
						LazyLoad.js('js/NBT/Provider/ArcGIS.js', function () { });
						LazyLoad.js('js/NBT/Control.js', function () { });
						LazyLoad.js('js/NBT/Control/ZoomBar.js', function () { });
						LazyLoad.js('js/NBT/Control/InfoWindow.js', function () { });
						LazyLoad.js('js/NBT/Control/ResourceList.js', function () { });
						LazyLoad.js('js/NBT/Control/States.js', function () { });
						LazyLoad.js('js/NBT/Control/Geolocator.js', function () { });
						LazyLoad.js('js/NBT/Control/ClearSelection.js', function () { });
						LazyLoad.js('js/NBT/Protocol.js', function () { });
						LazyLoad.js('js/NBT/Protocol/MultiRequester.js', function () { });
						LazyLoad.js('js/NBT/Format.js', function () { });
						LazyLoad.js('js/NBT/Format/ArcGIS.js', function () { });
						LazyLoad.js('js/NBT/Rule.js', function () { });
						LazyLoad.js('js/NBT/Index.js', function () { });
						LazyLoad.js('js/NBT/Index/QuadTree.js', function () { });
						LazyLoad.js('js/NBT/Strategy.js', function () { });
						LazyLoad.js('js/NBT/Strategy/FixedFilter.js', function () { });
						LazyLoad.js('js/NBT/Thematic.js', function () { });
						LazyLoad.js('js/NBT/Thematic/Classifier.js', function () { });
						LazyLoad.js('js/NBT/Thematic/LayerUpdater.js', function () { });
						LazyLoad.js('js/NBT_App_DOL.js', function () { });
					});
				});			  
			});
        </script>
	</body>
</html>