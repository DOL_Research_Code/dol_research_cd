

<?php

function generateIndustryData() {
	///*
	$serverName = DB_HOST;
	$connectionInfo = array("Database"=>DB_DATABASE, "UID"=>DB_USER, "PWD"=>DB_PASSWORD);
	$conn = sqlsrv_connect($serverName, $connectionInfo);
	
	if($conn === false) {
		die(print_r( sqlsrv_errors(), true));
	}

	$stmt = sqlsrv_query($conn, "select TITLE, NAICS, NAICS_LEVEL from NAICS_CODE where (NAICS_LEVEL = 2 and NAICS != '00' and NAICS != '951' and NAICS != '952' and NAICS != '953' and NAICS != '99' and NAICS != '01'  and NAICS != '02') or NAICS = '000000' or NAICS = '9991' or NAICS = '9992' or NAICS = '9993' or NAICS = '999' order by NAICS");
	if($stmt === false) {
		die( print_r( sqlsrv_errors(), true) );
	}

	echo "var naicsData = [\n";
	
	$prevLevel = -1;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC) ) {
		$level = 2; // This is to be flat
		
		if($prevLevel == -1) {
		}
		else if($level > $prevLevel) {
			echo ",\nchildren: [\n";
		}
		else if($level == $prevLevel) {
			echo "},\n";
		}
		else if($level < $prevLevel) {
			echo "}\n]\n},\n";
		}
		
		echo "{title:\"".$row[0]." (".$row[1].")\", key: \"".$row[1]."\"";
	
		$prevLevel = $level;	
	}
	
	echo "}\n";
	if($prevLevel == 3) {
		echo "]\n}\n";
	}
	
	echo "];\n";
}

function generateOccupationData() {
	$serverName = DB_HOST;
	$connectionInfo = array("Database"=>DB_DATABASE, "UID"=>DB_USER, "PWD"=>DB_PASSWORD);
	$conn = sqlsrv_connect($serverName, $connectionInfo);
	 
	if($conn === false) {
		die(print_r( sqlsrv_errors(), true));
	}
	 
	$stmt = sqlsrv_query($conn, "select SOCTITLE, SOCCODE from SOCCODE where SOCCODE != 550000 order by SOCCODE");
	if($stmt === false) {
		die( print_r( sqlsrv_errors(), true) );
	}
	 
	echo "var socData = [\n";

	$prevLevel = -1;
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC) ) {
		if( $row[1] == '000000' || preg_match('/^[1-9][1-9]0000/', $row[1])){
			$level = 2;
		}
		else{
		$level = 3;		
		}

		if($prevLevel == -1) {
		}
		else if($level > $prevLevel) {
			echo ",\nchildren: [\n";
		}
		else if($level == $prevLevel) {
			echo "},\n";
		}
		else if($level < $prevLevel) {
			echo "}\n]\n},\n";
		}

		echo "{title:\"".str_replace("\"","",$row[0])." (".preg_replace("/^(.{2})/", "$1-", $row[1]).")\", key: \"".$row[1]."\"";

		$prevLevel = $level;	
	}

	echo "}\n";
	if($prevLevel == 3) {
		echo "]\n}\n";
	}
	echo "];\n";
}	

?>

        <script type="text/javascript">
		<?php 
			generateIndustryData(); 
			generateOccupationData();
		?> 
		
			function generateQueryString() {
				var buffer = "";
				
				if(document.getElementById("ratetype_1").checked){
					buffer += "?ratetype=1";
				}else{
					buffer += "?ratetype=4";
				}
				
				buffer +="&resultset="+$('#resultset').val();
				
				if(document.frmParams.entirestate.checked) {
					buffer += "&isstatewide=1";
				}
				
				buffer += "&areatype=";
				buffer += document.frmParams.regiontype[document.frmParams.regiontype.selectedIndex].value;
				for (var i = 0; i < document.frmParams.listofchosen.options.length; i++) {
					buffer += "&a";
					buffer += i;
					buffer += "=";
					buffer += document.frmParams.listofchosen.options[i].value;					
				}
				
				if (document.frmParams.entirestate.checked) {
					var selectedNodes = $("#naicsTree").dynatree("getSelectedNodes");
					for(var j = 0; j < selectedNodes.length; j++) {
						buffer += "&i";
						buffer += j;
						buffer += "=";
						buffer += selectedNodes[j].data.key;				
					}
				}
				else {
					buffer += "&i0=000000";
				}
				
				var selectedNodes2 = $("#socTree").dynatree("getSelectedNodes");
				for(var k = 0; k < selectedNodes2.length; k++) {
					buffer += "&o";
					buffer += k;
					buffer += "=";
					buffer += selectedNodes2[k].data.key;				
				}
				
				return buffer;
			}
		
			function populatePrimaryAxis() {
				$('#primaryaxis').append('<option value="1">Geography</option>'); 
				$('#primaryaxis').append('<option value="3">Industry</option>'); 		  
				$('#primaryaxis').append('<option value="4">Occupation</option>'); 		  
			}
			
			function loadChartIndexParams(primaryAxisVal, indexParamsObj) {
				if (primaryAxisVal == 1) { // Geography
					indexParamsObj.groupByColumnArr.push(1);
					indexParamsObj.columnDisplayColumnIndex = 8;
					indexParamsObj.rowDisplayColumnIndex = 1;

					if(globalChartData.getDistinctValues(indexParamsObj.columnDisplayColumnIndex).length == 1) {
						indexParamsObj.columnDisplayColumnIndex = 6;
					}					
				}
				else if (primaryAxisVal == 3) { // Industry
					indexParamsObj.groupByColumnArr.push(6);
					indexParamsObj.columnDisplayColumnIndex = 1;
					indexParamsObj.rowDisplayColumnIndex = 6;

					if(globalChartData.getDistinctValues(indexParamsObj.columnDisplayColumnIndex).length == 1) {
						indexParamsObj.columnDisplayColumnIndex = 8;
					}					
				}
				else if (primaryAxisVal == 4) { // Occupation
					indexParamsObj.groupByColumnArr.push(8);
					indexParamsObj.columnDisplayColumnIndex = 1;
					indexParamsObj.rowDisplayColumnIndex = 8;
					
					if(globalChartData.getDistinctValues(indexParamsObj.columnDisplayColumnIndex).length == 1) {
						indexParamsObj.columnDisplayColumnIndex = 6;
					}
				}
				
			}
			
            function populateGeographies() {
                var regionTypeList = document.frmParams.regiontype;

                // Clear out the list
                clearOptions(document.frmParams.listofchoices);
                clearOptions(document.frmParams.listofchosen);

                if (regionTypeList[regionTypeList.selectedIndex].value == "04") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='04' order by AREA"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "10") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='10' order by AREA"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "15") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='15' order by AREANAME"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "21") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='21' order by AREANAME"); ?> 
                }
                else if (regionTypeList[regionTypeList.selectedIndex].value == "51") {
					<?php generateOptionList("document.frmParams.listofchoices", "select AREA, AREANAME from GEOG where STFIPS='36' AND AREATYPE='51' order by AREANAME"); ?> 
                }				
            }

			function adjustPeriod() {
				// No periods in OES but we'll use this function to adjust the chart types
				$("#chartType option[value='3']").remove();
				// And set the rate type enabled status
				dataTypeChanged($('#resultset option:selected').val());
			}
			
			function dataTypeChanged(val) {
				if(val == "9") {
					$('#ratetype_1').attr('disabled', true);
					$('#ratetype_4').attr('disabled', true);
				}
				else {
					$('#ratetype_1').attr('disabled', false);
					$('#ratetype_4').attr('disabled', false);
				}
			}
			
            function entireStateChanged() {
                if (document.frmParams.entirestate.checked) {
					$("#naicsTree").dynatree("enable");
					$("#naicsTree").dynatree("getTree").getNodeByKey("000000").select(false);
                }
                else {
					$("#naicsTree").dynatree("getRoot").visit(function (node) {
						node.select(false);
					});				
					$("#naicsTree").dynatree("getTree").getNodeByKey("000000").select(true);
					$("#naicsTree").dynatree("disable");
                }
            }

			function showHideIndustryTree() {
				entireStateChanged();
			}

			function showHideOccupationTree() {
			}
			
			function getVisibleTableColumns(resultsetCol) {
				var arr = new Array();
				arr.push(resultsetCol);
				arr.push(1);
				arr.push(6);
				arr.push(8);
				arr.push(9);
				arr.push(10);
				arr.push(11);
				arr.push(12);
				arr.push(13);
				arr.push(14);
				
				return arr;
			}

			function formatColumn(colIndex, x) {
				if (colIndex == 10) {
					return formatDollar(x);
				}
				else if (colIndex == 11 || colIndex == 12 || colIndex == 13) {
					return formatLargeDollar(x);
				}
				else if (colIndex == 9 || colIndex == 14) {
					return formatPositiveNumber(x);
				}
				
				return formatNumber(x);
			}
			
			function getPageTitle() {
				return "Occupational Employment Statistics (OES)";
			}
			
			function generateFilenameTitle() {
				return "OES_" + $('#resultset option:selected').text().replace(/ /g, '_')
				+ "_by_" + ($('#listofchosen').children().length == 0 ? "" : $('#regiontype option:selected').text().replace(/ /g, '_'))
				+ (document.frmParams.entirestate.checked ? (($('#listofchosen').children().length == 0) ? "Statewide" : "_and_Statewide") : "");
										
			}

			function generateTitle() {
				return "OES: " + $('#resultset option:selected').text()
				+ " by " + ($('#listofchosen').children().length == 0 ? "" : $('#regiontype option:selected').text())
				+ (document.frmParams.entirestate.checked ? (($('#listofchosen').children().length == 0) ? "Statewide" : " and Statewide") : "");
			}

			function shouldAdjustForRegion(region) {
				return false;
			}
			
        </script>

<form action="/" name="frmParams" class="ux-form-top">

	<input type="hidden" name="periodType" id="periodType" value="01" />
    <div class="grid_11">
        <div class="dol-container">
            <h3>Data Type and Display Options</h3>
            <div class="grid_5">
                <label for="resultset">
                    <span>Data Type:</span>
                    <select name="resultset" id="resultset" onchange="dataTypeChanged(this.options[this.selectedIndex].value);">
                        <option value="9">Number Employed</option>
                        <option value="10">Average wage</option>
                        <option value="11">Entry level wage</option>
                        <option value="12">Experienced level wage</option>
                        <option value="13">Median wage</option>
                        <option value="14">Location quotient</option>
                    </select>
                </label>
            </div>
            <div class="grid_5">
                <label for="ratetype">
                    <span>Rate Type:</span>
					<input type="radio" name="ratetype" id="ratetype_4" value="4" style="display:inline;" checked/> Annual
					<br/>
					<input type="radio" name="ratetype" id="ratetype_1" value="1" style="display:inline;"/> Hourly
                </label>
            </div>
        </div>
        <!-- end .dol-container -->
    </div>
    <div class="grid_11">
        <div class="dol-container">
            <h3>Geography Filter</h3>
            <div class="grid_11">
                <div class="grid_5 alpha">
                    <label for="regiontype">
                        <span>Type:</span>
                    </label>
					<input type="checkbox" name="entirestate" id="entirestate" onchange="entireStateChanged();" /> State-wide data?
					<select name="regiontype" id="regiontype" onchange="populateGeographies();">						
						<option value="10">Labor Market Region</option>
					</select>
                </div>
                <div class="grid_6 omega">
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="grid_4 ux-selection-component-left">
                <label for="listofchoices"><span>Available Areas:</span>
                    <select name="listofchoices" id="listofchoices" multiple="multiple" size="7">
                        <option></option>
                    </select>
                </label>
            </div>
            <div class="grid_2 ux-selection-component-middle">
                <div class="button-container">
                    <input type="button" value="Add" id="addButton" class="button next icon-right width-100" onclick="addToSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Add All" id="addAllButton" class="button next icon-right width-100" onclick="addAllToSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Remove" id="removeButton" class="button previous width-100" onclick="removeFromSelected();" />
                </div>
                <div class="button-container">
                    <input type="button" value="Remove All" id="removeAllButton" class="button previous width-100" onclick="removeAllFromSelected();" />
                </div>
            </div>
            <div class="grid_4 ux-selection-component-right">
                <label for="listofchosen"><span>Selected Areas:</span>
                    <select name="listofchosen" id="listofchosen" multiple="multiple" size="7">
                    </select>
                </label>
            </div>
        </div>
        <!-- end .dol-container -->
    </div>
	<div class="grid_11">
		<div class="dol-container">
			<h3>Occupation Filter</h3>
			&nbsp;<a href="#" id="btnSelectAllSoc">Select all</a>&nbsp;&nbsp;
			<a href="#" id="btnDeselectAllSoc">Deselect all</a>
			<div id="socTree">
			</div>
		</div>
	</div>
    <div class="grid_11">
		<div class="dol-container">
			<h3>Industry Filter</h3>
			&nbsp;<a href="#" id="btnSelectAll">Select all</a>&nbsp;&nbsp;
			<a href="#" id="btnDeselectAll">Deselect all</a>
			<div id="naicsTree">
			</div>
		</div>
		<!-- end .dol-container -->
	</div>	
<!-- end .dol-container -->
</form>
